import tkinter as tk
import serial
from tkinter import ttk
# from funcs_stat import *


# Функция выхода их программы
def on_closing():
    scores.destroy()


scores = tk.Tk()
# scores.iconbitmap('C:\\TestInstruments\\Reports\\atom.ico')

scores.title('Номер пенала')
scores.geometry('{}x{}'.format(250, 150))
scores.resizable(0, 0)

try:
    # ser = serial.Serial('\\.\\COM8', 9600, timeout=None, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)
    label = tk.Label(scores, text="Номер пенала:\n" + str(12), font=('Calibri Bold', 22), justify=tk.CENTER)
except Exception:
    label = tk.Label(scores, text=Exception, font=('Calibri', 12), justify=tk.CENTER)

label.place(relx=0.1, rely=0.2)

# Закрытие окна через крестик
scores.protocol("WM_DELETE_WINDOW", on_closing)
scores.mainloop()
