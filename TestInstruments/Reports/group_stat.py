import sys
import tkinter as tk
from tkinter import ttk
from funcs_stat import *


# Исправление бага с отображением background в таблице - не удалять
def fixed_map(option):
    return [elm for elm in style.map('mystyle.Treeview', query_opt=option) if elm[:2] != ('!disabled', '!selected')]


# Функция выхода их программы
def on_closing():
    scores.destroy()


connection = None
leters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M']
groups = []
lstb_donnes = []
numer = 1

initdonn, arm_id, stat = initdb(sys.argv[0][0:sys.argv[0][0:sys.argv[0].rfind("\\")].rfind("\\") + 1] + "arm_id.ini")
# initdonn, stat = initdb('C:\\TestInstruments\\arm_id.ini')
status, connection = connectdb(initdonn)

with connection.cursor() as cur:
    cur.execute("SELECT result_group FROM atedb.testplan_results WHERE arm_id = " + str(arm_id) + " ORDER by id DESC LIMIT " + str(sys.argv[3]))
    rows = cur.fetchall()
    rows.reverse()

for i in range(0, int(sys.argv[3])):
    groups.append(rows[i].get('result_group'))

if int(sys.argv[3]) != 0:
    for j in range(0, len(leters)):
        if groups.count(leters[j]) != 0:
            lstb_donnes.append([str(numer), leters[j], str(groups.count(leters[j]))])
            numer = numer + 1

scores = tk.Tk()
scores.iconbitmap(sys.argv[0][0:sys.argv[0].rfind("\\") + 1] + '\\atom.ico')
# scores.iconbitmap('C:\\TestInstruments\\Reports\\atom.ico')

scores.title('Статистика по группам')
scores.geometry('{}x{}'.format(360, 300))
scores.resizable(0, 0)

# Создание Treeview (исходные константы)
cols = ('№', 'Группа', 'Количество')
align = [tk.CENTER, tk.CENTER, tk.CENTER]
widt = [60, 140, 140]

# Стили для таблицы
style = ttk.Style()
style.configure("mystyle.Treeview", highlightthickness=0, bd=0, font=('Calibri', '15'))  # Modify the font of the body
style.configure("mystyle.Treeview.Heading", font=('Calibri', '14', 'bold'))  # Modify the font of the headings
style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders
style.map('mystyle.Treeview', background=fixed_map('background'))

# Настройка дерева тривьев
listBox = ttk.Treeview(scores, columns=cols, show='headings', style="mystyle.Treeview", selectmode="none")

# set column headings
for i in range(0, len(cols)):
    listBox.column(str(i), minwidth=15, stretch=False, width=widt[i], anchor=align[i])
    listBox.heading(str(i), text=cols[i], anchor=align[i])

for n in range(0, len(lstb_donnes)):
    listBox.insert("", "end", values=(lstb_donnes[n][:]))

# Создание скролбара по оси Y
verscrlbar = ttk.Scrollbar(scores, orient="vertical")
verscrlbar.pack(side='right', expand=True, padx=2, fill='y')
verscrlbar.config(command=listBox.yview)
listBox.configure(yscrollcommand=verscrlbar.set)

listBox.pack(side='left', expand=1, anchor='n', padx=2, pady=6, fill=tk.BOTH)

# Закрытие окна через крестик
scores.protocol("WM_DELETE_WINDOW", on_closing)
scores.mainloop()
