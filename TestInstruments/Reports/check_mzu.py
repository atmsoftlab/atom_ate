import serial
import time
import tkinter as tk
from tkinter import messagebox as mb


def check_state_mzu():
    def data_request():
        # initialization and open the port

        # possible timeout values:
        #    1. None: wait forever, block call
        #    2. 0: non-blocking mode, return immediately
        #    3. x, x is bigger than 0, float allowed, timeout block call

        ser = serial.Serial()
        ser.port = "COM10"
        ser.baudrate = 9600
        ser.bytesize = serial.EIGHTBITS  # number of bits per bytes
        ser.parity = serial.PARITY_NONE  # set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE  # number of stop bits
        ser.timeout = 1  # non-block read
        # ser.timeout = None                    # block read
        # ser.timeout = 2                       # timeout block read
        ser.xonxoff = False  # disable software flow control
        ser.rtscts = False  # disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False  # disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 2  # timeout for write

        try:
            ser.open()

            if ser.isOpen():
                try:
                    ser.flushInput()  # flush input buffer, discarding all its contents
                    ser.flushOutput()  # flush output buffer, aborting current output
                    # and discard all that is in buffer

                    # write data
                    ser.write(bytes("State", 'utf-8'))
                    print("Запрос состояния МЗУ: State")

                    time.sleep(1)  # give the serial port sometime to receive the data

                    response = ser.readline()
                    print("Ответ на запрос: " + str(response.decode('utf-8')))

                    check = str(response.decode('utf-8'))
                    status = 0

                    ser.close()
                except Exception as e1:
                    print("Ошибка получения запроса о состояниии МЗУ ... : " + str(e1))
                    check = "Ошибка получения запроса о состояниии МЗУ ... : " + str(e1)
                    status = 1

            else:
                print("Ошибка при открытии последовательного порта")
                check = "Ошибка при открытии последовательного порта"
                status = 1

        except Exception as e:
            print("Ошибка открытия последовательного порта: " + str(e))
            check = "Ошибка открытия последовательного порта: " + str(e)
            status = 1

        return check, status

    root = tk.Tk()
    root.withdraw()

    chk, stat = data_request()
    # print(chk)

    if stat == 1:
        answer = 'YES'
        while answer == 'YES' and stat == 1:
            answer = mb.askquestion(title="Ошибка соединения последовательного порта ... ", message="Попытка запроса о состоянии МЗУ завершилась "
                                                                                                    "следуюещей ошибкой : \n\n" + chk + "\n\nВыполнить повторный запрос?")
            chk, stat = data_request()
        else:
            if stat == 0:
                if chk.find('FLT'):
                    state = "Авария ЗУ!"
                    st = 1
                elif chk.find('BSY'):
                    state = "ЗУ находится в режиме ручной настройки, обхода или исполняет команду!"
                    st = 1
                elif chk.find('ERC'):
                    state = "Ошибка команды или формата!"
                    st = 1
                elif chk.find('NBP'):
                    state = "Не установлена базовая точка!"
                    st = 1
                elif chk.find('EDG'):
                    state = "Достигнут датчик края!"
                    st = 1
                else:
                    state = "Статус OK!"
                    st = 0

    elif stat == 0:
        if chk.find('FLT') != -1:
            state = "Авария ЗУ!"
            st = 1
        elif chk.find('BSY') != -1:
            state = "ЗУ находится в режиме ручной настройки, обхода или исполняет команду!"
            st = 1
        elif chk.find('ERC') != -1:
            state = "Ошибка команды или формата!"
            st = 1
        elif chk.find('NBP') != -1:
            state = "Не установлена базовая точка!"
            st = 1
        elif chk.find('EDG') != -1:
            state = "Достигнут датчик края!"
            st = 1
        else:
            state = "Статус OK!"
            st = 0

    mb.showinfo("Состояние МЗУ ...", state)
    return st

    root.destroy()
    root.mainloop()


# st = check_state_mzu()
# print(st)
