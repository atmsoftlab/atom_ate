import os
import sys
import subprocess
import configparser
import tkinter as tk
from tkinter import messagebox as mb

from funcs_stat import getpath2pathconf, initdb, connectdb, readfromdb_runreport


def runreports(pathrep):
    subprocess.Popen([sys.executable, pathrep])
    root.destroy()


# Функция выхода их программы
def on_closing():
    if mb.askokcancel("Выход", "Вы хотите выйти из программы?"):
        root.destroy()


# -- ОСНОВНОЙ КОД --
path2readini = os.path.abspath(os.path.dirname(sys.argv[0])).split(str(os.path.abspath(os.path.dirname(sys.argv[0])).split('TestInstruments')[-1]))[0]

if os.path.exists(path2readini + "\\arm_id.ini"):  # Проверка существования ini файла с arm_id

    config = configparser.ConfigParser()
    config.read(path2readini + "\\arm_id.ini")

    if config.sections()[0] == 'MAIN':

        if 'arm_id' in config[config.sections()[0]]:
            arm_id = config[config.sections()[0]]['arm_id']

            connection = None
            patchconfig, var_zapuska = getpath2pathconf()  # Путь к файлу AToM_ATE.exe.config настроек для инфы подключения к БД

            # Считывание данных из БД
            initdonn, arm_id, stat = initdb(patchconfig)

            if stat == 0:
                status, connection = connectdb(initdonn)
                if status == 0:
                    info_reports = readfromdb_runreport(connection, arm_id)
                    connection.close()

                    root = tk.Tk()
                    root.title('Выбор отчета для загрузки ...')
                    root.resizable(0, 0)
                    root.iconbitmap(str(os.path.abspath(os.path.dirname(sys.argv[0]))) + '\\atom.ico')

                    # Gets the requested values of the height and widht.
                    windowWidth = root.winfo_reqwidth()
                    windowHeight = root.winfo_reqheight()

                    # Gets both half the screen width/height and window width/height
                    positionRight = int(root.winfo_screenwidth() / 2 - windowWidth)
                    positionDown = int(root.winfo_screenheight() / 2 - windowHeight)

                    # Positions the window in the center of the page.
                    root.geometry("+{}+{}".format(positionRight, positionDown))

                    label_pojasnenie = tk.Label(root, text="Из предложенного списка отчетов выберите \n необходимый для дальнейшего запуска.",
                                                font=('Calibri Bold', 14), justify=tk.CENTER)
                    label_pojasnenie.grid(row=0, column=0, pady=10)

                    for i in range(0, len(info_reports)):
                        bufer_pathrep = path2readini + "\\" + info_reports[i].get('path_to_script')
                        print(bufer_pathrep)
                        exec("button" + str(i) + " = tk.Button(root, text='" + info_reports[i].get('fullname') +
                             "', font=('Calibri Bold', 12), height=3, width=60, wraplength=450, justify=tk.CENTER, padx=5, pady=5, bg='#e0e3de', relief='groove',"
                             " cursor='hand2', command=lambda pathrep=bufer_pathrep: runreports(pathrep))")
                        exec("button" + str(i) + ".grid(row=" + str(i + 1) + ", column=0, pady=5, padx=5)")

                    # Закрытие окна через крестик
                    root.protocol("WM_DELETE_WINDOW", on_closing)
                    root.mainloop()

            elif stat == 1:
                root = tk.Tk()
                root.withdraw()
                mb.showwarning(title='Внимание!', message='Не найден файл AToM_ATE.exe.config по указанному пути')
                root.destroy()
            elif stat == 2:
                root = tk.Tk()
                root.withdraw()
                mb.showwarning(title='Внимание!', message='В файле AToM_ATE.exe.config отсутствует необходимая информация для инициализации подключения к БД')
                root.destroy()
        else:
            mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: в секции MAIN отсутствует ключ arm_id. '
                                                                                         'Программа завершает работу.', icon='error')
    else:
        mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: отсутствует секция MAIN в конфигурационном ini-файле arm_id.ini. '
                                                                                     'Программа завершает работу.', icon='error')
else:
    mb.showerror(title='Отсутствует ini файл arm_id ...', message='Ошибка: отсутствует конфигурационный ini-файл arm_id.ini. '
                                                                  'Программа завершает работу.', icon='error')
