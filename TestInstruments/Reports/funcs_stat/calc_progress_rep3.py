def calc_progress_rep3(testplanres, duts, measurements):
    import os
    import sys
    import math
    import tkinter as tk
    from tkinter import ttk

    calc_breaking = 0

    def findduts(_testplanres, _measurements, _duts):
        duts_name = []
        duts_not_name = []
        testplanres_af = []

        len_meas = len(_measurements)
        meas = {_measurements[j].get('testplan_results'): _measurements[j].get('duts_id') for j in range(0, len_meas)}

        dutsn = {_duts[k].get('id'): _duts[k].get('name') for k in range(len(_duts))}

        len_tstplnres2 = len(_testplanres)
        for m in range(len_tstplnres2):
            if meas.get(testplanres[m].get('id')) is not None:
                testplanres_af.append(_testplanres[m])
                duts_name.append(dutsn[meas[testplanres[m].get('id')]])
            else:
                duts_not_name.append(_testplanres[m].get('id'))

        return duts_name, testplanres_af

    def func_convmeasurement(_duts, _testplanres):
        global calc_breaking

        pas = [1]
        if len(_testplanres) >= 100:
            delta = math.floor(len(_testplanres)/100)
            for j in range(1, 101):
                if j != 100:
                    pas.append(pas[j - 1] + delta)
                else:
                    pas.append(len(_testplanres))
            zn_point = 1
        else:
            bar['maximum'] = len(_testplanres)
            for f in range(2, len(_testplanres) + 1):
                pas.append(f)
            zn_point = 100 / len(_testplanres)

        point = 1
        dtestplanres = []
        for n in range(0, len(_testplanres)):
            if button['state'] == 'normal':
                if n >= pas[point]:
                    bar['value'] += 1
                    if zn_point == 1:
                        percentlabel.config(text=str(point) + " %")
                    else:
                        percentlabel.config(text='{:.2%}'.format((point * zn_point) / 100))
                    percentlabel.grid(row=0, column=1, padx=5)
                    point += 1
                    window.update()

                dtestplanres.append([n + 1, duts_name[n],
                                    testplanres_af[n].get('start_date').strftime("%H:%M:%S %d.%m.%Y"),
                                    testplanres_af[n].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                                    testplanres_af[n].get('batch_number'), testplanres_af[n].get('plate_number'),
                                    "ГОДЕН" if testplanres_af[n].get('result') == 0 else "БРАК", testplanres_af[n].get('result_group')])
            else:
                dtestplanres.clear()
                calc_breaking = 1
                break

        window.destroy()
        return dtestplanres

    def disable_event():
        pass

    window = tk.Toplevel()
    window.iconbitmap(os.path.abspath(os.path.dirname(sys.argv[0])).replace('funcs_stat', '') + '\\atom.ico')
    window.title('Процесс подготовки данных  ... ')
    window.geometry('{}x{}+{}+{}'.format(380, 85, int(window.winfo_screenwidth() * 0.1), int(window.winfo_screenheight() * 0.1)))
    window.resizable(0, 0)
    window.protocol("WM_DELETE_WINDOW", disable_event)

    bar = ttk.Progressbar(window, orient=tk.HORIZONTAL, length=300)
    bar.grid(row=0, column=0, padx=10, pady=10)

    percentlabel = tk.Label(window, text="0 %", font=('Calibri', 12, 'bold'))

    button = tk.Button(window, text="СТОП", font=('Calibri', 12, 'bold'))
    button.grid(row=1, columnspan=2)

    duts_name, testplanres_af = findduts(testplanres, measurements, duts)
    dtestplanres = func_convmeasurement(duts_name, testplanres_af)

    return dtestplanres, calc_breaking

    window.mainloop()
