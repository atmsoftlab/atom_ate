def savexlsxn_rep3(file_name, headers, lstb_donnes):
    import os
    import psutil
    import time
    import xlsxwriter

    colmnname = []

    # Закрытие запущенного процесса EXCEL
    for proc in psutil.process_iter():
        if proc.name() == "EXCEL.EXE":
            os.system('taskkill /f /im EXCEL.EXE')
    time.sleep(3)

    # Удаление уже существующего файла
    if os.path.exists(file_name):
        os.remove(file_name)
        time.sleep(2)

    for i in range(0, 23):  # Создание массива алфавитной нумерации столбцов на английском
        colmnname.append(chr(i + 97).upper())

    colmnwidth = [13, 17, 19, 19, 25, 25, 17, 17]

    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()

    head = workbook.add_format()
    head.set_align('left')
    head.set_bold()

    head_format_left = workbook.add_format()
    head_format_left.set_border(1)
    head_format_left.set_align('left')
    head_format_left.set_bold()
    head_format_left.set_bg_color('#e8e7e7')

    head_format_center = workbook.add_format()
    head_format_center.set_border(1)
    head_format_center.set_align('center')
    head_format_center.set_bold()
    head_format_center.set_bg_color('#e8e7e7')

    cellLeft_format = workbook.add_format()
    cellLeft_format.set_border(1)
    cellLeft_format.set_align('left')

    cellCenter_format = workbook.add_format()
    cellCenter_format.set_border(1)
    cellCenter_format.set_align('center')

    cellLeft_format_brak = workbook.add_format()
    cellLeft_format_brak.set_border(1)
    cellLeft_format_brak.set_align('left')
    cellLeft_format_brak.set_bg_color('#f2dedf')

    cellCenter_format_brak = workbook.add_format()
    cellCenter_format_brak.set_border(1)
    cellCenter_format_brak.set_align('center')
    cellCenter_format_brak.set_bg_color('#f2dedf')

    colgoden = 0
    colbrak = 0
    for m in range(0, len(lstb_donnes)):
        if lstb_donnes[m][6] == 'БРАК':
            colbrak = colbrak + 1
        else:
            colgoden = colgoden + 1

    stat_text = ("Статистика: ВСЕГО ИЗДЕЛИЙ - " + str(len(lstb_donnes)) + "  шт. / 100% :  ИЗ НИХ ГОДНЫХ - " + str(colgoden) + "  шт. / "
                 + str(0 if colgoden == 0 else round(colgoden / len(lstb_donnes) * 100, 3)) + "% ; ИЗ НИХ ЗАБРАКОВАННЫХ - "
                 + str(colbrak) + "  шт. / " + str(0 if colbrak == 0 else round(colbrak / len(lstb_donnes) * 100, 3)) + " %.")

    worksheet.write(0, 0, stat_text, head)

    for i in range(0, len(headers)):
        worksheet.set_column(colmnname[i] + ":" + colmnname[i], colmnwidth[i])
        worksheet.write(2, i, headers[i], head_format_center)

    for j in range(0, len(lstb_donnes)):
        for n in range(0, len(lstb_donnes[0])):
            if lstb_donnes[j][6] == 'БРАК':
                if n == 0:
                    worksheet.write(j + 3, n, int(lstb_donnes[j][n]), cellCenter_format_brak)
                else:
                    worksheet.write(j + 3, n, lstb_donnes[j][n], cellCenter_format_brak)
            else:
                if n == 0:
                    worksheet.write(j + 3, n, int(lstb_donnes[j][n]), cellCenter_format)
                else:
                    worksheet.write(j + 3, n, lstb_donnes[j][n], cellCenter_format)
    workbook.close()
