def readfromdb_rep1_nv(connection, initdonn, max_col_group, data_start_vvd, data_finish_vvd, arm_id):
    import json
    import time
    import tkinter as tk
    from funcs_stat import connectdb
    from tkinter import messagebox as mb

    dmeasurements = []
    errindxdata = 0

    def func_convtstplnres(_testplanres):
        len_testplanres = len(_testplanres)
        return {_testplanres[j].get('id'): {'start_date': _testplanres[j].get('start_date').strftime("%H:%M:%S %d.%m.%Y"),
                                            'finish_date': _testplanres[j].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                                            'batch_number': _testplanres[j].get('batch_number'),
                                            'plate_number': _testplanres[j].get('plate_number')} for j in range(0, len_testplanres)}

    def func_convduts(_duts):
        return {_duts[k].get('id'): _duts[k].get('name') for k in range(len(_duts))}

    def func_convtestelements(_tstelem):
        return {_tstelem[n].get('id'): _tstelem[n].get('name') for n in range(len(_tstelem))}

    def findnormes(mcol_groupn, normatable):
        massnormes = []
        for j in range(0, mcol_groupn):
            if j < len(normatable):
                massnormes.extend([normatable[j].get('Min'), normatable[j].get('Max')])
            else:
                massnormes.extend(['-', '-'])
        return massnormes

    def func_convmeasurement(_measurements, _testplanres, _tstelem, _duts):
        testplanres_keys = []
        for k in _testplanres.keys():
            testplanres_keys.append(k)

        dictmeas = {}

        while len(_measurements) > 0:
            buf_meas = []
            stop = False
            while stop is not True:
                sravnitel = _measurements[0].get('testplan_results')
                buf_meas.append(_measurements[0])
                del _measurements[0]
                if len(_measurements) > 1 and sravnitel == _measurements[0].get('testplan_results'):
                    buf_meas.append(_measurements[0])
                    del _measurements[0]
                elif len(_measurements) == 1 and sravnitel == _measurements[0].get('testplan_results'):
                    dictmeas.update({sravnitel: _measurements[0]})
                else:
                    dictmeas.update({sravnitel: buf_meas})
                    stop = True

        measurements_output = []
        for n in testplanres_keys:
            if dictmeas.get(n) is not None:
                for p in range(len(dictmeas[n])):
                    meas_row = dictmeas[n][p]
                    meas_value = json.loads(meas_row.get('value'))
                    for g in range(0, len(meas_value)):
                        try:
                            bufer_measurements = [len(measurements_output) + 1, _duts[meas_row.get('duts_id')],
                                                  _testplanres[meas_row.get('testplan_results')].get('start_date'),
                                                  _testplanres[meas_row.get('testplan_results')].get('batch_number'),
                                                  _testplanres[meas_row.get('testplan_results')].get('plate_number'),
                                                  meas_value[g].get('Name'), meas_value[g].get('Unit'),
                                                  round(float(meas_value[g].get('Value')), 5) if len(meas_value[g].get('Value')) != 0 else ""]
                            bufer_measurements.extend((lambda mcol_groupn, normatable: findnormes(mcol_groupn, normatable))
                                                      (max_col_group, meas_value[g].get('NormaTable')))
                            bufer_measurements.extend(["ГОДЕН" if meas_value[g].get('ClassAfterTestExecute') != 'Брак' else "БРАК",
                                                       meas_value[g].get('ClassAfterTestExecute')])
                            measurements_output.append(bufer_measurements)

                        except TypeError:
                            bufer_measurements = [len(measurements_output) + 1, _duts[meas_row.get('duts_id')],
                                                  _testplanres[meas_row.get('testplan_results')].get('start_date'),
                                                  _testplanres[meas_row.get('testplan_results')].get('batch_number'),
                                                  _testplanres[meas_row.get('testplan_results')].get('plate_number'),
                                                  meas_value[g].get('Name'), meas_value[g].get('Unit'),
                                                  meas_value[g].get('Value')]
                            bufer_measurements.extend((lambda mcol_groupn, normatable: findnormes(mcol_groupn, normatable))
                                                      (max_col_group, meas_value[g].get('NormaTable')))
                            bufer_measurements.extend(["ГОДЕН" if meas_value[g].get('ClassAfterTestExecute') != 'Брак' else "БРАК",
                                                       meas_value[g].get('ClassAfterTestExecute')])
                            measurements_output.append(bufer_measurements)

        return measurements_output

    try:
        try:
            with connection.cursor() as cur:
                start_db_scan = time.time()
                cur.execute("SELECT id, start_date, finish_date, batch_number, plate_number FROM atedb.testplan_results WHERE start_date BETWEEN \""
                            + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                testplanres = cur.fetchall()

                cur.execute("SELECT value, test_id, duts_id, testplan_results FROM atedb.measurements WHERE testplan_results BETWEEN \"" + str(testplanres[0].get('id')) +
                            "\" AND \"" + str(testplanres[-1].get('id')) + "\" AND arm_id = " + str(arm_id))
                measurements = cur.fetchall()

                # cur.execute("SELECT id, name FROM atedb.test_elements WHERE arm_id = " + str(arm_id))
                cur.execute("SELECT id, name FROM atedb.test_elements")
                tstelem = cur.fetchall()

                # cur.execute("SELECT id, name FROM atedb.duts WHERE arm_id = " + str(arm_id))
                cur.execute("SELECT id, name FROM atedb.duts")
                duts = cur.fetchall()
                finish_db_scan = time.time()
                print("db_scan = " + str(finish_db_scan - start_db_scan))

                start_calc = time.time()
                dtestplanres = func_convtstplnres(testplanres)
                dduts = func_convduts(duts)
                dtstelem = func_convtestelements(tstelem)
                dmeasurements = func_convmeasurement(measurements, dtestplanres, dtstelem, dduts)
                end_calc = time.time()
                print("time_calc = " + str(end_calc - start_calc))

            exec_status = 0
        except IndexError:
            exec_status = 0
            errindxdata = 1

    except Exception as e:
        print(e)
        exec_status = 1

    root = tk.Tk()
    root.withdraw()

    answer = True
    while answer is True and exec_status == 1:
        answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                        "повторную попытку подключения?", icon="question")
        if answer:
            exec_status, connection = connectdb(initdonn)

        if exec_status == 0:
            try:
                with connection.cursor() as cur:
                    start_db_scan = time.time()
                    cur.execute("SELECT id, start_date, finish_date, batch_number, plate_number FROM atedb.testplan_results WHERE start_date BETWEEN \""
                                + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                    testplanres = cur.fetchall()

                    cur.execute("SELECT value, test_id, duts_id, testplan_results FROM atedb.measurements WHERE testplan_results BETWEEN \""
                                + str(testplanres[0].get('id')) + "\" AND \"" + str(testplanres[-1].get('id')) + "\" AND arm_id = " + str(arm_id))
                    measurements = cur.fetchall()

                    # cur.execute("SELECT id, name FROM atedb.test_elements WHERE arm_id = " + str(arm_id))
                    cur.execute("SELECT id, name FROM atedb.test_elements")
                    tstelem = cur.fetchall()

                    # cur.execute("SELECT id, name FROM atedb.duts WHERE arm_id = " + str(arm_id))
                    cur.execute("SELECT id, name FROM atedb.duts")
                    duts = cur.fetchall()
                    finish_db_scan = time.time()
                    print("db_scan = " + str(finish_db_scan - start_db_scan))

                start_calc = time.time()
                dtestplanres = func_convtstplnres(testplanres)
                dduts = func_convduts(duts)
                dtstelem = func_convtestelements(tstelem)
                dmeasurements = func_convmeasurement(measurements, dtestplanres, dtstelem, dduts)
                end_calc = time.time()
                print("time_calc = " + str(end_calc - start_calc))
            except IndexError:
                errindxdata = 1

    root.destroy()

    return dmeasurements, exec_status, errindxdata
