def initdb(path):
    import configparser
    import os

    initdonn = {}

    stat = 0

    if os.path.exists(path):
        config = configparser.ConfigParser()
        config.read(path)

        if config.sections()[0] == 'MAIN':
            arm_id = config[config.sections()[0]]['arm_id']

        if config.sections()[1] == 'dbconnection':
            initdonn = {'Database': config[config.sections()[1]]['Database'],
                        'Server': config[config.sections()[1]]['Server'],
                        'Port': config[config.sections()[1]]['Port'],
                        'UserId': config[config.sections()[1]]['User'],
                        'Password': config[config.sections()[1]]['Password']}
        else:
            stat = 2
    else:
        stat = 1

    return initdonn, arm_id, stat



