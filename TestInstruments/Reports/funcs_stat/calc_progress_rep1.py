def calc_progress_rep1(testplanres, tstelem, duts, mass_measurements, max_col_group):
    import os
    import sys
    import math
    import json
    import tkinter as tk
    from tkinter import ttk

    calc_breaking = 0

    def func_convtstplnres(_testplanres):
        len_testplanres = len(_testplanres)
        return {_testplanres[j].get('id'): {'start_date': _testplanres[j].get('start_date').strftime("%H:%M:%S %d.%m.%Y"),
                                            'finish_date': _testplanres[j].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                                            'batch_number': _testplanres[j].get('batch_number'),
                                            'plate_number': _testplanres[j].get('plate_number')} for j in range(0, len_testplanres)}

    def func_convduts(_duts):
        return {_duts[k].get('id'): _duts[k].get('name') for k in range(len(_duts))}

    def func_convtestelements(_tstelem):
        return {_tstelem[n].get('id'): _tstelem[n].get('name') for n in range(len(_tstelem))}

    def findnormes(mcol_groupn, normatable):
        massnormes = []
        for j in range(0, mcol_groupn):
            if j < len(normatable):
                massnormes.extend([normatable[j].get('Min'), normatable[j].get('Max')])
            else:
                massnormes.extend(['-', '-'])
        return massnormes

    def func_convmeasurement(_measurements, _testplanres, _tstelem, _duts):
        global calc_breaking
        dictmeas = {}

        testplanres_keys = []
        for k in _testplanres.keys():
            testplanres_keys.append(k)

        while len(_measurements) > 0:
            buf_meas = []
            stop = False
            while stop is not True:
                sravnitel = _measurements[0].get('testplan_results')
                buf_meas.append(_measurements[0])
                del _measurements[0]
                if len(_measurements) > 1 and sravnitel == _measurements[0].get('testplan_results'):
                    buf_meas.append(_measurements[0])
                    del _measurements[0]
                elif len(_measurements) == 1 and sravnitel == _measurements[0].get('testplan_results'):
                    dictmeas.update({sravnitel: _measurements[0]})
                else:
                    dictmeas.update({sravnitel: buf_meas})
                    stop = True

        if len(testplanres_keys) >= 100:
            delta = math.floor(testplanres_keys[-1] / 100)
            pas = [1]
            for j in range(1, 101):
                if j != 100:
                    pas.append(pas[j - 1] + delta)
                else:
                    pas.append(testplanres_keys[-1])
            zn_point = 1
        else:
            bar['maximum'] = len(testplanres_keys)
            pas = testplanres_keys[:]
            zn_point = 100 / len(testplanres_keys)

        point = 1
        measurements_output = []
        for n in testplanres_keys:
            if button['state'] == 'normal':
                if n >= pas[point]:
                    bar['value'] += 1
                    if zn_point == 1:
                        percentlabel.config(text=str(point) + " %")
                    else:
                        percentlabel.config(text='{:.2%}'.format((point * zn_point) / 100))
                    percentlabel.grid(row=0, column=1, padx=5)
                    point += 1
                    window.update()

                if dictmeas.get(n) is not None:
                    for p in range(len(dictmeas[n])):
                        meas_row = dictmeas[n][p]
                        meas_value = json.loads(meas_row.get('value'))
                        for g in range(0, len(meas_value)):
                            try:
                                bufer_measurements = [len(measurements_output) + 1, _duts[meas_row.get('duts_id')],
                                                      _testplanres[meas_row.get('testplan_results')].get('start_date'),
                                                      _testplanres[meas_row.get('testplan_results')].get('batch_number'),
                                                      _testplanres[meas_row.get('testplan_results')].get('plate_number'),
                                                      meas_value[g].get('Name'), meas_value[g].get('Unit'),
                                                      round(float(meas_value[g].get('Value')), 5) if len(meas_value[g].get('Value')) != 0 else ""]
                                bufer_measurements.extend((lambda mcol_groupn, normatable: findnormes(mcol_groupn, normatable))
                                                          (max_col_group, meas_value[g].get('NormaTable')))
                                bufer_measurements.extend(["ГОДЕН" if meas_value[g].get('ClassAfterTestExecute') != 'Брак' else "БРАК",
                                                           meas_value[g].get('ClassAfterTestExecute')])
                                measurements_output.append(bufer_measurements)
                            except TypeError:
                                bufer_measurements = [len(measurements_output) + 1, _duts[meas_row.get('duts_id')],
                                                      _testplanres[meas_row.get('testplan_results')].get('start_date'),
                                                      _testplanres[meas_row.get('testplan_results')].get('batch_number'),
                                                      _testplanres[meas_row.get('testplan_results')].get('plate_number'),
                                                      meas_value[g].get('Name'), meas_value[g].get('Unit'),
                                                      meas_value[g].get('Value')]
                                bufer_measurements.extend((lambda mcol_groupn, normatable: findnormes(mcol_groupn, normatable))
                                                          (max_col_group, meas_value[g].get('NormaTable')))
                                bufer_measurements.extend(["ГОДЕН" if meas_value[g].get('ClassAfterTestExecute') != 'Брак' else "БРАК",
                                                           meas_value[g].get('ClassAfterTestExecute')])
                                measurements_output.append(bufer_measurements)
            else:
                measurements_output.clear()
                calc_breaking = 1
                break

        window.destroy()
        return measurements_output

    def disable_event():
        pass

    window = tk.Toplevel()
    window.iconbitmap(os.path.abspath(os.path.dirname(sys.argv[0])).replace('funcs_stat', '') + '\\atom.ico')
    window.title('Процесс подготовки данных  ... ')
    window.geometry('{}x{}+{}+{}'.format(380, 85, int(window.winfo_screenwidth() * 0.1), int(window.winfo_screenheight() * 0.1)))
    window.resizable(0, 0)
    window.protocol("WM_DELETE_WINDOW", disable_event)

    bar = ttk.Progressbar(window, orient=tk.HORIZONTAL, length=300)
    bar.grid(row=0, column=0, padx=10, pady=10)

    percentlabel = tk.Label(window, text="0 %", font=('Calibri', 12, 'bold'))

    button = tk.Button(window, text="СТОП", font=('Calibri', 12, 'bold'))
    button.grid(row=1, columnspan=2)

    dtestplanres = func_convtstplnres(testplanres)
    dduts = func_convduts(duts)
    dtstelem = func_convtestelements(tstelem)
    dmeasurements = func_convmeasurement(mass_measurements, dtestplanres, dtstelem, dduts)

    return dmeasurements, calc_breaking

    window.mainloop()
