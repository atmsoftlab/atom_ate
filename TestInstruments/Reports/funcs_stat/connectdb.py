def connectdb(initdb):  # host, port, user, password, db
    import pymysql.cursors
    import tkinter as tk
    from tkinter import messagebox as mb

    connection = None
    connect = 0
    status = 0
    port = initdb.get('Port')

    if type(port) != int:
        port = int(port)

    root = tk.Tk()
    root.withdraw()

    while connect == 0:
        try:
            connection = pymysql.connect(host=initdb.get('Server'),
                                         port=port,
                                         user=initdb.get('UserId'),
                                         password=initdb.get('Password'),
                                         db=initdb.get('Database'),
                                         charset='utf8mb4',
                                         cursorclass=pymysql.cursors.DictCursor)
            connect = 1
            status = 0
            # print("DB connection is good!")
        except Exception as e:
            print(e)
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")
            if answer is not True:
                connect = 1
                status = 1

    root.destroy()

    return status, connection
