def readb_progress_rep4(connection, initdonn, data_start_vvd, data_finish_vvd, arm_id, numsid, amountid):
    import os
    import sys
    import math
    import tkinter as tk
    # from memory_profiler import profile
    from funcs_stat import connectdb
    from tkinter import ttk
    from tkinter import Toplevel
    from tkinter import messagebox as mb

    # @profile
    def start(connect):
        window.transient()
        window.grab_set()
        window.focus_set()
        try:
            with connect.cursor() as cur:
                cur.execute("SELECT id, start_date, finish_date, batch_number, plate_number FROM atedb.testplan_results WHERE start_date BETWEEN \""
                            + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                testplanres = cur.fetchall()

                # cur.execute("SELECT id, name FROM atedb.test_elements WHERE arm_id = " + str(arm_id))
                cur.execute("SELECT id, name FROM atedb.test_elements")
                tstelem = cur.fetchall()

                # cur.execute("SELECT id, name FROM atedb.duts WHERE arm_id = " + str(arm_id))
                cur.execute("SELECT id, name FROM atedb.duts")
                duts = cur.fetchall()

            exec_status = 0
        except Exception as e:
            print(e)
            exec_status = 1

            dialog = Toplevel()
            dialog.withdraw()

            answer = True
            while answer is True and exec_status == 1:
                answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                                "повторную попытку подключения?", icon="question")
                if answer:
                    exec_status, connect = connectdb(initdonn)

                if exec_status == 0:
                    with connect.cursor() as cur:
                        cur.execute("SELECT id, start_date, finish_date, batch_number, plate_number FROM atedb.testplan_results WHERE start_date BETWEEN \""
                                    + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                        testplanres = cur.fetchall()

                        # cur.execute("SELECT id, name FROM atedb.test_elements WHERE arm_id = " + str(arm_id))
                        cur.execute("SELECT id, name FROM atedb.test_elements")
                        tstelem = cur.fetchall()

                        # cur.execute("SELECT id, name FROM atedb.duts WHERE arm_id = " + str(arm_id))
                        cur.execute("SELECT id, name FROM atedb.duts")
                        duts = cur.fetchall()

            dialog.destroy()

        if amountid == 2:
            coefprogress = 10000
            bar['maximum'] = coefprogress
        else:
            coefprogress = 100
            bar['maximum'] = coefprogress

        delta = math.floor((numsid[1] - numsid[0]) / coefprogress)

        pasl = [numsid[0]]
        pasr = [numsid[0] + delta]
        for j in range(1, coefprogress+1):
            if j != coefprogress:
                pasl.append(pasl[j - 1] + delta + 1)
                pasr.append(pasr[j - 1] + delta + 1)
            else:
                pasl.append(pasl[j - 1] + delta + 1)
                pasr.append(numsid[1])

        point = 1
        mass_measurements = []
        breaking = 0
        while point <= coefprogress:
            if button['state'] == 'normal':
                try:
                    with connect.cursor() as cur:
                        cur.execute("SELECT value, test_id, duts_id, testplan_results FROM atedb.measurements WHERE id BETWEEN \"" + str(pasl[point - 1]) +
                                    "\" AND \"" + str(pasr[point - 1]) + "\" AND arm_id = " + str(arm_id))
                        measurements = cur.fetchall()
                    exec_status = 0
                except Exception as e:
                    print(e)
                    exec_status = 1

                    dialog = Toplevel()
                    dialog.withdraw()

                    answer = True
                    while answer is True and exec_status == 1:
                        answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                                        "повторную попытку подключения?", icon="question")
                        if answer:
                            exec_status, connect = connectdb(initdonn)

                        if exec_status == 0:
                            with connect.cursor() as cur:
                                cur.execute("SELECT value, test_id, duts_id, testplan_results FROM atedb.measurements WHERE id BETWEEN \"" + str(pasl[point - 1]) +
                                            "\" AND \"" + str(pasr[point - 1]) + "\" AND arm_id = " + str(arm_id))
                                measurements = cur.fetchall()

                mass_measurements.extend(measurements)

                bar['value'] += 1
                bar.update()

                if coefprogress == 10000:
                    percentlabel.config(text='{:.2%}'.format((point-1)/coefprogress))
                else:
                    percentlabel.config(text=str(point - 1) + " %")

                percentlabel.grid(row=0, column=1, padx=5)
                percentlabel.update()
                point += 1
            else:
                breaking = 1
                break

        window.destroy()
        return exec_status, testplanres, tstelem, duts, mass_measurements, breaking

    def coursor_navedenie(event):
        t = event.widget
        if isinstance(t, tk.Button):
            t.configure(bg='red')

    def coursor_otvedenie(event):
        t = event.widget
        if isinstance(t, tk.Button):
            t.configure(bg='SystemButtonFace')

    def disable_event():
        pass

    window = tk.Toplevel()
    window.iconbitmap(os.path.abspath(os.path.dirname(sys.argv[0])).replace('funcs_stat', '') + '\\atom.ico')
    window.title('Процесс загрузки данных из БД  ... ')
    window.geometry('{}x{}+{}+{}'.format(390, 85, int(window.winfo_screenwidth() * 0.1), int(window.winfo_screenheight() * 0.1)))
    window.resizable(0, 0)
    window.protocol("WM_DELETE_WINDOW", disable_event)

    bar = ttk.Progressbar(window, orient=tk.HORIZONTAL, length=300)
    bar.grid(row=0, column=0, padx=10, pady=10)

    percentlabel = tk.Label(window, text="0 %", font=('Calibri', 12, 'bold'))

    button = tk.Button(window, text="СТОП", font=('Calibri', 12, 'bold'))
    button.bind("<Enter>", coursor_navedenie)
    button.bind("<Leave>", coursor_otvedenie)
    button.grid(row=1, column=0)

    exec_status, testplanres, tstelem, duts, mass_measurements, breaking = start(connection)
    return exec_status, testplanres, tstelem, duts, mass_measurements, breaking

    window.mainloop()
