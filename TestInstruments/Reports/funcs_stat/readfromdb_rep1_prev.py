def readfromdb_rep1_prev(connection):
    import json
    col_group = []

    with connection.cursor() as cur:
        # cur.execute("SELECT parametr FROM atedb.test_parameters WHERE type = 2 AND arm_id = " + str(arm_id))
        cur.execute("SELECT parametr FROM atedb.test_parameters WHERE type = 2")
        parameter = cur.fetchall()

    for h in range(len(parameter)):
        outparameters = json.loads(parameter[h].get('parametr'))
        for i in range(len(outparameters)):
            col_group.append(len(outparameters[i].get('NormaTable')))

    max_col_group = max(col_group)

    return max_col_group
