def readfromdb_rep2(connection):
    import json
    testplanres = tstelem = duts = []
    stat_donnes = []

    def findres(clasaftertestexec):
        if clasaftertestexec != '':
            status = 'ГОДЕН'
        else:
            status = "БРАК"
        return status

    def findnametest(test_id):
        for k in range(0, len(tstelem)):
            if test_id == tstelem[k].get('id'):
                break
        return tstelem[k].get('name')

    def findduts(duts_id):
        for j in range(0, len(duts)):
            if duts_id == duts[j].get('id'):
                break
        return duts[j].get('name')

    def findtstplndonnes(testplan_results):
        for n in range(0, len(testplanres)):
            if testplan_results == testplanres[n].get('id'):
                break
        return {'start_date': testplanres[n].get('start_date').strftime("%H:%M:%S %d.%m.%Y"),
                'finish_date': testplanres[n].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                'batch_number': testplanres[n].get('batch_number'),
                'plate_number': testplanres[n].get('plate_number')}

    def lenmantis(result):
        if result is not None:
            if result.find('.') != -1:
                if (len(result) - result.find('.')) > 5:
                    nresult = result[0:result.find('.') + 5]
                else:
                    nresult = result
            else:
                nresult = result
        else:
            nresult = result
        return nresult

    with connection.cursor() as cur:
        cur.execute("SELECT id, plate_number, batch_number, start_date, finish_date FROM atedb.testplan_results")
        testplanres = cur.fetchall()

        cur.execute("SELECT value, test_id, duts_id, testplan_results FROM atedb.measurements")
        measurements = cur.fetchall()

        cur.execute("SELECT id, name FROM atedb.test_elements")
        tstelem = cur.fetchall()

        cur.execute("SELECT id, name FROM atedb.duts")
        duts = cur.fetchall()

    for h in range(0, len(measurements)):
        massvalue = json.loads(measurements[h].get('value'))
        for i in range(0, len(massvalue)):
            stat_donnes.append([len(stat_donnes)+1, (lambda duts_id: findduts(duts_id))(measurements[h].get('duts_id')),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('start_date'),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('finish_date'),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('batch_number'),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('plate_number'),
                                (lambda test_id: findnametest(test_id))(measurements[h].get('test_id')),
                                massvalue[i].get('Name'), massvalue[i].get('Unit'), (lambda result: lenmantis(result))(massvalue[i].get('Value')),
                                (lambda clasaftertestexec: findres(clasaftertestexec))(massvalue[i].get('ClassAfterTestExecute')),
                                massvalue[i].get('ClassAfterTestExecute')])

    return stat_donnes
