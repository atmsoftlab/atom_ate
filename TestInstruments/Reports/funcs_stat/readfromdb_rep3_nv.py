def readfromdb_rep3_nv(connection, initdonn, data_start_vvd, data_finish_vvd, arm_id):
    import tkinter as tk
    import time
    from funcs_stat import connectdb
    from tkinter import messagebox as mb

    stat_donnes = []
    errindxdata = 0

    def findduts(testplanres, measurements, duts):
        duts_name = []
        duts_not_name = []
        testplanres_af = []

        start_time_transform1 = time.time()
        len_meas = len(measurements)
        meas = {measurements[j].get('testplan_results'): measurements[j].get('duts_id') for j in range(0, len_meas)}
        end_time_transform1 = time.time()
        print("time_transform1 = " + str(end_time_transform1 - start_time_transform1))

        start_time_transform2 = time.time()
        dutsn = {duts[k].get('id'): duts[k].get('name') for k in range(len(duts))}
        end_time_transform2 = time.time()
        print("time_transform2 = " + str(end_time_transform2 - start_time_transform2))

        start_time_transform3 = time.time()
        len_tstplnres2 = len(testplanres)
        for m in range(len_tstplnres2):
            if meas.get(testplanres[m].get('id')) is not None:
                testplanres_af.append(testplanres[m])
                duts_name.append(dutsn[meas[testplanres[m].get('id')]])
            else:
                duts_not_name.append(testplanres[m].get('id'))
        end_time_transform3 = time.time()
        print("time_transform3 = " + str(end_time_transform3 - start_time_transform3))

        return duts_name, testplanres_af

    try:
        try:
            time_db_scan_start = time.time()
            with connection.cursor() as cur:
                time_atedb_testplan_results_start = time.time()
                cur.execute("SELECT id, plate_number, batch_number, result, result_group, start_date, finish_date FROM atedb.testplan_results WHERE start_date BETWEEN \""
                            + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                testplanres = cur.fetchall()
                time_atedb_testplan_results_end = time.time()
                print("time_atedb_testplan_results = " + str(time_atedb_testplan_results_end - time_atedb_testplan_results_start))

                time_atedb_measurements_start = time.time()
                cur.execute("SELECT duts_id, testplan_results FROM atedb.measurements WHERE testplan_results BETWEEN \""
                            + str(testplanres[0].get('id')) + "\" AND \"" + str(testplanres[-1].get('id')) + "\" AND arm_id = " + str(arm_id))
                measurements = cur.fetchall()
                time_atedb_measurements_end = time.time()
                print("time_atedb_measurements = " + str(time_atedb_measurements_end - time_atedb_measurements_start))

                time_atedb_duts_start = time.time()
                cur.execute("SELECT id, name FROM atedb.duts")
                duts = cur.fetchall()
                time_atedb_duts_end = time.time()
                print("time_atedb_duts = " + str(time_atedb_duts_end - time_atedb_duts_start))
            time_db_scan_end = time.time()
            print("time_db_scan = " + str(time_db_scan_end - time_db_scan_start))

            duts_name, testplanres_af = findduts(testplanres, measurements, duts)

            time_calc_start = time.time()
            len_tstplnres = len(testplanres_af)
            for i in range(len_tstplnres):
                stat_donnes.append([i + 1, duts_name[i],
                                    testplanres_af[i].get('start_date').strftime("%H:%M:%S %d.%m.%Y"), testplanres_af[i].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                                    testplanres_af[i].get('batch_number'), testplanres_af[i].get('plate_number'),
                                    "ГОДЕН" if testplanres_af[i].get('result') == 0 else "БРАК", testplanres_af[i].get('result_group')])

            time_calc_end = time.time()
            print("time_calc = " + str(time_calc_end - time_calc_start))
            exec_status = 0
        except IndexError:
            exec_status = 0
            errindxdata = 1

    except Exception as e:
        print(e)
        exec_status = 1
        answer = True

        root = tk.Tk()
        root.withdraw()

        while answer is True and exec_status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")

            if answer:
                exec_status, connection = connectdb(initdonn)

            if exec_status == 0:
                stat_donnes = []

                try:
                    time_db_scan_start = time.time()
                    with connection.cursor() as cur:
                        time_atedb_testplan_results_start = time.time()
                        cur.execute(
                            "SELECT id, plate_number, batch_number, result, result_group, start_date, finish_date FROM atedb.testplan_results WHERE start_date BETWEEN \""
                            + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                        testplanres = cur.fetchall()
                        time_atedb_testplan_results_end = time.time()
                        print("time_atedb_testplan_results = " + str(time_atedb_testplan_results_end - time_atedb_testplan_results_start))

                        time_atedb_measurements_start = time.time()
                        cur.execute("SELECT duts_id, testplan_results FROM atedb.measurements WHERE arm_id = " + str(arm_id))
                        measurements = cur.fetchall()
                        time_atedb_measurements_end = time.time()
                        print("time_atedb_measurements = " + str(time_atedb_measurements_end - time_atedb_measurements_start))

                        time_atedb_duts_start = time.time()
                        cur.execute("SELECT id, name FROM atedb.duts")
                        duts = cur.fetchall()
                        time_atedb_duts_end = time.time()
                        print("time_atedb_duts = " + str(time_atedb_duts_end - time_atedb_duts_start))
                    time_db_scan_end = time.time()
                    print("time_db_scan = " + str(time_db_scan_end - time_db_scan_start))

                    duts_name, testplanres_af = findduts(testplanres, measurements, duts)

                    time_calc_start = time.time()
                    len_tstplnres = len(testplanres_af)
                    for i in range(len_tstplnres):
                        stat_donnes.append([i + 1, duts_name[i],
                                            testplanres_af[i].get('start_date').strftime("%H:%M:%S %d.%m.%Y"),
                                            testplanres_af[i].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                                            testplanres_af[i].get('batch_number'), testplanres_af[i].get('plate_number'),
                                            "ГОДЕН" if testplanres_af[i].get('result') == 0 else "БРАК", testplanres_af[i].get('result_group')])

                    time_calc_end = time.time()
                    print("time_calc = " + str(time_calc_end - time_calc_start))
                    exec_status = 0
                except IndexError:
                    errindxdata = 1

        root.destroy()

    return stat_donnes, exec_status, errindxdata
