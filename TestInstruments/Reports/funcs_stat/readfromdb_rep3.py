def readfromdb_rep3(connection):
    duts = []
    stat_donnes = []

    def findres(resstat):
        if resstat == 0:
            status = 'ГОДЕН'
        else:
            status = "БРАК"
        return status

    def findduts(tst_id):
        duts_name = ''
        for j in range(0, len(measurements)):
            if tst_id == measurements[j].get('testplan_results'):
                for g in range(0, len(duts)):
                    if measurements[j].get('duts_id') == duts[g].get('id'):
                        duts_name = duts[g].get('name')
                        break
            elif duts_name != '':
                break
        return duts_name

    with connection.cursor() as cur:
        cur.execute("SELECT id, plate_number, batch_number, result, result_group, start_date, finish_date FROM atedb.testplan_results")
        testplanres = cur.fetchall()

        cur.execute("SELECT duts_id, testplan_results FROM atedb.measurements")
        measurements = cur.fetchall()

        # cur.execute("SELECT id, name FROM atedb.test_elements")
        # tstelem = cur.fetchall()

        cur.execute("SELECT id, name FROM atedb.duts")
        duts = cur.fetchall()

    for i in range(0, len(testplanres)):
        stat_donnes.append([i + 1, (lambda tst_id: findduts(tst_id))(testplanres[i].get('id')),
                            testplanres[i].get('start_date').strftime("%H:%M:%S %d.%m.%Y"), testplanres[i].get('finish_date').strftime("%H:%M:%S %d.%m.%Y"),
                            testplanres[i].get('batch_number'), testplanres[i].get('plate_number'),
                            (lambda resstat: findres(resstat))(testplanres[i].get('result')), testplanres[i].get('result_group')])
    return stat_donnes
