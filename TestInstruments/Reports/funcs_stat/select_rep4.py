def select_rep4(connection, initdonn, data_start_vvd, data_finish_vvd, arm_id):
    from funcs_stat import connectdb
    from tkinter import Toplevel
    from tkinter import messagebox as mb

    try:
        with connection.cursor() as cur:
            cur.execute("SELECT id, start_date, finish_date, batch_number, plate_number FROM atedb.testplan_results WHERE start_date BETWEEN \""
                        + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
            testplanres = cur.fetchall()

            # определение первого индекса выбранного диапазона строк таблицы measurements
            cur.execute("SELECT id FROM atedb.measurements WHERE testplan_results = " + str(testplanres[0].get('id')))
            numoneid = cur.fetchall()[0].get('id')

            # определение последнего индекса выбранного диапазона строк таблицы measurements
            flag_poiska = 0
            n = -1
            while flag_poiska != 1 and n > -10:
                cur.execute("SELECT id FROM atedb.measurements WHERE testplan_results = " + str(testplanres[n].get('id')) + " AND arm_id = " + str(arm_id)
                            + " ORDER BY id DESC LIMIT 1")
                lastid = cur.fetchall()

                if len(lastid) != 0:
                    numlastid = lastid[0].get('id')
                    flag_poiska = 1
                else:
                    n -= 1

            numsid = [numoneid, numlastid]
            if numsid[1] - numsid[0] < 100:
                amountid = 0
            elif 100 < numsid[1] - numsid[0] < 10001:
                amountid = 1
            elif numsid[1] - numsid[0] > 10001:
                amountid = 2

            exec_status = 0
    except Exception as e:
        print(e)
        exec_status = 1

        dialog = Toplevel()
        dialog.withdraw()

        answer = True
        while answer is True and exec_status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")
            if answer:
                exec_status, connect = connectdb(initdonn)

            if exec_status == 0:
                with connection.cursor() as cur:
                    cur.execute("SELECT id, start_date, finish_date, batch_number, plate_number FROM atedb.testplan_results WHERE start_date BETWEEN \""
                                + str(data_start_vvd) + "\" AND \"" + str(data_finish_vvd) + "\" AND arm_id = " + str(arm_id))
                    testplanres = cur.fetchall()

                    # определение первого индекса выбранного диапазона строк таблицы measurements
                    cur.execute("SELECT id FROM atedb.measurements WHERE testplan_results = " + str(testplanres[0].get('id')))
                    numoneid = cur.fetchall()[0].get('id')

                    # определение последнего индекса выбранного диапазона строк таблицы measurements
                    flag_poiska = 0
                    n = -1
                    while flag_poiska != 1 and n > -10:
                        cur.execute("SELECT id FROM atedb.measurements WHERE testplan_results = " + str(testplanres[n].get('id')) + " AND arm_id = " + str(arm_id)
                                    + " ORDER BY id DESC LIMIT 1")
                        lastid = cur.fetchall()

                        if len(lastid) != 0:
                            numlastid = lastid[0].get('id')
                            flag_poiska = 1
                        else:
                            n -= 1

                    numsid = [numoneid, numlastid]
                    if numsid[1] - numsid[0] < 100:
                        amountid = 0
                    elif 100 < numsid[1] - numsid[0] < 10001:
                        amountid = 0
                    elif numsid[1] - numsid[0] > 10001:
                        amountid = 2

        dialog.destroy()

    return numsid, amountid, exec_status
