def readfromdb_pred_time(connection, initdonn, arm_id):
    import tkinter as tk
    from funcs_stat import connectdb
    from tkinter import messagebox as mb

    exec_status_pred = 0
    errarmiddata = 0

    stat_donnes = []
    try:
        try:
            with connection.cursor() as cur:
                cur.execute("SELECT start_date FROM atedb.testplan_results WHERE arm_id = " + str(arm_id) + " LIMIT 1")
                stat_donnes.append(str(cur.fetchall()[0].get('start_date').strftime("%H:%M:%S %d.%m.%Y")))

                cur.execute("SELECT id, start_date FROM atedb.testplan_results WHERE arm_id = " + str(arm_id) + " ORDER BY id DESC LIMIT 1")
                stat_donnes.append(str(cur.fetchall()[0].get('start_date').strftime("%H:%M:%S %d.%m.%Y")))

                cur.execute("SELECT name FROM atedb.arm WHERE id = " + str(arm_id))
                name_arm = cur.fetchall()[0].get('name')
        except IndexError:
            errarmiddata = 1

    except Exception as e:
        print(e)
        exec_status = 1
        answer = True

        root = tk.Tk()
        root.withdraw()

        while answer is True and exec_status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")

            if answer:
                exec_status_pred, connection = connectdb(initdonn)

            if exec_status_pred == 0:
                try:
                    with connection.cursor() as cur:
                        cur.execute("SELECT start_date FROM atedb.testplan_results WHERE arm_id = " + str(arm_id) + " LIMIT 1")
                        stat_donnes.append(str(cur.fetchall()[0].get('start_date').strftime("%H:%M:%S %d.%m.%Y")))

                        cur.execute("SELECT id, start_date FROM atedb.testplan_results WHERE arm_id = " + str(arm_id) + " ORDER BY id DESC LIMIT 1")
                        stat_donnes.append(str(cur.fetchall()[0].get('start_date').strftime("%H:%M:%S %d.%m.%Y")))

                        cur.execute("SELECT name FROM atedb.arm WHERE id = " + str(arm_id))
                        name_arm = cur.fetchall()[0].get('name')
                except IndexError:
                    errarmiddata = 1

        root.destroy()

    return stat_donnes, name_arm, exec_status_pred, errarmiddata
