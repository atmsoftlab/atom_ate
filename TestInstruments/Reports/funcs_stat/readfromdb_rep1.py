def readfromdb_rep1(connection, max_col_groupn):
    import json
    testplanres = duts = []
    stat_donnes = []

    def findres(clasaftertestexec):
        if clasaftertestexec != '':
            status = 'ГОДЕН'
        else:
            status = "БРАК"
        return status

    # def findnametest(test_id):
    #     for k in range(0, len(tstelem)):
    #         if test_id == tstelem[k].get('id'):
    #             break
    #     return tstelem[k].get('name')

    def findduts(duts_id):
        for j in range(0, len(duts)):
            if duts_id == duts[j].get('id'):
                break
        return duts[j].get('name')

    def findtstplndonnes(testplan_results):
        for n in range(0, len(testplanres)):
            if testplan_results == testplanres[n].get('id'):
                break
        return {'start_date': testplanres[n].get('start_date').strftime("%H:%M:%S %d.%m.%Y"),
                'batch_number': testplanres[n].get('batch_number'),
                'plate_number': testplanres[n].get('plate_number')}

    def lenmantis(result):
        if result is not None:
            if result.find('.') != -1:
                if (len(result) - result.find('.')) > 5:
                    nresult = result[0:result.find('.') + 5]
                else:
                    nresult = result
            else:
                nresult = result
        else:
            nresult = result
        return nresult

    def findnormes(mcol_groupn, normatable):
        massnormes = []
        for j in range(0, mcol_groupn):
            if j < len(normatable):
                massnormes.extend([normatable[j].get('Min'), normatable[j].get('Max')])
            else:
                massnormes.extend(['-', '-'])
        return massnormes

    with connection.cursor() as cur:
        cur.execute("SELECT id, plate_number, batch_number, start_date FROM atedb.testplan_results")
        testplanres = cur.fetchall()

        cur.execute("SELECT value, test_id, duts_id, testplan_results FROM atedb.measurements")
        measurements = cur.fetchall()

        # cur.execute("SELECT id, name FROM atedb.test_elements")
        # tstelem = cur.fetchall()

        cur.execute("SELECT id, name FROM atedb.duts")
        duts = cur.fetchall()

    zet = 0
    for h in range(0, len(measurements)):
        massvalue = json.loads(measurements[h].get('value'))
        for i in range(0, len(massvalue)):
            stat_donnes.append([len(stat_donnes) + 1, (lambda duts_id: findduts(duts_id))(measurements[h].get('duts_id')),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('start_date'),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('batch_number'),
                                (lambda testplan_results: findtstplndonnes(testplan_results))(measurements[h].get('testplan_results')).get('plate_number'),
                                massvalue[i].get('Name'), massvalue[i].get('Unit'), (lambda result: lenmantis(result))(massvalue[i].get('Value'))])
            stat_donnes[zet].extend((lambda mcol_groupn, normatable: findnormes(mcol_groupn, normatable))(max_col_groupn, massvalue[i].get('NormaTable')))
            stat_donnes[zet].extend([(lambda clasaftertestexec: findres(clasaftertestexec))(massvalue[i].get('ClassAfterTestExecute')),
                                     massvalue[i].get('ClassAfterTestExecute')])
            zet = zet + 1
    return stat_donnes
