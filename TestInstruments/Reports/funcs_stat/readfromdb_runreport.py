def readfromdb_runreport(connection, arm_id):

    with connection.cursor() as cur:
        cur.execute("SELECT fullname, path_to_script FROM atedb.reports WHERE arm_id = " + str(arm_id))
        info_reports = cur.fetchall()

    return info_reports
