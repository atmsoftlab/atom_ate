def savexlsxn_rep4(file_name, headers, stat_donnes):
    import os
    import psutil
    import time
    import xlsxwriter

    colmnname = []

    # Закрытие запущенного процесса EXCEL
    for proc in psutil.process_iter():
        if proc.name() == "EXCEL.EXE":
            os.system('taskkill /f /im EXCEL.EXE')
    time.sleep(3)

    # Удаление уже существующего файла
    if os.path.exists(file_name):
        os.remove(file_name)
        time.sleep(2)

    for i in range(0, 23):
        colmnname.append(chr(i + 97).upper())

    colmnwidth = [15, 15, 19, 19, 19, 19, 70, 80, 12, 15, 15, 15, 12]

    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()

    head_format_left = workbook.add_format()
    head_format_left.set_border(1)
    head_format_left.set_align('left')
    head_format_left.set_bold()
    head_format_left.set_bg_color('#e8e7e7')

    head_format_center = workbook.add_format()
    head_format_center.set_border(1)
    head_format_center.set_align('center')
    head_format_center.set_bold()
    head_format_center.set_bg_color('#e8e7e7')

    cellLeft_format = workbook.add_format()
    cellLeft_format.set_border(1)
    cellLeft_format.set_align('left')

    cellCenter_format = workbook.add_format()
    cellCenter_format.set_border(1)
    cellCenter_format.set_align('center')

    cellLeft_format_brak = workbook.add_format()
    cellLeft_format_brak.set_border(1)
    cellLeft_format_brak.set_align('left')
    cellLeft_format_brak.set_bg_color('#f2dedf')

    cellCenter_format_brak = workbook.add_format()
    cellCenter_format_brak.set_border(1)
    cellCenter_format_brak.set_align('center')
    cellCenter_format_brak.set_bg_color('#f2dedf')

    for i in range(0, len(headers)):
        if (i == 6) or (i == 7):
            worksheet.set_column(colmnname[i] + ":" + colmnname[i], colmnwidth[i])
            worksheet.write(0, i, headers[i], head_format_left)
        else:
            worksheet.set_column(colmnname[i] + ":" + colmnname[i], colmnwidth[i])
            worksheet.write(0, i, headers[i], head_format_center)

    for j in range(0, len(stat_donnes)):
        for n in range(0, len(stat_donnes[0])):
            if stat_donnes[j][10] == 'БРАК':
                if n == 0:
                    worksheet.write(j + 1, n, int(stat_donnes[j][n]), cellCenter_format_brak)
                elif (n == 6) or (n == 7):
                    worksheet.write(j + 1, n, stat_donnes[j][n], cellLeft_format_brak)
                # elif (n == 4) and stat_donnes[j][n].isdigit():
                #     worksheet.write(j + 1, n, int(stat_donnes[j][n]), cellCenter_format_brak)
                # elif (n == 5) and stat_donnes[j][n].isdigit():
                #     worksheet.write(j + 1, n, int(stat_donnes[j][n]), cellCenter_format_brak)
                elif n == 9 and str(stat_donnes[j][n]) != '' and str(stat_donnes[j][n]) != 'nan' and str(stat_donnes[j][n]) != 'None':
                    worksheet.write_number(j + 1, n, float(stat_donnes[j][n]), cellCenter_format_brak)
                else:
                    worksheet.write(j + 1, n, str(stat_donnes[j][n]), cellCenter_format_brak)
            else:
                if n == 0:
                    worksheet.write(j + 1, n, int(stat_donnes[j][n]), cellCenter_format)
                elif (n == 6) or (n == 7):
                    worksheet.write(j + 1, n, stat_donnes[j][n], cellLeft_format)
                # elif (n == 4) and stat_donnes[j][n].isdigit():
                #     worksheet.write(j + 1, n, int(stat_donnes[j][n]), cellCenter_format)
                # elif (n == 5) and stat_donnes[j][n].isdigit():
                #     worksheet.write(j + 1, n, int(stat_donnes[j][n]), cellCenter_format)
                elif n == 9 and str(stat_donnes[j][n]) != '' and str(stat_donnes[j][n]) != 'nan' and str(stat_donnes[j][n]) != 'None':
                    worksheet.write_number(j + 1, n, float(stat_donnes[j][n]), cellCenter_format)
                else:
                    worksheet.write(j + 1, n, str(stat_donnes[j][n]), cellCenter_format)

    workbook.close()
