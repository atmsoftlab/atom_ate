import tkinter as tk
import configparser
import threading
from tkinter import ttk
from tkinter import messagebox as mb
from tkcalendar import DateEntry
from funcs_stat import *

lent = []
sizes = []

lstb_donnes = []

colgoden = 0
colbrak = 0


# Исправление бага с отображением background в таблице - не удалять
def fixed_map(option):
    return [elm for elm in style.map('mystyle.Treeview', query_opt=option) if elm[:2] != ('!disabled', '!selected')]


def date_entry_selected(event):
    w = event.widget
    datfiltr_start = w.get_date()
    return datfiltr_start


def clrtime_start(time_start):
    if len(time_start.get()) != 0:
        time_start.delete(0, tk.END)


def statistics():
    global colgoden, colbrak, lstb_donnes

    colgoden = 0
    colbrak = 0

    status_bar.configure(text="Идет рассчет статистики ...")

    for d in range(0, len(lstb_donnes)):
        if lstb_donnes[d][6] == 'БРАК':
            colbrak = colbrak + 1
        else:
            colgoden = colgoden + 1
    status_bar.configure(text="Данные успешно отфильтрованы. Статистика: ВСЕГО РЕЗУЛЬТАТОВ ПО ИЗДЕЛИЮ - " + str(len(lstb_donnes))
                              + "  шт. / 100% :  ИЗ НИХ ГОДНЫХ - " + str(colgoden) + "  шт. / "
                              + str(0 if colgoden == 0 else round(colgoden / len(lstb_donnes) * 100, 3)) + "% ; ИЗ НИХ ЗАБРАКОВАННЫХ - " +
                              str(colbrak) + "  шт. / " + str(0 if colbrak == 0 else round(colbrak / len(lstb_donnes) * 100, 3)) + " %.")


# Функция сохранения данных в файл xlsx
def save2xlsx():
    global lstb_donnes

    from tkinter.filedialog import asksaveasfilename
    file_name = asksaveasfilename(defaultextension="*.xlsx", title="Укажите каталог и имя файла для сохранения файла отчетности",
                                  filetypes=[("Excel files", "*.xlsx *.xls")], initialdir="C:\\", initialfile="Report")
    status_bar.configure(text="Сохранение данных в файл " + file_name)

    if file_name != '':
        savexlsxn_rep3(file_name, cols, lstb_donnes)
    status_bar.configure(text="Данные успешно сохранены в файл " + file_name)


# Функция выхода их программы
def on_closing():
    status_bar.configure(text="Выход из программы ...")
    if mb.askokcancel("Выход", "Вы хотите выйти из программы?"):
        scores.destroy()


# Функция динамического изменения размеров окна
def onresize(event):
    if event.widget.winfo_name() == "!treeview":

        if scores.winfo_width() <= 1280:
            Fontsize_new = 7
        elif 1280 < scores.winfo_width() <= 1600:
            Fontsize_new = 8
        elif 1600 < scores.winfo_width() <= 1920:
            Fontsize_new = 10
        elif scores.winfo_width() > 1920:
            Fontsize_new = 13
        else:
            Fontsize_new = 14

        widthgrid_new = scores.winfo_width() - 20
        cols_new = ('№', 'Изделие', 'Дата запуска', 'Дата окончания', 'Партия', 'Шифр', 'Статус изделия', 'Группа изделия')
        colspercent_new = [5, 15, 15, 15, 15, 15, 10, 10]
        align_new = [tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER]
        onepercent_new = widthgrid_new / 100

        style.configure("mystyle.Treeview", highlightthickness=0, bd=0, font=('Calibri', str(Fontsize_new)))  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', str(Fontsize_new), 'bold'))  # Modify the font of the headings

        # set column headings
        for j in range(0, len(cols_new)):
            listBox.column(str(j), minwidth=int(colspercent_new[j] / 2 * onepercent), width=int(colspercent_new[j] * onepercent_new), anchor=align_new[j])
            listBox.heading(str(j), text=cols_new[j], anchor=align_new[j])


# Функция вывода данных из БД (временной фильтр)
def show():
    def calculate_filtr():
        global lstb_donnes
        lstb_donnes.clear()

        # Считывание параметров из БД
        connection2filtr = None
        initdonn2filtr, arm_id, stat2filtr = initdb(patchconfig)
        if stat2filtr == 0:
            status2filtr, connection2filtr = connectdb(initdonn2filtr)
            if status2filtr == 1:
                dop_window.destroy()
        elif stat2filtr == 1:
            mb.showwarning(title='Внимание!', message='Не найден файл AToM_ATE.exe.config по указанному пути')
        elif stat2filtr == 2:
            mb.showwarning(title='Внимание!', message='В файле AToM_ATE.exe.config отсутствует необходимая'
                                                      'информация для инициализации подключения к БД')

        # Задание даты и времени, конвертация
        data_start_vvd = (data_start.get().split('.')[2] + '-' + data_start.get().split('.')[1] +
                          '-' + data_start.get().split('.')[0] + ' '
                          + [("0" + sphour_strt.get()) if int(sphour_strt.get()) < 10 else sphour_strt.get()][0] + ":"
                          + [("0" + spmin_strt.get()) if int(spmin_strt.get()) < 10 else spmin_strt.get()][0] + ":"
                          + [("0" + spsec_strt.get()) if int(spsec_strt.get()) < 10 else spsec_strt.get()][0])

        data_finish_vvd = (data_finish.get().split('.')[2] + '-' + data_finish.get().split('.')[1] +
                           '-' + data_finish.get().split('.')[0] + ' '
                           + [("0" + sphour_end.get()) if int(sphour_end.get()) < 10 else str(sphour_end.get())][0] + ":"
                           + [("0" + spmin_end.get()) if int(spmin_end.get()) < 10 else spmin_end.get()][0] + ":"
                           + [("0" + spsec_end.get()) if int(spsec_end.get()) < 10 else spsec_end.get()][0])

        dop_window.destroy()

        # Считывание данных из базы
        numsid, amountid, exec_status = select_rep3(connection2filtr, initdonn2filtr, data_start_vvd, data_finish_vvd, arm_id)

        errindxdata = 0
        if exec_status == 0 and amountid == 0:
            lstb_donnes, exec_status, errindxdata = readfromdb_rep3_nv(connection2filtr, initdonn, data_start_vvd, data_finish_vvd, arm_id)

        elif exec_status == 0 and amountid > 0:
            exec_status, testplanres, duts, mass_measurements, breaking = readb_progress_rep3(connection2filtr, initdonn2filtr, data_start_vvd,
                                                                                              data_finish_vvd, arm_id, numsid, amountid)

            print("Size of testplanres: " + str(sys.getsizeof(testplanres)))
            print("Size of duts: " + str(sys.getsizeof(duts)))
            print("Size of mass_measurements: " + str(sys.getsizeof(mass_measurements)))

            if exec_status == 0 and breaking == 0:
                lstb_donnes, calc_breaking = calc_progress_rep3(testplanres, duts, mass_measurements)
                print("Size of lstb_donnes: " + str(sys.getsizeof(lstb_donnes)))
                print("lstb = " + str(len(lstb_donnes)))
                del testplanres, duts, mass_measurements

                if calc_breaking == 1:
                    lstb_donnes.clear()
            elif exec_status == 0 and breaking != 0:
                lstb_donnes.clear()
                status_bar.configure(text="Процесс загрузки и преобразования данных отменен пользователем")
                del testplanres, duts, mass_measurements

        if exec_status == 0 and errindxdata == 0 and len(lstb_donnes) != 0:
            connection2filtr.close()

            threading.Thread(target=statistics).start()

            lenprc = len(lstb_donnes)
            if lenprc > 2000:
                lenprc = 2000

            listBox.delete(*listBox.get_children())

            for n in range(0, lenprc):
                if lstb_donnes[n][6] == 'БРАК':
                    listBox.insert("", "end", values=(lstb_donnes[n][:]), tags=('BRAK',))
                else:
                    listBox.insert("", "end", values=(lstb_donnes[n][:]))
            print("Size of listBox: " + str(sys.getsizeof(listBox)))

            first_item.entryconfig(1, label="Сохранить отчет", state="normal")
            second_item.entryconfig(0, label="Фильтр по изделию", state="normal")
            dop_window.destroy()
        elif exec_status == 0 and errindxdata == 1 and len(lstb_donnes) == 0:
            connection2filtr.close()
            dop_window.destroy()

            answer = mb.askyesno(title="Внимание!", message="В указанном диапазоне даты не найдено ни одного результата для тестируемого изделия.\n Хотите "
                                                            "изменить диапазон поиска результатов?", icon="question")
            if answer:
                show()

    #  Разделение функции filtr_data и диалогового окна
    path2readini = os.path.abspath(os.path.dirname(sys.argv[0])).split(str(os.path.abspath(os.path.dirname(sys.argv[0])).split('TestInstruments')[-1]))[0]
    if os.path.exists(path2readini + "\\arm_id.ini"):  # Проверка существования ini файла с arm_id

        config = configparser.ConfigParser()
        config.read(path2readini + "\\arm_id.ini")

        if config.sections()[0] == 'MAIN':

            if 'arm_id' in config[config.sections()[0]]:
                arm_id = config[config.sections()[0]]['arm_id']

                dop_window = tk.Toplevel()
                dop_window.title("Фильтр по дате начала тестирования")
                dop_window.iconbitmap(str(os.path.abspath(os.path.dirname(sys.argv[0]))) + '\\atom.ico')

                # Gets the requested values of the height and widht.
                windowWidth = 650
                # windowWidth = root.winfo_reqwidth()
                windowHeight = 480
                # windowHeight = root.winfo_reqheight()

                # Gets both half the screen width/height and window width/height
                positionRight = int(dop_window.winfo_screenwidth() / 2 - windowWidth / 2)
                positionDown = int(dop_window.winfo_screenheight() / 2 - windowHeight / 2)

                # Positions the window in the center of the page.
                dop_window.geometry("{}x{}+{}+{}".format(windowWidth, windowHeight, positionRight, positionDown))
                dop_window.resizable(0, 0)

                initdonn, arm_id, stat = initdb(patchconfig)

                if stat == 0:
                    status, connection = connectdb(initdonn)
                    if status == 0:
                        # Считывание данных из базы
                        stat_donnes, name_arm, exec_status_pred, errarmiddata = readfromdb_pred_time(connection, initdonn, arm_id)

                        if exec_status_pred == 0 and errarmiddata == 0:
                            connection.close()

                            label_pojasn = tk.Label(dop_window, text="Установка АРМ: " + name_arm +
                                                                     "\n\nВыберите начальную и конечную даты фильтра по времени\n"
                                                                     "(отображение изделий только в выбранном диапазоне тестирования).\n"
                                                                     "Выбранные значения должны попадать в существующий диапазон дат БД\n(иначе"
                                                                     " подстановка значений по умолчанию - даты первого и последнего изделия): \n\n"
                                                                     "дата тестирования первого изделия (в базе данных или при сбросе) - "
                                                                     + stat_donnes[0].split(' ')[1] + "\nвремя тестирования (в базе данных или при сбросе) - "
                                                                     + stat_donnes[0].split(' ')[0] + "\n\n"
                                                                     + "дата тестирования последнего изделия (в базе данных или при сбросе) - "
                                                                     + stat_donnes[1].split(' ')[1] + "\nвремя тестирования (в базе данных или при сбросе) - "
                                                                     + stat_donnes[1].split(' ')[0] + "\n",
                                                    font=('Calibri', 12), justify=tk.LEFT, wraplength=630)
                            label_pojasn.grid(row=0, columnspan=7, padx=15, pady=5, sticky='w')

                            label_zapros1 = tk.Label(dop_window, text="Задайте дату и время начала выборки фильтра:",
                                                     font=('Calibri Bold', 12), justify=tk.CENTER, wraplength=630)
                            label_zapros1.grid(row=1, columnspan=7, padx=15, pady=5, sticky='ew')

                            data_start = DateEntry(dop_window, date_pattern='DD.MM.YYYY', font=('Calibri Bold', 12), justify=tk.CENTER)
                            data_start.delete(0, tk.END)
                            data_start.insert(0, stat_donnes[0].split(' ')[1])
                            data_start.grid(row=2, column=0, ipadx=2, ipady=2, padx=35, pady=5, sticky='e')
                            data_start.bind("<<DateEntrySelected>>", date_entry_selected)

                            sphour_strt = tk.Spinbox(dop_window, width=3, from_=0, to=23, font=('Calibri Bold', 12), justify=tk.CENTER)
                            sphour_strt.grid(row=2, column=1, padx=0)
                            sphour_strt.bind('<MouseWheel>', spinbox_mousewheel)
                            txthour_strt = tk.Label(dop_window, text='часов : ', font=('Calibri Bold', 12))
                            txthour_strt.grid(row=2, column=2, padx=0, sticky='w')

                            spmin_strt = tk.Spinbox(dop_window, width=3, from_=0, to=59, font=('Calibri Bold', 12), justify=tk.CENTER)
                            spmin_strt.grid(row=2, column=3, padx=0)
                            spmin_strt.bind('<MouseWheel>', spinbox_mousewheel)
                            txtmin_strt = tk.Label(dop_window, text='минут : ', font=('Calibri Bold', 12))
                            txtmin_strt.grid(row=2, column=4, padx=0, sticky='w')

                            spsec_strt = tk.Spinbox(dop_window, width=3, from_=0, to=59, font=('Calibri Bold', 12), justify=tk.CENTER)
                            spsec_strt.grid(row=2, column=5, padx=0)
                            spsec_strt.bind('<MouseWheel>', spinbox_mousewheel)
                            txtsec_strt = tk.Label(dop_window, text='секунд ', font=('Calibri Bold', 12))
                            txtsec_strt.grid(row=2, column=6, padx=0, sticky='w')

                            label_zapros2 = tk.Label(dop_window, text="Задайте дату и время окончания выборки фильтра:",
                                                     font=('Calibri Bold', 12), justify=tk.CENTER, wraplength=530)
                            label_zapros2.grid(row=3, columnspan=7, padx=15, pady=5, sticky='ew')

                            data_finish = DateEntry(dop_window, date_pattern='DD.MM.YYYY', font=('Calibri Bold', 12), justify=tk.CENTER)
                            data_finish.delete(0, tk.END)
                            data_finish.insert(0, stat_donnes[1].split(' ')[1])
                            data_finish.grid(row=4, column=0, ipadx=2, ipady=2, padx=35, pady=5, sticky='e')
                            data_finish.bind("<<DateEntrySelected>>", date_entry_selected)

                            sphour_end = tk.Spinbox(dop_window, width=3, from_=0, to=23, font=('Calibri Bold', 12), justify=tk.CENTER)
                            sphour_end.grid(row=4, column=1, padx=5)
                            sphour_end.bind('<MouseWheel>', spinbox_mousewheel)
                            txthour_end = tk.Label(dop_window, text='часов : ', font=('Calibri Bold', 12))
                            txthour_end.grid(row=4, column=2, padx=5, sticky='w')

                            spmin_end = tk.Spinbox(dop_window, width=3, from_=0, to=59, font=('Calibri Bold', 12), justify=tk.CENTER)
                            spmin_end.grid(row=4, column=3, padx=5)
                            spmin_end.bind('<MouseWheel>', spinbox_mousewheel)
                            txtmin_end = tk.Label(dop_window, text='минут : ', font=('Calibri Bold', 12))
                            txtmin_end.grid(row=4, column=4, padx=5, sticky='w')

                            spsec_end = tk.Spinbox(dop_window, width=3, from_=0, to=59, font=('Calibri Bold', 12), justify=tk.CENTER)
                            spsec_end.grid(row=4, column=5, padx=5)
                            spsec_end.bind('<MouseWheel>', spinbox_mousewheel)
                            txtsec_end = tk.Label(dop_window, text='секунд ', font=('Calibri Bold', 12))
                            txtsec_end.grid(row=4, column=6, padx=5, sticky='w')

                            btn = tk.Button(dop_window, text='Применить указанные настройки и выйти в основное окно', font=('Calibri Bold', 12), command=calculate_filtr)
                            btn.grid(row=5, columnspan=7, pady=15, ipadx=10, ipady=1)

                            dop_window.transient()
                            dop_window.grab_set()
                            dop_window.focus_set()
                            dop_window.wait_window()
                        elif exec_status_pred == 0 and errarmiddata == 1:
                            mb.showinfo(title="Внимание!", message="В базе данных нет результатов для выбранного АРМа (arm_id = " + str(arm_id) +
                                                                   "). Проверьте правильность указанного arm_id и запустите отчет заново!", icon="question")
                    else:
                        exec_status_pred = 1
                        errarmiddata = 0
                elif stat == 1:
                    mb.showwarning(title='Внимание!', message='Не найден файл AToM_ATE.exe.config по указанному пути')
                elif stat == 2:
                    mb.showwarning(title='Внимание!', message='В файле AToM_ATE.exe.config отсутствует необходимая'
                                                              'информация для инициализации подключения к БД')
            else:
                mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: в секции MAIN отсутствует ключ arm_id. '
                                                                                             'Программа завершает работу.', icon='error')
        else:
            mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: отсутствует секция MAIN в конфигурационном ini-файле arm_id.ini. '
                                                                                         'Программа завершает работу.', icon='error')
    else:
        mb.showerror(title='Отсутствует ini файл arm_id ...', message='Ошибка: отсутствует конфигурационный ini-файл arm_id.ini. '
                                                                      'Программа завершает работу.', icon='error')

    return exec_status_pred, errarmiddata


# Функция фильтра сортировки по изделию
def filtr_duts():
    global lstb_donnes

    def find_duts():
        global lstb_donnes

        sum_izdel = []
        for g in range(0, len(vars_duts)):
            sum_izdel.append(vars_duts[g].get())

        spisok_nondelelem = []
        if (sum(sum_izdel) != 0) and (sum(sum_izdel) != len(sum_izdel)):
            for s in range(0, len(vars_duts)):
                if vars_duts[s].get() == 1:
                    spisok_nondelelem.append(name_izdelie[s])

            lstb_donnes_copy = lstb_donnes[:]
            lstb_donnes.clear()
            for d in range(0, len(lstb_donnes_copy)):
                for w in range(0, len(spisok_nondelelem)):
                    if lstb_donnes_copy[d][1] == spisok_nondelelem[w]:
                        lstb_donnes.append(lstb_donnes_copy[d])
                        break

            threading.Thread(target=statistics).start()

            lenprc = len(lstb_donnes)
            if lenprc > 2000:
                lenprc = 2000

            # Вывод отфильтрованного массива на экран
            listBox.delete(*listBox.get_children())
            for s in range(0, lenprc):
                if lstb_donnes[s][6] == 'БРАК':
                    lstb_donnes[s][0] = s + 1
                    listBox.insert("", "end", values=(lstb_donnes[s][:]), tags=('BRAK',))
                else:
                    lstb_donnes[s][0] = s + 1
                    listBox.insert("", "end", values=(lstb_donnes[s][:]))
        duts_window.destroy()

    #  Разделение функции filtr_duts и диалогового окна

    duts_window = tk.Toplevel()
    duts_window.title("Фильтр по изделию")
    duts_window.iconbitmap(str(os.path.abspath(os.path.dirname(sys.argv[0]))) + '\\atom.ico')

    # Gets the requested values of the height and widht.
    windowWidth = 400
    # windowWidth = root.winfo_reqwidth()
    windowHeight = 500
    # windowHeight = root.winfo_reqheight()

    # Gets both half the screen width/height and window width/height
    positionRight = int(duts_window.winfo_screenwidth() / 2 - windowWidth / 2)
    positionDown = int(duts_window.winfo_screenheight() / 2 - windowHeight / 2)

    # Positions the window in the center of the page.
    duts_window.geometry("{}x{}+{}+{}".format(windowWidth, windowHeight, positionRight, positionDown))
    duts_window.resizable(0, 0)

    # Функция поиска экземпляров типов изделия и размещение в treeview для предоставления выбора
    lstb_izdelie = []
    for h in range(0, len(lstb_donnes)):
        lstb_izdelie.append(lstb_donnes[h][1])

    # Поиск уникальных значений
    lstb_izdelie.sort()
    ind = 0
    flag = 1
    name_izdelie = []
    while flag > 0:
        if ind == len(lstb_izdelie):
            flag = 0
        else:
            name_izdelie.append(lstb_izdelie[ind])
            ind = ind + lstb_izdelie.count(lstb_izdelie[ind])

    label_pojasn = tk.Label(duts_window, text="Выберите из списка какое издели(е/я) необходимо отобразить в отчете:",
                            font=('Calibri', 12), justify=tk.LEFT, wraplength=380)
    label_pojasn.grid(row=0, column=0, padx=16, pady=5)

    checklist_duts = tk.Text(duts_window, width=44, height=23)
    checklist_duts.grid(row=1, column=0)

    # Создание скролбара по оси Y
    verscrlbar_duts = ttk.Scrollbar(duts_window, orient="vertical")
    verscrlbar_duts.grid(row=1, column=0, sticky=tk.E + tk.NS)
    verscrlbar_duts.config(command=checklist_duts.yview)
    checklist_duts.configure(yscrollcommand=verscrlbar_duts.set)

    vars_duts = []
    for f in range(0, len(name_izdelie)):
        var = tk.IntVar()
        vars_duts.append(var)
        chckbtn_duts = tk.Checkbutton(checklist_duts, text=name_izdelie[f], variable=var, onvalue=1, offvalue=0,
                                      font=['Calibri Bold', 12], bg='white', overrelief='groove', cursor='hand2')
        checklist_duts.window_create("end", window=chckbtn_duts)
        checklist_duts.insert("end", "\n")
    checklist_duts.configure(state="disabled")

    btn = tk.Button(duts_window, text='Применить указанные настройки \n и выйти в основное окно', font=('Calibri Bold', 11), command=find_duts)
    btn.grid(row=3, column=0, pady=11)

    duts_window.transient()
    duts_window.grab_set()
    duts_window.focus_set()
    duts_window.wait_window()


def motion(event):
    global lstb_donnes
    w = event.widget

    if isinstance(w, ttk.Scrollbar):
        lstb_strt_value = []
        lstb_last_value = []
        # Считывание последнего индекса из таблицы:
        child_part = listBox.get_children()
        lstb_strt_value.append(list(listBox.item(child_part[0], option="values")))
        lstb_last_value.append(list(listBox.item(child_part[-1], option="values")))
        strt_index = int(lstb_strt_value[0][0])
        last_index = int(lstb_last_value[0][0])

        if w.get()[1] > 0.9 and last_index >= 2000:
            if last_index + 1500 <= len(lstb_donnes):
                for h in range(0, 1500):
                    listBox.delete(child_part[h])
                    if lstb_donnes[h + last_index][6] == 'БРАК':
                        listBox.insert("", "end", values=(lstb_donnes[h + last_index][:]), tags=('BRAK',))
                    else:
                        listBox.insert("", "end", values=(lstb_donnes[h + last_index][:]))
            else:
                for h in range(0, len(lstb_donnes) - last_index):
                    listBox.delete(child_part[h])
                    if lstb_donnes[h + last_index][6] == 'БРАК':
                        listBox.insert("", "end", values=(lstb_donnes[h + last_index][:]), tags=('BRAK',))
                    else:
                        listBox.insert("", "end", values=(lstb_donnes[h + last_index][:]))

        if w.get()[1] < 0.1:
            if strt_index - 1500 >= 1:
                for n in range(0, 1500):
                    listBox.delete(child_part[500 + n])
                    if lstb_donnes[strt_index - n][6] == 'БРАК':
                        listBox.insert("", 0, values=(lstb_donnes[strt_index - n][:]), tags=('BRAK',))
                    else:
                        listBox.insert("", 0, values=(lstb_donnes[strt_index - n][:]))
            elif strt_index > 1:
                for n in range(0, strt_index + 1):
                    listBox.delete(child_part[500 + n])
                    if lstb_donnes[strt_index - n][6] == 'БРАК':
                        listBox.insert("", 0, values=(lstb_donnes[strt_index - n][:]), tags=('BRAK',))
                    else:
                        listBox.insert("", 0, values=(lstb_donnes[strt_index - n][:]))


def spinbox_mousewheel(event):
    t = event.widget
    if isinstance(t, tk.Spinbox):
        if event.num == 5 or event.delta == -120:
            t.invoke('buttondown')
        elif event.num == 4 or event.delta == 120:
            t.invoke('buttonup')


# -- ОСНОВНОЙ КОД --
patchconfig, var_zapuska = getpath2pathconf()

if patchconfig != '':

    scores = tk.Tk()
    scores.iconbitmap(str(os.path.abspath(os.path.dirname(sys.argv[0]))) + '\\atom.ico')
    scores.title('Report3 - Cтатистика для конкретного изделия за выбранный период: статус изделия плюс количество и процентное соотношения брака к годным')
    scores.geometry('{}x{}'.format(scores.winfo_screenwidth(), scores.winfo_screenheight()))
    scores.state('zoomed')
    scores.minsize(1280, 800)

    lstb_donnes = []

    if scores.winfo_screenwidth() <= 1280:
        scores.resizable(0, 0)
        Fontsize = 7
    elif 1280 < scores.winfo_screenwidth() <= 1600:
        Fontsize = 8
    elif 1600 < scores.winfo_screenwidth() <= 1920:
        Fontsize = 10
    elif scores.winfo_screenwidth() > 1920:
        Fontsize = 13
    else:
        Fontsize = 14

    main_menu = tk.Menu(scores)
    scores.configure(menu=main_menu)

    first_item = tk.Menu(main_menu, tearoff=0)
    main_menu.add_cascade(label="File", menu=first_item)
    first_item.add_command(label="Считать данные / Сброс фильтров", command=show)
    first_item.add_command(label="Сохранить отчет", command=save2xlsx, state="disabled")
    first_item.add_command(label="Выход", command=on_closing)
    second_item = tk.Menu(main_menu, tearoff=0)
    main_menu.add_cascade(label="Filtres", menu=second_item)
    second_item.add_command(label="Фильтр по изделию", command=filtr_duts, state="disabled")

    status_bar = tk.Label(scores, relief=tk.SUNKEN, anchor=tk.W, text="")
    status_bar.pack(side=tk.BOTTOM, fill='x')

    widthgrid = scores.winfo_screenwidth() - 20

    # Создание Treeview с 8 столбцами (исходные константы)
    cols = ('№', 'Изделие', 'Дата запуска', 'Дата окончания', 'Партия', 'Шифр', 'Статус изделия', 'Группа изделия')
    colspercent = [5, 15, 15, 15, 15, 15, 10, 10]
    align = [tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER, tk.CENTER]
    onepercent = widthgrid / 100

    # Стили для таблицы
    style = ttk.Style()
    style.configure("mystyle.Treeview", highlightthickness=0, bd=0, font=('Calibri', str(Fontsize)))  # Modify the font of the body
    style.configure("mystyle.Treeview.Heading", font=('Calibri', str(Fontsize), 'bold'))  # Modify the font of the headings
    style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders
    style.map('mystyle.Treeview', background=fixed_map('background'))

    # Настройка дерева тривьев
    listBox = ttk.Treeview(scores, columns=cols, show='headings', style="mystyle.Treeview", selectmode="none")
    listBox.tag_configure('BRAK', background='#f2dedf')

    # set column headings
    for i in range(0, len(cols)):
        listBox.column(str(i), minwidth=int((colspercent[i] / 2) * onepercent), stretch=False, width=int(colspercent[i] * onepercent), anchor=align[i])
        listBox.heading(str(i), text=cols[i], anchor=align[i])

    # Создание скролбара по оси Y
    verscrlbar = ttk.Scrollbar(scores, orient="vertical")
    verscrlbar.pack(side='right', expand=True, padx=2, fill='y')
    verscrlbar.config(command=listBox.yview)
    listBox.configure(yscrollcommand=verscrlbar.set)

    listBox.pack(side='left', expand=1, anchor='n', padx=2, pady=6, fill='y')

    # Запуск функции выбора и загрузки данных из БД
    exec_status_pred, errarmiddata = show()

    verscrlbar.bind('<B1-Motion>', lambda e: motion(e))
    verscrlbar.bind('<ButtonRelease-1>', lambda e: motion(e))

    if exec_status_pred == 0 and errarmiddata == 0:
        # Изменение размера окна
        scores.bind('<Configure>', onresize)

        # Закрытие окна через крестик
        scores.protocol("WM_DELETE_WINDOW", on_closing)
        scores.mainloop()
    else:
        scores.destroy()
