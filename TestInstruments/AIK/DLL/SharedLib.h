#include "extcode.h"
#ifdef __cplusplus
extern "C" {
#endif

/*!
 * XMLWorker
 */
int32_t __cdecl XMLWorker(double ArrayIn[], char path[], char results[], 
	int32_t len, int32_t len2);

MgErr __cdecl LVDLLStatus(char *errStr, int errStrLen, void *module);

void __cdecl SetExecuteVIsInPrivateExecutionSystem(Bool32 value);

#ifdef __cplusplus
} // extern "C"
#endif

