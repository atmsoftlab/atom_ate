# def runtest(path, numtest, regim, inputdata, colvihparam):
import ctypes
import numpy as np
import os

path = "E:\\Razrab_11\\ATOMUND\\DLL\\I2IS-1.dll"

newlib = ctypes.CDLL(path)

# Вариант сброса модулей
# newlib.HReset.restype = ctypes.c_int
# a = newlib.HReset()
# print(a)

# Вариант дискретного входа для Start
modulename = b'PXI1Slot8/port0/line4'

bufferstrmodname = (ctypes.c_char * len(modulename))(*modulename)

newlib.DAQ_NStart.restype = ctypes.c_int
newlib.DAQ_NStart.argtypes = [ctypes.POINTER(ctypes.c_char)]
const_start = newlib.DAQ_NStart(bufferstrmodname)

print('const_start = ' + str(const_start))

# 0 - нет сигнала, 1 - старт, 1000 - ошибка

# Вариант дискретного выхода для Start

PortFinTest = b'PXI1Slot8/port1/line2'
PortStatTest = b'PXI1Slot8/port1/line3'

BPortFinTest = (ctypes.c_char * len(PortFinTest))(*PortFinTest)
BPortStatTest = (ctypes.c_char * len(PortStatTest))(*PortStatTest)

# 1 - Годен, 0 - Брак
StatTest = 0

# Период импульса сигнала Годен/Брак
ImpPeriod = 0.1

newlib.DAQ_StatTest.restype = ctypes.c_int
newlib.DAQ_StatTest.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.POINTER(ctypes.c_char), ctypes.c_int, ctypes.c_double]
const_error = newlib.DAQ_StatTest(BPortFinTest, BPortStatTest, StatTest, ImpPeriod)

# 0 - нет ошибки, 1000 - ошибка
print('const_error = ' + str(const_error))



