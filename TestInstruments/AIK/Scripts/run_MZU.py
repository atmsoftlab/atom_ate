from sys import argv
from funcs import *
from tkinter import messagebox as mb

import time
import pathlib
import os
import keyboard
import configparser

patchconfig, var_zapuska = getpath2pathconf()  # Путь к файлу AToM_ATE.exe.config настроек для инфы подключения к БД

if var_zapuska == 0:
    # Входные аргументы для скрипта из ВУ
    path2savtxt = argv[0].split('run')[0]
    bufpath = argv[0].split('\\')
    path2readini = bufpath[0] + "\\" + bufpath[2]
    pathXML = argv[1]
    pathDll = "C:\\TestInstruments\\AIK\\DLL\\SharedLib.dll"
    regim = int(argv[2])
    test_plan_id = int(argv[3])
    duts_id = int(argv[4])
    batch_number = argv[5]
    loop = int(argv[6])

    if loop == 1:
        cyklzap = 1000000
    else:
        cyklzap = 1

elif var_zapuska == 1:
    # Для отладки в PyCharm
    path2savtxt = "C:\\TestInstruments\\AIK\\Scripts"
    path2readini = "C:\\TestInstruments"
    pathXML = "C:\\TestInstruments\\AIK\\testLibXML\\somePrg.xml"
    pathDll = "C:\\TestInstruments\\AIK\\DLL\\SharedLib.dll"
    regim = 0
    test_plan_id = 52
    duts_id = 10
    batch_number = "new_partie105"
    loop = 1

    if loop == 1:
        cyklzap = 1000000
    else:
        cyklzap = 1


status = 0
stattest = 0
testparams = []
testresults = []

if pathlib.Path(path2savtxt + '\\pause.pause').exists():
    os.remove(path2savtxt + '\\pause.pause')

if pathlib.Path(path2savtxt + '\\stoppause.pause').exists():
    os.remove(path2savtxt + '\\stoppause.pause')

if os.path.exists(path2readini + "\\arm_id.ini"):  # Проверка существования ini файла с arm_id

    config = configparser.ConfigParser()
    config.read(path2readini + "\\arm_id.ini")

    if config.sections()[0] == 'MAIN':

        if 'arm_id' in config[config.sections()[0]]:
            arm_id = config[config.sections()[0]]['arm_id']

            connection = None
            plate_number = ''

            err_status = 0  # убрать если запуск на железе
            flag_regim_brak = 0  # Первоначальный сброс флага брака

            # patchconfig, var_zapuska = getpath2pathconf()  # Путь к файлу AToM_ATE.exe.config настроек для инфы подключения к БД

            if patchconfig != '':
                # Функция выдачи интерактивного диалога с запросом "Партии" и "Шифра микрухи" (tkinter)
                platenuminstr = interwindow()
                if len(platenuminstr) != 0:
                    plate_number = int(platenuminstr[0])
                else:
                    flag_stop = 1  # Отмена запуска через окно ввода шифра микросхемы/кристалла

            if patchconfig != '' and plate_number != '':
                # Считывание данных из БД
                initdonn, stat = initdb(patchconfig)
                print('initdonn = ' + str(initdonn))

                if stat == 0:
                    status, connection = connectdb(initdonn)
                    if status == 0:
                        testresults, test_id, status = readfromdb_testres(connection, initdonn, duts_id, test_plan_id)
                        if status == 0:
                            testparams, status = readfromdb_testprms(connection, initdonn, duts_id, test_plan_id)
                        connection.close()
                elif stat == 1:
                    mb.showwarning(title='Внимание!', message='Не найден файл AToM_ATE.exe.config по указанному пути')
                elif stat == 2:
                    mb.showwarning(title='Внимание!', message='В файле AToM_ATE.exe.config отсутствует необходимая информация для инициализации подключения к БД')

                colbrak = 0  # переменная для сохранения количества браков
                colcykl = 0  # переменная количества циклов запуска

                if status == 0:
                    flag_stop = 0

                    while flag_stop != 1 and colcykl < cyklzap:
                        # Не удалять!!! Программный косяк иначе будет
                        keyboard.is_pressed('esc')

                        const_pfi_start = 0
                        colcykl += 1

                        while const_pfi_start == 0 and flag_stop == 0:
                            const_pfi_start = 1
                            # const_pfi_start = pfi_start(pathDll)  -  дискрет "СТАРТ"

                            file_pause = pathlib.Path(path2savtxt + '\\pause.pause')
                            file_stoppause = pathlib.Path(path2savtxt + '\\stoppause.pause')
                            file_stoptest = pathlib.Path(path2savtxt + '\\stoptest.stoptest')

                            if os.path.exists(path2savtxt + '\\stoptest.stoptest') is True and keyboard.is_pressed('esc') is True:
                                flag_stop = 1
                                if var_zapuska == 0:
                                    f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
                                else:
                                    f = sys.stdout
                                print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Нажата клавиша СТОП - тест-программа остановлена!\n", file=f)
                                if var_zapuska == 0:
                                    f.close()
                                time.sleep(1)
                            elif file_pause.exists():
                                h = 0
                                while file_pause.exists() and h < 7200:
                                    if h == 0:
                                        if var_zapuska == 0:
                                            f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
                                        else:
                                            f = sys.stdout
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) +
                                              " - Нажата клавиша ПАУЗА - тест-программа в ожидании продолжения(СТАРТ)/сброса(СТОП)!\n", file=f)
                                        if var_zapuska == 0:
                                            f.close()
                                            time.sleep(1)
                                    if file_stoptest.exists() or keyboard.is_pressed('esc') is True:
                                        flag_stop = 1
                                        if var_zapuska == 0:
                                            f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
                                        else:
                                            f = sys.stdout
                                        print("\nthisistrueresults DONE!\n", file=f)
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) +
                                              " - Нажата клавиша СТОП - тест-программа остановлена!\n", file=f)
                                        if var_zapuska == 0:
                                            f.close()
                                        if file_pause.exists():
                                            os.remove(path2savtxt + '\\pause.pause')
                                        break
                                    if file_stoppause.exists() and h < 7200:
                                        os.remove(path2savtxt + '\\pause.pause')
                                        os.remove(path2savtxt + '\\stoppause.pause')
                                        if var_zapuska == 0:
                                            f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
                                        else:
                                            f = sys.stdout
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Пауза снята - продолжение тестирования!\n", file=f)
                                        if var_zapuska == 0:
                                            f.close()
                                            time.sleep(1)
                                        break
                                    time.sleep(1)
                                    h += 1

                        if flag_stop != 1:
                            StatArr = []
                            measurements = []
                            resarray = []
                            results = []

                            tofile2 = ''

                            if var_zapuska == 0:
                                f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
                            else:
                                f = sys.stdout

                            if os.path.exists(path2savtxt + '\\pause.pause') is not True and os.path.exists(path2savtxt + '\\stoptest.stoptest') is not True \
                                    and keyboard.is_pressed('esc') is not True and flag_stop != 1 and err_status == 0:

                                # запись номера тестируемой схемы
                                print("\nplate number = " + str(plate_number) + "\n", file=f)

                                # замер времени перед началом запуска тест-программы
                                start_date = time.strftime("%Y-%m-%d %H:%M:%S")
                                print("start_date = " + str(start_date), file=f)

                                for n in range(0, len(testparams)):
                                    time.sleep(0.5)
                                    if os.path.exists(path2savtxt + '\\stoptest.stoptest') is True or keyboard.is_pressed('esc') or err_status != 0:
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Прерывание выполнения тест-программы на тесте "
                                              + str(testresults[n][0].get('TName')) + "\n", file=f)
                                        flag_stop = 1
                                        break
                                    else:
                                        # runtest - функция запуска теста-функции dll на выполнение - описание входных параметров:
                                        # pathDll = "C:\\TestInstruments\\AIK\\DLL\\SharedLib.dll";
                                        # "XMLPath" - "C:\\TestInstruments\\AIK\\XML\\diod_vac.xml";
                                        # "FName" - XMLWorker; "InputParamsValue" - массив входных данных;
                                        # "bufferSize" - количество символов для вывода строки ошибок из dll.
                                        # results, iserror, deskerror = runtest(pathDll, testparams[n][0].get('InputParamsValue')[0], testparams[n][0].get('FName'), 50000)
                                        print("testparams[n][0].get('InputParamsValue')[0] = " + str(testparams[n][0].get('InputParamsValue')[0]))

                                        results, iserror, deskerror = runtest(pathDll, pathXML+"\\"+testparams[n][0].get('FName'), [float(i) for i in testparams[n][0].get('InputParamsValue')], 50000)
                                        if iserror != 0:
                                            print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Прерывание выполнения тест-программы: ошибка "
                                                  + str(deskerror) + "\n", file=f)
                                            print("\nthisistrueresults DONE!\n", file=f)
                                            flag_stop = 1
                                            err_status = iserror
                                            break
                                        else:
                                            StatArr = sorting(results, testresults, n)
                                            resarray.append(StatArr)
                                            if regim != 0 and StatArr[2].get('Status') == 'БРАК':
                                                colbrak += 1
                                                if colbrak == regim:
                                                    print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Прерывание выполнения тест-программы по "
                                                          + str(colbrak) + " браку" + "\n", file=f)
                                                    flag_stop = 1
                                                    break

                                # запуск классификации результирующего массива выходных данных
                                test_plan_status, result_group = classification(resarray)

                                # замер времени окончания выполения тест-программы
                                finish_date = time.strftime("%Y-%m-%d %H:%M:%S")
                                print("finish_date = " + str(finish_date) + "\n", file=f)

                                # Подготовка массива для записи в первую таблицу БД - testplan_results
                                testplan_results_donnes = [plate_number, batch_number, test_plan_status, result_group, start_date, finish_date]

                                status, connection = connectdb(initdonn)
                                if status == 0:
                                    # запись результатов testplan_results_donnes в БД
                                    testplan_results, status = write2db_tpr(connection, initdonn, testplan_results_donnes, arm_id)

                                    if status == 0:
                                        # Подготовка данных для записи во вторую таблицу БД - measurements
                                        measurements, resarray, status = preparation(connection, initdonn, resarray, test_plan_id, duts_id, testplan_results, arm_id)

                                        if status == 0:
                                            if regim == 0:
                                                for i in range(0, len(resarray)):
                                                    tofile2 = tofile2 + str("[green]" if resarray[i][2].get('Status') == "ГОДЕН" else "[red]") + \
                                                              "TName = " + str(resarray[i][0].get('TName')) + ";  БРАК = " + str(resarray[i][0].get('БРАК')) + \
                                                              ';  A = ' + str(resarray[i][0].get('A')) + ';  СТАТУС = ' + str(resarray[i][2].get('Status')) + "\n"
                                                    for j in range(0, len(measurements[i][0])):
                                                        if measurements[i][0][j].get('Unit') is None:
                                                            tofile2 = tofile2 + measurements[i][0][j].get('Name') + ": " + \
                                                                      str(measurements[i][0][j].get('Value')) + "\n"
                                                        else:
                                                            tofile2 = tofile2 + measurements[i][0][j].get('Name') + ", " + measurements[i][0][j].get('Unit') + ": " + \
                                                                  str(measurements[i][0][j].get('Value')) + "\n"

                                                        if j == len(measurements[i][0])-1:
                                                            tofile2 = tofile2 + "\n"

                                            else:
                                                for i in range(0, len(resarray)):
                                                    tofile2 = tofile2 + str("[green]" if resarray[i][2].get('Status') == "ГОДЕН" else "[red]") + \
                                                              "TName = " + str(resarray[i][0].get('TName')) + ";  БРАК = " + str(resarray[i][0].get('БРАК')) + \
                                                              ';  A = ' + str(resarray[i][0].get('A')) + ';  СТАТУС = ' + str(resarray[i][2].get('Status')) + "\n"

                                            print(tofile2, file=f)
                                            print(str(str("[green]" if test_plan_status == 0 else "[red]")) + "Статус тест-плана: " +
                                                  str("ГОДЕН" if test_plan_status == 0 else "БРАК"), file=f)
                                            print("Группа по тест-плану: " + str("-" if result_group == "" else result_group) + "\n", file=f)

                                            # замер времени перед записью данных в датабазу
                                            date_before_save2db = time.strftime("%Y-%m-%d %H:%M:%S")
                                            print("date_before_save2db = " + str(date_before_save2db), file=f)

                                            if status == 0:
                                                # запись результатов measurements в БД
                                                status = write2db_meas(connection, initdonn, measurements)

                                                if status == 0:
                                                    # замер времени после записи данных в датабазу
                                                    date_after_save2db = time.strftime("%Y-%m-%d %H:%M:%S")
                                                    print("date_after_save2db = " + str(date_after_save2db) + "\n", file=f)

                                                    # закрытие соединения с БД
                                                    connection.close()
                                                    test_plan_status = 0  # убрать при работе с железом
                                                    if test_plan_status == 0:
                                                        stattest = 1
                                                    elif test_plan_status == 1:
                                                        stattest = 0

                                                    #const_error = pfi_state(pathDll, stattest)
                                                    # print("const_error = " + str(const_error))

                                                    plate_number = plate_number + 1

                                                else:
                                                    flag_stop = 1
                                                    print("Останов пользователя после неудачного соединения с базой данных\n", file=f)
                                            else:
                                                flag_stop = 1
                                                print("Останов пользователя после неудачного соединения с базой данных\n", file=f)
                                        else:
                                            flag_stop = 1
                                            print("Останов пользователя после неудачного соединения с базой данных\n", file=f)
                                    else:
                                        flag_stop = 1
                                        print("Останов пользователя после неудачного соединения с базой данных\n", file=f)
                                else:
                                    flag_stop = 1
                                    print("Останов пользователя после неудачного соединения с базой данных\n", file=f)
                            else:
                                flag_stop = 1
                                if pathlib.Path(path2savtxt + '\\stoptest.stoptest').exists():
                                    os.remove(path2savtxt + '\\stoptest.stoptest')
                            if (loop == 1 and flag_stop == 1) or loop == 0:
                                print("\nthisistrueresults DONE!\n", file=f)
                        if var_zapuska == 0:
                            f.close()
                        time.sleep(1)
                else:
                    if var_zapuska == 0:
                        f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
                    else:
                        f = sys.stdout
                    print("Остановка выполнения пользователем после неудачного соединения с базой данных! \n", file=f)
                    print("\nthisistrueresults DONE!\n", file=f)
                    f.close()
        else:
            mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: в секции MAIN отсутствует ключ arm_id. '
                                                                                         'Программа завершает работу.', icon='error')
    else:
        mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: отсутствует секция MAIN в конфигурационном ini-файле arm_id.ini. '
                                                                                     'Программа завершает работу.', icon='error')
else:
    mb.showerror(title='Отсутствует ini файл arm_id ...', message='Ошибка: отсутствует конфигурационный ini-файл arm_id.ini. '
                                                                  'Программа завершает работу.', icon='error')
    # if var_zapuska == 0:
    #     f = open(path2savtxt + '\\log_current_test.txt', 'w+', encoding='utf8')
    # else:
    #     f = sys.stdout
    # print("\nthisistrueresults DONE!\n", file=f)
    # f.close()
