def readfromdb(connection, initdonn):
    import tkinter as tk
    from funcs import connectdb
    from tkinter import messagebox as mb

    testplan_results = 0
    bid = []

    try:
        with connection.cursor() as cur:
            cur.execute("SELECT id FROM atedb.testplan_results")
            bufid = cur.fetchall()

            for i in range(0, len(bufid)):
                bid.append(bufid[i].get('id'))
            testplan_results = max(bid)

        status = 0

    except Exception as e:
        print(e)
        status = 1
        answer = True

        root = tk.Tk()
        root.withdraw()

        while answer is True and status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")
            if answer:
                status, connection = connectdb(initdonn)

            if status == 0:
                with connection.cursor() as cur:
                    cur.execute("SELECT id FROM atedb.testplan_results")
                    bufid = cur.fetchall()

                    for i in range(0, len(bufid)):
                        bid.append(bufid[i].get('id'))
                    testplan_results = max(bid)

        root.destroy()

    return testplan_results, status
