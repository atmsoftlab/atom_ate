def sorting(results, testresults, pornumtest):
    Results = []
    MaxGrArr = []
    Status = []
    FGroup = ''

    Group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    Results.append([float(i) for i in results])
    ResultsN = Results[0]

    for j in range(0, len(ResultsN)):  # цикл перемещения по количетву выходных параметров
        MaxGrArr.append(len(testresults[pornumtest][1][j].get('NormaTable')))  # определение макс количества групп
    MaxNumGroup = max(MaxGrArr)
    MaxGrArr.clear()

    StatArr = [{"TName": testresults[pornumtest][0].get('TName')}, {'results': results}, {'Status': ''}, {'FinalGroup': ''}]
    for m in range(0, MaxNumGroup):
        StatArr.append({"NameGroup " + Group[m]: []})  # массив словарей для групп выходных классов

    for j in range(0, len(ResultsN)):
        for k in range(0, len(testresults[pornumtest][1][j].get('NormaTable'))):  # цикл перемещения по количеству групп выходных параметров
            if (testresults[pornumtest][1][j].get('NormaTable')[k].get('Min') == testresults[pornumtest][1][j].get('NormaTable')[k].get('Max')) or\
                    ((testresults[pornumtest][1][j].get('NormaTable')[k].get('Min') == 0) and
                     (testresults[pornumtest][1][j].get('NormaTable')[k].get('Max') == 0)):
                StatArr[4 + k].get("NameGroup " + Group[k]).append('')
            elif testresults[pornumtest][1][j].get('NormaTable')[k].get('Min') < ResultsN[j] < testresults[pornumtest][1][j].get('NormaTable')[k].get('Max'):
                StatArr[4+k].get("NameGroup " + Group[k]).append('ГОДЕН')
            else:
                StatArr[4+k].get("NameGroup " + Group[k]).append('БРАК')

    for m in range(0, len(StatArr) - 4):
        Status.append('-')
        for k in range(0, len(StatArr[m + 4].get('NameGroup ' + Group[m]))):
            if StatArr[m + 4].get('NameGroup ' + Group[m])[k] == 'БРАК':
                Status.remove('-')
                Status.insert(m, 'БРАК')
                break
        if Status[m] == '-':
            Status.remove('-')
            Status.insert(m, 'ГОДЕН')

    for n in range(0, len(Status)):
        if Status[n] == 'ГОДЕН':
            StatArr[2].update({'Status': 'ГОДЕН'})
            break
        else:
            StatArr[2].update({'Status': 'БРАК'})

    for n in range(0, len(Status)):
        if Status[n] == 'ГОДЕН':
            FGroup = FGroup + Group[n]

        if len(FGroup) == 0:
            StatArr[3].update({'FinalGroup': ''})
        else:
            StatArr[3].update({'FinalGroup': FGroup})

    return StatArr
