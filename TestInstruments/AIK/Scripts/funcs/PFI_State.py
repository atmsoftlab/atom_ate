def pfi_state(path_dll, stattest):
    import ctypes

    # StatTest: 1 - Годен, 0 - Брак
    # Период импульса сигнала Годен/Брак
    ImpPeriod = 0.1

    newlib = ctypes.CDLL(path_dll)

    # PortFinTest = b'PXI2Slot8/port1/line2'
    # PortStatTest = b'PXI2Slot8/port1/line3'

    # BPortFinTest = (ctypes.c_char * len(PortFinTest))(*PortFinTest)
    # BPortStatTest = (ctypes.c_char * len(PortStatTest))(*PortStatTest)

    # newlib.DAQ_StatTest.restype = ctypes.c_int
    # newlib.DAQ_StatTest.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.POINTER(ctypes.c_char), ctypes.c_int, ctypes.c_double]
    # const_error = newlib.DAQ_StatTest(BPortFinTest, BPortStatTest, stattest, ImpPeriod)

    newlib.DAQ_StatTest.restype = ctypes.c_int
    newlib.DAQ_StatTest.argtypes = [ctypes.c_int, ctypes.c_double]
    const_error = newlib.DAQ_StatTest(stattest, ImpPeriod)

    return const_error

# 0 - нет ошибки, 1000 - ошибка
