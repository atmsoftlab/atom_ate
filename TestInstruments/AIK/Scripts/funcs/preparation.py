def preparation(connection, initdonn, resarray, test_plan_id, duts_id, testplan_results, arm_id):
    import json
    import hashlib
    import time
    import tkinter as tk
    from funcs import connectdb
    from tkinter import messagebox as mb

    measurements = []
    buf = []

    Group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M']

    # Подготовка массива value
    test_id = []
    value = []
    outparam_id_all = []
    Outparam = []

    try:
        with connection.cursor() as cur:
            cur.execute("SELECT id, name, type, duts_id, short_name, unit FROM atedb.attr_test_param")
            buffer_attr = cur.fetchall()

            attr = buffer_attr[:]
            dict_attr = {}
            for h in range(0, len(attr)):
                dict_attr.setdefault(attr[h].pop('id'), attr[h])
            del attr

            cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
            buffer_tstplan = cur.fetchall()[0]

        json_buffer = json.loads(buffer_tstplan.get('structure'))

        for i in range(0, len(json_buffer)):
            test_id.append(json_buffer[i].get('TestId'))

        for j in range(0, len(test_id)):
            with connection.cursor() as cur:
                cur.execute("SELECT test_function, outparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                buffer_tstelem = cur.fetchall()[0]
            outparam_id_all.append(buffer_tstelem.get('outparam'))
        outparam_id = outparam_id_all[0:len(resarray)]

        for n in range(0, len(outparam_id)):
            with connection.cursor() as cur:
                cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(outparam_id[n]))
                buffer_tstparam = cur.fetchall()[0]
            Outparam.append(json.loads(buffer_tstparam.get('parametr')))

        for k in range(0, len(outparam_id)):

            value.append([])
            for m in range(0, len(Outparam[k])):
                value[k].append({'Ind': Outparam[k][m].get('Ind'),
                                 'Name': Outparam[k][m].get('Name'),
                                 'Unit': dict_attr.get(Outparam[k][m].get('AttrId')).get('Unit'),
                                 'Value': resarray[k][1].get('results')[m],
                                 'AttrId': Outparam[k][m].get('AttrId'),
                                 'ShortName': dict_attr.get(Outparam[k][m].get('AttrId')).get('short_name'),
                                 'NormaTable': Outparam[k][m].get('NormaTable')})

        # Подготовка массива input_parameters
        test_id = []
        inparam_id = []
        Inparam = []

        with connection.cursor() as cur:
            cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
            buffer_tstplan = cur.fetchall()[0]

        json_buffer = json.loads(buffer_tstplan.get('structure'))

        for i in range(0, len(json_buffer)):
            test_id.append(json_buffer[i].get('TestId'))

        for j in range(0, len(test_id)):
            with connection.cursor() as cur:
                cur.execute("SELECT test_function, inparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                buffer_tstelem = cur.fetchall()[0]
            inparam_id.append(buffer_tstelem.get('inparam'))

        Inhash = []
        for n in range(0, len(inparam_id)):
            with connection.cursor() as cur:
                cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(inparam_id[n]))
                buffer_tstparam = cur.fetchall()
            Inparam.append(json.loads(buffer_tstparam[0].get('parametr')))
            Inhash.append(hashlib.md5(buffer_tstparam[0].get('parametr').encode()).hexdigest())

        # Работа с массивом input_parameters
        with connection.cursor() as cur:
            # cur.execute("SELECT id, hash FROM atedb.input_parameters WHERE arm_id = " + str(arm_id))
            cur.execute("SELECT id, hash FROM atedb.input_parameters")
            buffer_inputparam = cur.fetchall()
            buffer_inputparam_id = []
            buffer_inputparam_hash = []
            for g in range(len(buffer_inputparam)):
                buffer_inputparam_id.append(buffer_inputparam[g].get('id'))
                buffer_inputparam_hash.append(buffer_inputparam[g].get('hash'))

        input_parameters_id = []

        for k in range(0, len(resarray)):
            try:
                input_parameters_id.append(buffer_inputparam_id[buffer_inputparam_hash.index(Inhash[k])])
            except ValueError as e:
                if e.__str__().find('is not in list') != -1:
                    with connection.cursor() as cur:
                        cur.execute("INSERT INTO atedb.input_parameters (id, value, arm_id)"
                                    " VALUES (%s, %s, %s)", (None, json.dumps(Inparam[k]), arm_id))
                    connection.commit()

                    time.sleep(0.1)

                    with connection.cursor() as cur:
                        cur.execute("SELECT id FROM atedb.input_parameters WHERE hash = '" + Inhash[k] + "'")
                    input_parameters_id.append(cur.fetchall()[0].get('id'))

        # Подготовка массива measurements
        for j in range(0, len(resarray)):
            resarray[j][0].setdefault('БРАК', 0)
            for i in range(0, len(resarray[j]) - 4):
                resarray[j][0].setdefault(Group[i], 0)
                buf.append(list(resarray[j][4 + i].values())[0])

            for n in range(0, len(buf)):
                for k in range(0, len(buf[n])):

                    if n == 0:
                        value[j][k].setdefault('ClassAfterTestExecute', '')

                    if buf[n][k] == 'ГОДЕН':
                        value[j][k].update({'ClassAfterTestExecute': value[j][k].get('ClassAfterTestExecute') + Group[n]})
                        resarray[j][0].update({Group[n]: resarray[j][0].get(Group[n]) + 1})
                    elif buf[n][k] == 'БРАК':
                        resarray[j][0].update({'БРАК': resarray[j][0].get('БРАК') + 1})
            buf.clear()

            measurements.append([value[j], test_id[j], test_plan_id, duts_id, testplan_results, arm_id, input_parameters_id[j]])

        status = 0

    except Exception as e:
        print(e)
        status = 1
        answer = True

        root = tk.Tk()
        root.withdraw()

        while answer is True and status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")
            if answer:
                status, connection = connectdb(initdonn)

            if status == 0:
                with connection.cursor() as cur:
                    cur.execute("SELECT id, name, type, duts_id, short_name, unit FROM atedb.attr_test_param")
                    buffer_attr = cur.fetchall()

                    attr = buffer_attr[:]
                    dict_attr = {}
                    for h in range(0, len(attr)):
                        dict_attr.setdefault(attr[h].pop('id'), attr[h])
                    del attr

                    cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
                    buffer_tstplan = cur.fetchall()[0]

                json_buffer = json.loads(buffer_tstplan.get('structure'))

                for i in range(0, len(json_buffer)):
                    test_id.append(json_buffer[i].get('TestId'))

                for j in range(0, len(test_id)):
                    with connection.cursor() as cur:
                        cur.execute("SELECT test_function, outparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                        buffer_tstelem = cur.fetchall()[0]
                    outparam_id_all.append(buffer_tstelem.get('outparam'))
                outparam_id = outparam_id_all[0:len(resarray)]

                for n in range(0, len(outparam_id)):
                    with connection.cursor() as cur:
                        cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(outparam_id[n]))
                        buffer_tstparam = cur.fetchall()[0]
                    Outparam.append(json.loads(buffer_tstparam.get('parametr')))

                for k in range(0, len(outparam_id)):

                    value.append([])
                    for m in range(0, len(Outparam[k])):
                        value[k].append({'Ind': Outparam[k][m].get('Ind'),
                                         'Name': Outparam[k][m].get('Name'),
                                         'Unit': dict_attr.get(Outparam[k][m].get('AttrId')).get('Unit'),
                                         'Value': resarray[k][1].get('results')[m],
                                         'AttrId': Outparam[k][m].get('AttrId'),
                                         'ShortName': dict_attr.get(Outparam[k][m].get('AttrId')).get('short_name'),
                                         'NormaTable': Outparam[k][m].get('NormaTable')})

                # Подготовка массива input_parameters
                test_id = []
                inparam_id = []
                Inparam = []

                with connection.cursor() as cur:
                    cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
                    buffer_tstplan = cur.fetchall()[0]

                json_buffer = json.loads(buffer_tstplan.get('structure'))

                for i in range(0, len(json_buffer)):
                    test_id.append(json_buffer[i].get('TestId'))

                for j in range(0, len(test_id)):
                    with connection.cursor() as cur:
                        cur.execute("SELECT test_function, inparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                        buffer_tstelem = cur.fetchall()[0]
                    inparam_id.append(buffer_tstelem.get('inparam'))

                Inhash = []
                for n in range(0, len(inparam_id)):
                    with connection.cursor() as cur:
                        cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(inparam_id[n]))
                        buffer_tstparam = cur.fetchall()
                    Inparam.append(json.loads(buffer_tstparam[0].get('parametr')))
                    Inhash.append(hashlib.md5(buffer_tstparam[0].get('parametr').encode()).hexdigest())

                # Работа с массивом input_parameters
                with connection.cursor() as cur:
                    # cur.execute("SELECT id, hash FROM atedb.input_parameters WHERE arm_id = " + str(arm_id))
                    cur.execute("SELECT id, hash FROM atedb.input_parameters")
                    buffer_inputparam = cur.fetchall()
                    buffer_inputparam_id = []
                    buffer_inputparam_hash = []
                    for g in range(len(buffer_inputparam)):
                        buffer_inputparam_id.append(buffer_inputparam[g].get('id'))
                        buffer_inputparam_hash.append(buffer_inputparam[g].get('hash'))

                input_parameters_id = []

                for k in range(0, len(resarray)):
                    try:
                        input_parameters_id.append(buffer_inputparam_id[buffer_inputparam_hash.index(Inhash[k])])
                    except ValueError as e:
                        if e.__str__().find('is not in list') != -1:
                            with connection.cursor() as cur:
                                cur.execute("INSERT INTO atedb.input_parameters (id, value, arm_id)"
                                            " VALUES (%s, %s, %s)", (None, json.dumps(Inparam[k]), arm_id))
                            connection.commit()

                            time.sleep(0.1)

                            with connection.cursor() as cur:
                                cur.execute("SELECT id FROM atedb.input_parameters WHERE hash = '" + Inhash[k] + "'")
                            input_parameters_id.append(cur.fetchall())

                # Подготовка массива measurements
                for j in range(0, len(resarray)):
                    resarray[j][0].setdefault('БРАК', 0)
                    for i in range(0, len(resarray[j]) - 4):
                        resarray[j][0].setdefault(Group[i], 0)
                        buf.append(list(resarray[j][4 + i].values())[0])

                    for n in range(0, len(buf)):
                        for k in range(0, len(buf[n])):

                            if n == 0:
                                value[j][k].setdefault('ClassAfterTestExecute', '')

                            if buf[n][k] == 'ГОДЕН':
                                value[j][k].update({'ClassAfterTestExecute': value[j][k].get('ClassAfterTestExecute') + Group[n]})
                                resarray[j][0].update({Group[n]: resarray[j][0].get(Group[n]) + 1})
                            elif buf[n][k] == 'БРАК':
                                resarray[j][0].update({'БРАК': resarray[j][0].get('БРАК') + 1})
                    buf.clear()

                    measurements.append([value[j], test_id[j], test_plan_id, duts_id, testplan_results, arm_id, input_parameters_id[j]])

        root.destroy()

    return measurements, resarray, status
