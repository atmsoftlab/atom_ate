def pfi_start(path_dll):
    import ctypes

    # modulename = b'PXI2Slot8/port0/line4'

    newlib = ctypes.CDLL(path_dll)
    # bufferstrmodname = (ctypes.c_char * len(modulename))(*modulename)

    newlib.DAQ_NStart.restype = ctypes.c_int
    # newlib.DAQ_NStart.argtypes = [ctypes.POINTER(ctypes.c_char)]
    const_start = newlib.DAQ_NStart()

    return const_start

    # 0 - нет сигнала, 1 - старт, 1000 - ошибка
