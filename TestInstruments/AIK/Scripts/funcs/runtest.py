def runtest(DLLPath, XMlPath, inputData, bufferSize):
    import ctypes
    import numpy as np
    import json
    import matplotlib.pyplot as plt
    from funcs import errorfunc

    print("inputdata = " + str(inputData))

    newlib = ctypes.CDLL(DLLPath)
    exec('newlib.XMLWorker.argtypes = [ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_char), ctypes.POINTER(ctypes.c_char), ctypes.c_int, ctypes.c_int]')
    exec('newlib.XMLWorker.restype = ctypes.c_int')
    outstr = np.full(bufferSize, b' ')
    bufferin = (ctypes.c_double * len(inputData))(*inputData)
    bufferout = (ctypes.c_char * len(outstr))(*outstr)
    Path = bytes(XMlPath, encoding='utf8')

    BPath = (ctypes.c_char * len(Path))(*Path)
    error = newlib.XMLWorker(bufferin, BPath, bufferout, len(inputData), bufferSize)

    # print("error = " + str(error))

    if error:
        # print("Code Error: {}".format(error))
        deskerror = errorfunc(error)
    elif error == 0:
        iserror = 0
        deskerror = ''
    outputdata = np.frombuffer(bufferout, dtype='S1')

    result = []
    for i in range(0, len(outputdata)):
        result.append(str(outputdata[i].decode('utf8')))

    resultStr = ''.join(result).rstrip()
    resultat, vacs = [json.loads(i) for i in resultStr.split('###')]

    resultDict = []
    for i in resultat:
        tempD = {}
        try:
            exec("tempD['value'] =" + i[0])
        except ZeroDivisionError as e:
            tempD['value'] = "NaN"
        tempD['unit'] = i[1]
        tempD['normed'] = i[2] != "-" and i[3] != "-"
        if tempD['normed']:
            tempD['minNorm'] = float(i[2])
            tempD['maxNorm'] = float(i[3])
        resultDict.append(tempD)

    result = []
    for i in range(0, len(resultDict)):
        result.append(resultDict[i].get('value'))

    def plot(vacs):
        L = len(vacs)
        placeNx = int(L ** (0.5)) + 1
        placeNy = L // placeNx + 1
        plt.figure(figsize=(4 * placeNx, 4 * placeNy))
        print(4 * placeNx, 4 * placeNy)
        for j in range(L):
            i = vacs[j]
            if i.get('needPlot') == 'yes':
                plt.subplot(placeNx, placeNy, j + 1)
                x = [i[0] for i in i.get("points")]
                y = [i[1] for i in i.get("points")]
                plt.plot(x, y)
                plt.title("ВАХ")
                plt.xlabel(i.get('labelX') + ', ' + i.get('unitX'))  # ось абсцисс
                plt.ylabel(i.get('labelY') + ', ' + i.get('unitY'))  # ось ординат
                plt.grid(True)  # включение отображение сетки
        plt.tight_layout(pad=1.1)
        plt.show()

    # plot(vacs)

    return result, error, deskerror

    # error - номер ошибки в Labview
    # iserror - переменная сообщающая об ошибке в тесте (0 - ошибки нет либо ее сбросили; 1 - ошибку возникшую в результате
    # выполнения теста не удалось сбросить (результат выполнения теста - "БРАК"); 2 - ошибка возникла на этапе инициализации,
    # еще до выполнения теста, (результат - "БРАК")
    # deskerror - переменная + описание ошибки
