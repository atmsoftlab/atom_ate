def write2db_tpr(connection, initdonn, testplan_results_donnes, arm_id):
    import tkinter as tk
    from funcs import readfromdb
    from funcs import connectdb
    from tkinter import messagebox as mb

    try:
        with connection.cursor() as cur:
            cur.execute("INSERT INTO atedb.testplan_results (id, plate_number, batch_number, result, result_group, start_date, finish_date, arm_id)"
                        " VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (None, testplan_results_donnes[0], testplan_results_donnes[1], testplan_results_donnes[2],
                                                                     str(testplan_results_donnes[3]), testplan_results_donnes[4], testplan_results_donnes[5],
                                                                     arm_id))
            connection.commit()

            testplan_results, status = readfromdb(connection, initdonn)
        cur.close()

    except Exception as e:
        print(e)
        status = 1
        answer = True

        root = tk.Tk()
        root.withdraw()

        while answer is True and status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")
            if answer:
                status, connection = connectdb(initdonn)

            if status == 0:
                with connection.cursor() as cur:
                    cur.execute("INSERT INTO atedb.testplan_results (id, plate_number, batch_number, result, result_group, start_date, finish_date, arm_id)"
                                " VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                                (None, testplan_results_donnes[0], testplan_results_donnes[1], testplan_results_donnes[2],
                                 str(testplan_results_donnes[3]), testplan_results_donnes[4], testplan_results_donnes[5], arm_id))

                    connection.commit()

                    testplan_results, status = readfromdb(connection, initdonn)
                cur.close()

        root.destroy()

    return testplan_results, status
