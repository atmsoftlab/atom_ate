def runtest(path, tname, regim, inputdata, colvihparam):

    import ctypes
    import numpy as np

    newlib = ctypes.CDLL(path)

    # инициализация списка выходных величин
    result = []

    # regim: 0 - "Все тесты"; 1 - "До брака"; 2 - "Автомат"; 3 - "Цикл"
    # tname: Test00
    # inputdata = [5.5, 5, 0.5, 30, 70, 110, 0, 15000, 5, 5.4, 5.6, 29.5, 30.5, 69.5, 70.5, 109.5, 110.5, 100, 112, -1, 10]
    # outputdata = np.full(int(240), 0)

    outputdata = np.full(int(colvihparam), 0)
    outstr = np.full(2000, b' ')
    error = ''

    bufferin = (ctypes.c_double * len(inputdata))(*inputdata)
    bufferout = (ctypes.c_double * len(outputdata))(*outputdata)
    bufferstrerrout = (ctypes.c_char * len(outstr))(*outstr)

    exec('newlib.' + tname + '.restype = ctypes.c_int')
    exec('newlib.' + tname + '.argtypes = [ctypes.POINTER(ctypes.c_double), ctypes.c_int, ctypes.POINTER(ctypes.c_double), '
                             'ctypes.POINTER(ctypes.c_char), ctypes.c_int, ctypes.c_int, ctypes.c_int]')
    exec('newlib.' + tname + '(bufferin, regim, bufferout, bufferstrerrout, len(inputdata), len(outputdata), len(outstr))')

    outputdata = np.frombuffer(bufferout)
    err = np.frombuffer(bufferstrerrout, dtype='S1')

    for i in range(0, len(outputdata)):
        result.append(str(outputdata[i]))

    for i in range(0, len(err)):
        error = error + err[i].decode('utf8')
    deskerror = error.rstrip()

    if len(deskerror) == 0:
        iserror = 0
    else:
        iserror = error[0]

    # error - номер ошибки в Labview
    # iserror - переменная сообщающая об ошибке в тесте (0 - ошибки нет либо ее сбросили; 1 - ошибку возникшую в результате
    # выполнения теста не удалось сбросить (результат выполнения теста - "БРАК"); 2 - ошибка возникла на этапе инициализации,
    # еще до выполнения теста, (результат - "БРАК")
    # deskerror - переменная + описание ошибки

    return result, iserror, deskerror
path = 'D:\\Documents\\aik_edit_prog\\builds\\tetsPRJ\\My DLL\\SharedLib.dll'
import ctypes
import numpy as np

newlib = ctypes.CDLL(path)
newlib.SetVoltFull.restype = ctypes.c_int
newlib.SetVoltFull.argtypes = [ctypes.c_double, ctypes.c_double, ctypes.c_double]
print(newlib.SetVoltFull(5.0,0.002,0.0))

newlib.ResetSource.restype = ctypes.c_int
#newlib.ResetSource.argtypes = [ctypes.c_void_p]
print(newlib.ResetSource())

#print(runtest(path, "InitSecion", 1, [], 2))
