not_intall_list = []
try:
	import numpy
except ImportError:
	not_intall_list.append('numpy')
try:
	import pymysql
except ImportError:
	not_intall_list.append('pymysql')
try:
	import keyboard
except ImportError:
	not_intall_list.append('keyboard')
try:
	import psutil
except ImportError:
	not_intall_list.append('psutil')
try:
	import tkcalendar
except ImportError:
	not_intall_list.append('tkcalendar')
try:
	import xlsxwriter
except ImportError:
	not_intall_list.append('xlsxwriter')

if len(not_intall_list) > 0:
	print('Some modules not installed')
	#installation script:
	
else:
	print('All is fine')