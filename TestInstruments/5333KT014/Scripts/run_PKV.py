from sys import argv
from funcs import *
from tkinter import messagebox as mb

import time
import pathlib
import os
import keyboard
import configparser

patchconfig, var_zapuska = getpath2pathconf()  # Путь к файлу arm_id.ini настроек для инфы подключения к БД

# pathconfig = 'C:\\TestInstruments\\arm_id.ini'
# var_zapuska = 1

# groups = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
groups = ['A', 'B', 'C', 'D']

if var_zapuska == 0:
    # Входные аргументы для скрипта из ВУ
    path2savtxt = argv[0].split('run')[0]
    bufpath = argv[0].split('\\')
    path2readini = bufpath[0] + "\\" + bufpath[2]
    pathDll = argv[1]
    regim = int(argv[2])
    test_plan_id = int(argv[3])
    duts_id = int(argv[4])
    batch_number = argv[5]
    loop = int(argv[6])
    # vibor_zap_platenum = int(argv[7])
    vibor_zap_platenum = 0

    if loop == 1:
        cyklzap = 1000000
    else:
        cyklzap = 1

elif var_zapuska == 1:
    # Для отладки в PyCharm
    path2savtxt = "C:\\TestInstruments\\LSN_IS-4\\Scripts"
    path2readini = "C:\\TestInstruments"
    pathDll = "C:\\TestInstruments\\LSN_IS-4\\DLL\\LSN_IS-4.dll"
    regim = 0
    test_plan_id = 60
    duts_id = 13
    batch_number = "LSN_IS-4"
    loop = 0
    vibor_zap_platenum = 0

    if loop == 1:
        cyklzap = 1000000
    else:
        cyklzap = 1

status = 0
testparams = []
testresults = []
pathresults = path2savtxt + '\\Results\\'
increm = 0

endstr = "\nthisistrueresults DONE!\n"

if pathlib.Path(path2savtxt + '\\pause.pause').exists():
    os.remove(path2savtxt + '\\pause.pause')

if pathlib.Path(path2savtxt + '\\stoppause.pause').exists():
    os.remove(path2savtxt + '\\stoppause.pause')

if os.path.exists(path2readini + "\\arm_id.ini"):  # Проверка существования ini файла с arm_id

    config = configparser.ConfigParser()
    config.read(path2readini + "\\arm_id.ini")

    if config.sections()[0] == 'MAIN':

        if 'arm_id' in config[config.sections()[0]]:
            arm_id = config[config.sections()[0]]['arm_id']

            connection = None
            plate_number = 0

            err_status = 0  # убрать если запуск на железе
            flag_regim_brak = 0  # Первоначальный сброс флага брака

            # Считывание данных из БД
            initdonn, stat = initdb(path2readini)

            if stat == 0:
                status, connection = connectdb(initdonn, path2savtxt)
                if status == 0:
                    testresults, test_id, status = readfromdb_testres(connection, initdonn, duts_id, test_plan_id, path2savtxt)
                    if status == 0:
                        testparams, status = readfromdb_testprms(connection, initdonn, duts_id, test_plan_id, path2savtxt)
            elif stat == 1:
                mb.showwarning(title='Внимание!', message='Не найден файл arm_id.ini по указанному пути')
            elif stat == 2:
                mb.showwarning(title='Внимание!', message='В файле arm_id.ini отсутствует необходимая информация для инициализации подключения к БД')

            if vibor_zap_platenum == 0:
                plate_number, status = getnumberfdb(connection, initdonn, path2savtxt)

            elif vibor_zap_platenum == 1:
                # Функция выдачи интерактивного диалога с запросом "Партии" и "Шифра микрухи" (tkinter)
                platenuminstr = interwindow()
                if len(platenuminstr) != 0 and platenuminstr != "*":
                    plate_number = int(platenuminstr)
                else:
                    flag_stop = 1  # Отмена запуска через окно ввода шифра микросхемы/кристалла

            # первоначальный сброс модулей если таковой будет необходим при инициализации - запускать только если железо
            # err_status = resmodules(pathDll)

            # colbrak = 0  # переменная для сохранения количества браков
            colcykl = 0  # переменная количества циклов запуска

            if status == 0:

                flag_stop = 0

                while flag_stop != 1 and colcykl < cyklzap:
                    # Не удалять!!! Программный косяк иначе будет
                    keyboard.is_pressed('esc')

                    counttime = 0
                    colcykl += 1

                    file_pause = pathlib.Path(path2savtxt + '\\pause.pause')
                    file_stoppause = pathlib.Path(path2savtxt + '\\stoppause.pause')
                    file_stoptest = pathlib.Path(path2savtxt + '\\stoptest.stoptest')

                    while PFI_Start(pathDll, 0, 'PKV') == 0 and flag_stop != 1:
                        if os.path.exists(path2savtxt + '\\stoptest.stoptest') is True or keyboard.is_pressed('esc') is True:
                            flag_stop = 1
                            if var_zapuska == 1:
                                print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Нажата клавиша СТОП - тест-программа остановлена!\n")
                            time.sleep(1)
                        elif file_pause.exists():
                            h = 0
                            while file_pause.exists() and h < 7200:
                                if h == 0:
                                    if var_zapuska == 1:
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) +
                                              " - Нажата клавиша ПАУЗА - тест-программа в ожидании продолжения(СТАРТ)/сброса(СТОП)!\n")
                                        time.sleep(1)
                                if file_stoptest.exists() or keyboard.is_pressed('esc') is True:
                                    flag_stop = 1
                                    if var_zapuska == 1:
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) +
                                              " - Нажата клавиша СТОП - тест-программа остановлена!\n")
                                    if file_pause.exists():
                                        os.remove(path2savtxt + '\\pause.pause')
                                    break
                                if file_stoppause.exists() and h < 7200:
                                    os.remove(path2savtxt + '\\pause.pause')
                                    os.remove(path2savtxt + '\\stoppause.pause')
                                    if var_zapuska == 1:
                                        print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Пауза снята - продолжение тестирования!\n")
                                        time.sleep(1)
                                    break
                                time.sleep(1)
                                h += 1
                        time.sleep(0.2)
                        # if var_zapuska == 1:
                        # pass
                        #     print(counttime, file=f)
                        counttime += 1
                        if counttime >= 1800 and var_zapuska == 1:
                            flag_stop = 1
                            # print('Закончилось время ожидания сигнала на Запуск! Выход.', file=f)

                    if flag_stop != 1:
                        StatArr = []
                        measurements = []
                        resarray = []
                        results = []

                        colbrak = 0  # переменная для сохранения количества браков

                        tofile2 = ''

                        if os.path.exists(path2savtxt + '\\pause.pause') is not True and os.path.exists(path2savtxt + '\\stoptest.stoptest') is not True \
                                and keyboard.is_pressed('esc') is not True and flag_stop != 1 and err_status == 0:

                            # запись номера тестируемой схемы
                            if var_zapuska == 1:
                                print("\nplate number = " + str(plate_number) + "\n")

                            # замер времени перед началом запуска тест-программы
                            start_date = time.strftime("%Y-%m-%d %H:%M:%S")
                            if var_zapuska == 1:
                                print("start_date = " + str(start_date) + "\n")

                            status_izm(path2savtxt, 'ИЗМЕРЕНИЕ')

                            for i in range(0, len(testparams)):
                                time.sleep(0.01)
                                if os.path.exists(path2savtxt + '\\stoptest.stoptest') is True or keyboard.is_pressed('esc') or err_status == 1 or err_status == 2:
                                    if var_zapuska == 1:
                                        print("\nПрерывание выполнения тест-программы на тесте " + str(testresults[i][0].get('TName')) + "\n")
                                    flag_stop = 1
                                    break
                                else:
                                    # runtest - функция запуска теста-функции dll на выполнение, описание входных параметров: (path - "C:\\TestInstruments\\I2IS-1.dll";
                                    # "TName" - Test00;
                                    # "Regim": 0 - Все, 1 - До брака;
                                    # "InputParamsValue" - массив входных данных;
                                    # "NumResParams" - количество выходных параметров.
                                    results, iserror, deskerror = runtest(pathDll, testresults[i][0].get('TName'), regim,
                                                                          [float(i) for i in testparams[i][0].get('InputParamsValue')],
                                                                          testresults[i][0].get('NumResParams'),
                                                                          [testresults[i][1][k].get('Unit') for k in range(0, len(testresults[i][1]))])
                                    if iserror != 0:
                                        if var_zapuska == 1:
                                            print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Прерывание выполнения тест-программы: ошибка "
                                                  + str(deskerror) + "\n", endstr)
                                        flag_stop = 1
                                        err_status = iserror
                                        break
                                    else:
                                        increm += 1

                                        StatArr = sorting(results, testresults, i)
                                        resarray.append(StatArr)

                                        num_param = 1
                                        tofile2 = ''
                                        for j in range(0, len(StatArr[1].get('results'))):
                                            tofile2 = tofile2 + "Num: " + str(num_param) + "; TName: " + testresults[i][0].get('TName') + "; ShortName: " + \
                                                      str(testresults[i][1][j].get('ShortName') if testresults[i][1][j].get('ShortName') is not None else "-") + "; " + \
                                                      "FullName: " + testresults[i][1][j].get('Name') + "; " + \
                                                      "Value: " + str("{:.4f}".format(float(StatArr[1].get('results')[j]))) + "; " + \
                                                      "Unit: " + str(testresults[i][1][j].get('Unit') if testresults[i][1][j].get('Unit') is not None and len(testresults[i][1][j].get('Unit')) != 0 else "-") + "; " + \
                                                      "Status: " + (StatArr[4].get('NameGroup ' + StatArr[3].get('FinalGroup'))[j] if StatArr[3].get('FinalGroup') != '' else StatArr[4].get('NameGroup A')[j]) + \
                                                      "; " + "Min: " + (str(testresults[i][1][j].get('NormaTable')[ord(StatArr[3].get('FinalGroup')) - 65].get('Min')) if StatArr[3].get('FinalGroup') != '' else
                                                                        str(testresults[i][1][j].get('NormaTable')[0].get('Min'))) + "; " + \
                                                      "Max: " + (str(testresults[i][1][j].get('NormaTable')[ord(StatArr[3].get('FinalGroup')) - 65].get('Max')) if StatArr[3].get('FinalGroup') != '' else
                                                                 str(testresults[i][1][j].get('NormaTable')[0].get('Max'))) + "; " + ("\n" if j != len(StatArr[1].get('results')) - 1 else "")

                                            num_param = num_param + 1

                                        if var_zapuska == 1:
                                            print(tofile2)

                                        fres = open(pathresults + 'TestResult' + str(increm) + '.txt', 'w+', encoding='utf8')
                                        print(tofile2, "\n" + endstr, file=fres)
                                        fres.close()

                                        if regim != 0 and StatArr[2].get('Status') == 'БРАК':
                                            if testresults[i][0].get('TName') == 'Test00':
                                                PFI_State(pathDll, 10, 'PKV')
                                                if var_zapuska == 1:
                                                    print('Неконтакт!')
                                            elif testresults[i][0].get('TName') != 'Test00':
                                                PFI_State(pathDll, 9, 'PKV')
                                                if var_zapuska == 1:
                                                    print('БРАК!')
                                            colbrak += 1
                                            if colbrak == regim:
                                                if var_zapuska == 1:
                                                    print("\n" + str(time.strftime("%Y-%m-%d %H:%M:%S")) + " - Прерывание выполнения тест-программы по "
                                                          + str(colbrak) + " браку" + "\n")
                                                # flag_stop = 1
                                                break

                            # запуск функции сброса модулей при возникновении ошибки в источниках - запуск только на железе
                            # if iserror == 1 or iserror == 2:
                            # err_status = resmodules(pathDll)

                            # замер времени окончания выполения тест-программы
                            finish_date = time.strftime("%Y-%m-%d %H:%M:%S")

                            # запуск классификации результирующего массива выходных данных
                            test_plan_status, result_group = classification(resarray)

                            # Подготовка массива для записи в первую таблицу БД - testplan_results
                            testplan_results_donnes = [plate_number, batch_number, test_plan_status, result_group, start_date, finish_date]

                            tofile2 = ''

                            if flag_stop != 1:
                                # запись результатов testplan_results_donnes в БД
                                testplan_results, status = write2db_tpr(connection, initdonn, testplan_results_donnes, arm_id, path2savtxt)

                                status_gb(path2savtxt, test_plan_status)
                                zapis_gba(path2savtxt, test_plan_status)

                                if status == 0:
                                    # Подготовка данных для записи во вторую таблицу БД - measurements
                                    measurements, resarray, duts_name, status = preparation(connection, initdonn, resarray, test_plan_id, duts_id, testplan_results, arm_id, path2savtxt)

                                    tofile2 = tofile2 + "Dut:  \" " + duts_name + " \" ;  Part:  \" " + batch_number + " \" ;  Plate_number:  \" " + str(plate_number) + " \" ; \n" + \
                                                        "Статус ТП: " + (" \" ГОДЕН \" " if test_plan_status == 0 else " \" БРАК \" ") + " ; " + \
                                                        "Группа ТП:" + str(" \" - \" " if result_group == "" else " \" " + result_group + " \"") + " ; " + "\n"

                                    increm += 1

                                    if var_zapuska == 1:
                                        print(tofile2)

                                    if test_plan_status == 0:
                                        numgroup = ord(result_group) - 64
                                        if var_zapuska == 1:
                                            print('result_group = ' + str(result_group), 'numgroup = ' + str(numgroup))
                                        PFI_State(pathDll, numgroup, 'PKV')

                                    fresend = open(pathresults + 'TestResult' + str(increm) + '.txt', 'w+', encoding='utf8')
                                    print(tofile2, endstr, file=fresend)
                                    fresend.close()

                                if var_zapuska == 1:
                                    print("finish_date = " + str(finish_date) + "\n")

                                # замер времени перед записью данных в датабазу
                                date_before_save2db = time.strftime("%Y-%m-%d %H:%M:%S")
                                if var_zapuska == 1:
                                    print("date_before_save2db = " + str(date_before_save2db))

                                if status == 0:
                                    # запись результатов measurements в БД
                                    status = write2db_meas(connection, initdonn, measurements, path2savtxt)

                                    if status == 0:
                                        # замер времени после записи данных в датабазу
                                        date_after_save2db = time.strftime("%Y-%m-%d %H:%M:%S")
                                        if var_zapuska == 1:
                                            print("date_after_save2db = " + str(date_after_save2db))

                                        if os.path.exists(path2savtxt + '\\clearstat.clearstat') is True:
                                            os.remove(path2savtxt + '\\clearstat.clearstat')

                                        plate_number = plate_number + 1

                                else:
                                    flag_stop = 1
                                    if var_zapuska == 1:
                                        print("Останов пользователем или неудачное соединения с базой данных\n")
                            else:
                                flag_stop = 1
                                if var_zapuska == 1:
                                    print("Останов пользователем\n")
                        else:
                            flag_stop = 1
                            if var_zapuska == 1:
                                print('Останов по ESC')
                            if pathlib.Path(path2savtxt + '\\stoptest.stoptest').exists():
                                os.remove(path2savtxt + '\\stoptest.stoptest')

                        if (loop == 1 and flag_stop == 1) or loop == 0:
                            if var_zapuska == 1:
                                pass
                            if pathlib.Path(path2savtxt + '\\stoptest.stoptest').exists():
                                os.remove(path2savtxt + '\\stoptest.stoptest')

                    if var_zapuska == 0:
                        pass
                    time.sleep(1)
            else:
                if var_zapuska == 1:
                    print("Остановка выполнения пользователем после неудачного соединения с базой данных! \n", endstr)
    else:
        mb.showerror(title='Отсутствует секция MAIN в ini файле arm_id ...', message='Ошибка: отсутствует секция MAIN в конфигурационном ini-файле arm_id.ini. '
                                                                                     'Программа завершает работу.', icon='error')
else:
    mb.showerror(title='Отсутствует ini файл arm_id ...', message='Ошибка: отсутствует конфигурационный ini-файл arm_id.ini. '
                                                                  'Программа завершает работу.', icon='error')

# закрытие соединения с БД
if status == 0:
    connection.close()
