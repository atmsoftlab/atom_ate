def write2db_meas(connection, initdonn, measurements, path2savtxt):
    import json
    import tkinter as tk
    from funcs import connectdb
    from tkinter import messagebox as mb

    try:
        for i in range(0, len(measurements)):
            with connection.cursor() as cur:
                cur.execute("INSERT INTO atedb.measurements (id, value, test_id, test_plan_id, duts_id, testplan_results, arm_id, input_parameters_id)"
                            " VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (None, json.dumps(measurements[i][0]), measurements[i][1], measurements[i][2],
                                                                         measurements[i][3], measurements[i][4], measurements[i][5], measurements[i][6]))
                connection.commit()

        status = 0

    except Exception as e:
        print(e)
        # status = 1
        # answer = True

        # root = tk.Tk()
        # root.withdraw()

        # while answer is True and status == 1:
        #     answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
        #                                                     "повторную попытку подключения?", icon="question")
        # if answer:
        status, connection = connectdb(initdonn, path2savtxt)

        if status == 0:
            for i in range(0, len(measurements)):
                with connection.cursor() as cur:
                    cur.execute("INSERT INTO atedb.measurements (id, value, test_id, test_plan_id, duts_id, input_parameters, testplan_results, arm_id)"
                                " VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                                (None, json.dumps(measurements[i][0]), measurements[i][1], measurements[i][2],
                                 measurements[i][3], measurements[i][4], measurements[i][5], measurements[i][6]))
                    connection.commit()

        # root.destroy()

    return status
