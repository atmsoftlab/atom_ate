import os
import psutil
from tkinter import *
from tkinter.filedialog import askopenfilename


def getpath2pathconf():
    proc_name = 'AToM_ATE.exe'
    path2conf = ''
    var_zapuska = 0  # 0 - если через ВУ; 1 - если из консоли

    for proc in psutil.process_iter():
        # Пока скрипт работает, процесс уже может перестать существовать
        # (поскольку между psutil.process_iter() и proc.name() проходит время)
        # и будет выброшено исключение psutil.NoSuchProcess
        try:
            proc_name_in_loop = proc.name()
        except psutil.NoSuchProcess:
            pass
        else:
            if proc_name_in_loop == proc_name:
                path2conf = proc.cwd() + "\\arm_id.ini"

    if len(path2conf) == 0:
        root = Tk()
        root.withdraw()  # hide the window
        root.iconbitmap(str(os.path.abspath(os.path.dirname(sys.argv[0]))) + '\\atom.ico')
        path2conf = askopenfilename(
            parent=root,
            title='Укажите путь к файлу arm_id.ini',
            initialdir="C:\\",
            filetypes=[('INI-file', '.ini')])
        root.destroy()
        var_zapuska = 1

    return path2conf, var_zapuska
