def PFI_State(path_dll, stattest, ustanovka):
	import ctypes

	newlib = ctypes.CDLL(path_dll)

	initport = b'PXI1Slot9'
	binitport = (ctypes.c_char * len(initport))(*initport)
	
	if ustanovka == 'SOT':
		newlib.DAQ_StatTestAC.restype = ctypes.c_int
		newlib.DAQ_StatTestAC.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.c_int]
		const_error = newlib.DAQ_StatTestAC(binitport, stattest)
	elif ustanovka == 'PKV':
		newlib.DAQ_StatTestPKV.restype = ctypes.c_int
		newlib.DAQ_StatTestPKV.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.c_int]
		const_error = newlib.DAQ_StatTestPKV(binitport, stattest)

	return const_error

# 0 - нет ошибки, 1000 - ошибка
