def imptstrests(input_donnes):
    import json

    input_donn = input_donnes.read().replace("\\", "")
    input_map = json.loads(input_donn)

    testresults = []
    value = []
    test_id = []

    for j in range(0, len(input_map)):
        testresults.append([{'TName': input_map[j].get('Name'), 'PathDll': input_map[j].get('PathDll'),
                             'NumResParams': len(input_map[j].get('TestResults'))}, []])

        value.append([])
        for i in range(0, len(input_map[j].get('TestResults'))):
            testresults[j][1].append({'Ind': input_map[j].get('TestResults')[i].get('Ind'),
                                      'Name': input_map[j].get('TestResults')[i].get('Name'),
                                      'ShortName': input_map[j].get('TestResults')[i].get('ShortName'),
                                      'Unit': input_map[j].get('TestResults')[i].get('Unit'),
                                      'NormaTable': input_map[j].get('TestResults')[i].get('NormaTable')})

            value[j].append({'Ind': input_map[j].get('TestResults')[i].get('Ind'),
                             'Name': input_map[j].get('TestResults')[i].get('Name'),
                             'Unit': input_map[j].get('TestResults')[i].get('Unit'),
                             'Value': input_map[j].get('TestResults')[i].get('Value'),
                             'AttrId': input_map[j].get('TestResults')[i].get('AttrId'),
                             'ShortName': input_map[j].get('TestResults')[i].get('ShortName'),
                             'NormaTable': input_map[j].get('TestResults')[i].get('NormaTable')})

        test_id.append(input_map[j].get('TestID'))

    return testresults, value, test_id
