def sorting(results, testresults, pornumtest):

    MaxGrArr = []
    Status = []
    FGroup = ''

    Group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    Results = ([float(i) for i in results])

    for j in range(0, len(Results)):  # цикл перемещения по количетву выходных параметров
        MaxGrArr.append(len(testresults[pornumtest][1][j].get('NormaTable')))  # определение макс количества групп
    MaxNumGroup = max(MaxGrArr)
    MaxGrArr.clear()

    StatArr = [{"TName": testresults[pornumtest][0].get('TName')}, {'results': results}, {'Status': ''}, {'FinalGroup': ''}]
    dict_group = {}

    for m in range(0, MaxNumGroup):
        dict_group["NameGroup " + Group[m]] = []
    StatArr.append(dict_group)

    for j in range(0, len(Results)):
        for k in range(0, len(testresults[pornumtest][1][j].get('NormaTable'))):  # цикл перемещения по количеству групп выходных параметров
            if (testresults[pornumtest][1][j].get('NormaTable')[k].get('Min') == testresults[pornumtest][1][j].get('NormaTable')[k].get('Max')) or\
                    ((testresults[pornumtest][1][j].get('NormaTable')[k].get('Min') == 0) and
                     (testresults[pornumtest][1][j].get('NormaTable')[k].get('Max') == 0)):
                StatArr[4].get("NameGroup " + Group[k]).append('')

            elif testresults[pornumtest][1][j].get('NormaTable')[k].get('Min') < Results[j] < testresults[pornumtest][1][j].get('NormaTable')[k].get('Max'):
                StatArr[4].get("NameGroup " + Group[k]).append('ГОДЕН')
            else:
                StatArr[4].get("NameGroup " + Group[k]).append('БРАК')

    for m in range(0, len(dict.keys(StatArr[4]))):
        Status.append('-')
        for k in range(0, len(StatArr[4].get('NameGroup ' + Group[m]))):
            if StatArr[4].get('NameGroup ' + Group[m])[k] == 'БРАК':
                Status.remove('-')
                Status.insert(m, 'БРАК')
                break
        if Status[m] == '-':
            Status.remove('-')
            Status.insert(m, 'ГОДЕН')

    for n in range(0, len(Status)):
        if Status[n] == 'ГОДЕН':
            StatArr[2].update({'Status': 'ГОДЕН'})
            break
        else:
            StatArr[2].update({'Status': 'БРАК'})

    for n in range(0, len(Status)):
        if Status[n] == 'ГОДЕН':
            FGroup = FGroup + Group[n]

        if len(FGroup) == 0:
            StatArr[3].update({'FinalGroup': ''})
        else:
            StatArr[3].update({'FinalGroup': FGroup[0]})

    return StatArr
