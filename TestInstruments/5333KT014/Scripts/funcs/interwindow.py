import os
from tkinter import *
from tkinter import messagebox as mb

number = []


def interwindow():
    def output(event):
        plate_number = entry_1.get()
        if (len(plate_number) > 0) and plate_number.isdigit():
            root.destroy()
        elif not plate_number.isdigit():
            entry_1.delete(0, END)
            plate_number = ""
        return str(plate_number)

    # Функция выхода из программы
    def on_closing():
        if mb.askokcancel("Выход", "Вы хотите выйти из программы?"):
            root.destroy()
            number.insert(0, "*")
        return number

    root = Tk()
    root.title("Введите данные о микросхеме")
    root.iconbitmap(str(os.path.abspath(os.path.dirname(sys.argv[0]))) + '\\atom.ico')

    # Gets the requested values of the height and widht.
    windowWidth = root.winfo_reqwidth()
    windowHeight = root.winfo_reqheight()

    # Gets both half the screen width/height and window width/height
    positionRight = int(root.winfo_screenwidth() / 2 - windowWidth / 2)
    positionDown = int(root.winfo_screenheight() / 2 - windowHeight / 2)

    # Positions the window in the center of the page.
    root.geometry("+{}+{}".format(positionRight, positionDown))

    label_1 = Label(root, text="Номер микросхемы:")

    entry_1 = Entry(root, width=30)

    label_1.grid(row=0, column=0, padx=10, pady=5, sticky=E)

    entry_1.grid(row=0, column=1, padx=10, pady=5)
    entry_1.focus()

    button1 = Button(root, pady=3, text="Ввести значение")
    button1.grid(columnspan=2, pady=10)

    button1.bind("<Button-1>", lambda e: number.insert(0, output(e)))
    root.bind('<Return>', lambda e: number.insert(0, output(e)))

    root.protocol("WM_DELETE_WINDOW", on_closing)
    root.mainloop()

    return number[0]
