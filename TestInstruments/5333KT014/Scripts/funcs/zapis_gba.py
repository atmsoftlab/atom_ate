def zapis_gba(path, test_plan_status):
    import os

    const_goden = 0
    const_brak = 0
    const_all = 0

    if os.path.exists(path + '\\clearstat.clearstat') is False:
        fle = open(path + '\\log_full_statistic.txt')
        str_const = fle.read()
        fle.close()
        const_goden = int(str_const.split(';')[0])
        const_brak = int(str_const.split(';')[1])
        const_all = int(str_const.split(';')[2])

        if test_plan_status == 0:
            const_goden = const_goden + 1
            const_brak = const_brak
        else:
            const_goden = const_goden
            const_brak = const_brak + 1

        const_all = const_all + 1

    elif os.path.exists(path + '\\clearstat.clearstat') is True:
        if test_plan_status == 0:
            const_goden = 1
            const_brak = 0
        else:
            const_goden = 0
            const_brak = 1

        const_all = 1

    fle = open(path + '\\log_full_statistic.txt', 'w')
    fle.write(str(const_goden) + ';' + str(const_brak) + ';' + str(const_all))
    fle.close()


# zapis_gba("C:\\TestInstruments\\5333KT014\\Scripts", 1)
