def getnumberfdb(connection, initdonn, path2savtxt):
    import tkinter as tk
    from funcs import connectdb
    from tkinter import messagebox as mb

    try:
        with connection.cursor() as cur:
            cur.execute("SELECT plate_number FROM atedb.testplan_results ORDER BY id DESC LIMIT 1")
            plate_number = cur.fetchall()[0].get('plate_number')

        status = 0

    except Exception as e:
        print(e)
        # status = 1
        # answer = True

        # root = tk.Tk()
        # root.withdraw()

        # while answer is True and status == 1:
        #     answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
        #                                                     "повторную попытку подключения?", icon="question")
        # if answer:
        status, connection = connectdb(initdonn, path2savtxt)

        if status == 0:
            with connection.cursor() as cur:
                cur.execute("SELECT plate_number FROM atedb.testplan_results ORDER BY id DESC LIMIT 1")
                plate_number = cur.fetchall()[0].get('plate_number')
        else:
            plate_number = 0

        # root.destroy()

    return int(plate_number) + 1, status
