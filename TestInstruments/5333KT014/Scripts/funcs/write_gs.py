def write_gs(path, result_group):
    import os

    if os.path.exists(path + '\\log_group_statistic.txt') is True and os.path.exists(path + '\\clearstat.clearstat') is False:
        fle = open(path + '\\log_group_statistic.txt')
        str_const = fle.read()
        fle.close()

        str_split = str_const.split('\n')

        num = 0
        for g in range(0, len(str_split)):
            if len(str_split[g]) != 0:
                num = num + 1

        if num != 0:

            massive_groups = {}

            for i in range(0, num):
                massive_groups[str(str_split[i].split(';')[0])] = str(str_split[i].split(';')[1])

            if massive_groups.get(result_group) is not None:
                massive_groups[result_group] = int(massive_groups.setdefault(result_group)) + 1

            keys = list(massive_groups.keys())
            vals = list(massive_groups.values())

            data = ''
            for i in range(0, len(massive_groups.keys())):
                data = data + str(keys[i]) + ';' + str(vals[i]) + '\n'

        else:
            if result_group == '':
                data = ''
            else:
                data = str(result_group) + ";1"

    elif os.path.exists(path + '\\log_group_statistic.txt') is True and os.path.exists(path + '\\clearstat.clearstat') is True:
        if result_group == "":
            data = ''
        else:
            data = str(result_group) + ';1'
            data.replace('\n', '')

    fle = open(path + '\\log_group_statistic.txt', 'w')
    fle.write(data)
    fle.close()


# write_gs('C:\\TestInstruments\\5333KT014\\Scripts', 'A')
