def resmodules(path):
    import ctypes

    newlib = ctypes.CDLL(path)

    # Cброс модулей
    newlib.HReset.restype = ctypes.c_int
    Status = newlib.HReset()

    return Status
