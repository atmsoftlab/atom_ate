def PFI_Start(path_dll, regim_measure, ustanovka):
	import ctypes

	# regim_measure - режим измерения входного сигнала DAQ: 0 - RSE (office), 1 - Differential (avoid)

	modulename = b'PXI1Slot8/ai3'

	newlib = ctypes.CDLL(path_dll)
	bufferstrmodname = (ctypes.c_char * len(modulename))(*modulename)
	
	if ustanovka == 'SOT':
		newlib.DAQ_NStartAC.restype = ctypes.c_int
		newlib.DAQ_NStartAC.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.c_int]
		const_start = newlib.DAQ_NStartAC(bufferstrmodname, regim_measure)
	elif ustanovka == 'PKV':
		newlib.DAQ_NStartPKV.restype = ctypes.c_int
		newlib.DAQ_NStartPKV.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.c_int]
		const_start = newlib.DAQ_NStartPKV(bufferstrmodname, regim_measure)

	return const_start

	# 0 - нет сигнала, 1 - старт, 1000 - ошибка


# print(PFI_Start('C:\\TestInstruments\\SVP_IS-8\\DLL\\SVP_IS-8.dll', 0, 'PKV'))
