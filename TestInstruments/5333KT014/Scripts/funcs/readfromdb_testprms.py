def readfromdb_testprms(connection, initdonn, duts_id, test_plan_id, path2savtxt):
    import json
    import hashlib
    import tkinter as tk
    from funcs import connectdb
    from tkinter import messagebox as mb

    testparams = []
    test_id = []
    inparam_id = []
    FName = []
    Inparam = []

    try:
        with connection.cursor() as cur:
            # cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) +
            #             "' AND arm_id = " + str(arm_id)) - с выбором arm_id
            cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
            buffer_tstplan = cur.fetchall()[0]

        json_buffer = json.loads(buffer_tstplan.get('structure'))

        for i in range(0, len(json_buffer)):
            test_id.append(json_buffer[i].get('TestId'))

        for j in range(0, len(test_id)):
            with connection.cursor() as cur:
                # cur.execute("SELECT test_function, inparam FROM atedb.test_elements WHERE id = " + str(test_id[j]) +
                #             " AND arm_id = " + str(arm_id)) - с выбором arm_id
                cur.execute("SELECT test_function, inparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                buffer_tstelem = cur.fetchall()[0]

            FName.append(buffer_tstelem.get('test_function'))
            inparam_id.append(buffer_tstelem.get('inparam'))

        for n in range(0, len(inparam_id)):
            with connection.cursor() as cur:
                # cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(inparam_id[n]) +
                #             " AND arm_id = " + str(arm_id)) - с выбором arm_id
                cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(inparam_id[n]))
                buffer_tstparam = cur.fetchall()[0]

            Inparam.append(json.loads(buffer_tstparam.get('parametr')))

        for k in range(0, len(inparam_id)):
            testparams.append([{'FName': FName[k], 'InputParamsName': [], 'InputParamsValue': []}])
            for m in range(0, len(Inparam[k])):
                testparams[k][0].get('InputParamsName').append(Inparam[k][m].get('Name'))
                testparams[k][0].get('InputParamsValue').append(Inparam[k][m].get('Value'))

        status = 0

    except Exception as e:
        print(e)
        # status = 1
        # answer = True

        # root = tk.Tk()
        # root.withdraw()

        # while answer is True and status == 1:
        #     answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
        #                                                     "повторную попытку подключения?", icon="question")
        # if answer:
        status, connection = connectdb(initdonn, path2savtxt)

        if status == 0:
            with connection.cursor() as cur:
                # cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) +
                #             "' AND arm_id = " + str(arm_id)) - с выбором arm_id
                cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
                buffer_tstplan = cur.fetchall()[0]

            json_buffer = json.loads(buffer_tstplan.get('structure'))

            for i in range(0, len(json_buffer)):
                test_id.append(json_buffer[i].get('TestId'))

            for j in range(0, len(test_id)):
                with connection.cursor() as cur:
                    # cur.execute("SELECT test_function, inparam FROM atedb.test_elements WHERE id = " + str(test_id[j]) +
                    #             " AND arm_id = " + str(arm_id)) - с выбором arm_id
                    cur.execute("SELECT test_function, inparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                    buffer_tstelem = cur.fetchall()[0]

                FName.append(buffer_tstelem.get('test_function'))
                inparam_id.append(buffer_tstelem.get('inparam'))

            for n in range(0, len(inparam_id)):
                with connection.cursor() as cur:
                    # cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(inparam_id[n]) +
                    #             " AND arm_id = " + str(arm_id)) - с выбором arm_id
                    cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(inparam_id[n]))
                    buffer_tstparam = cur.fetchall()[0]

                Inparam.append(json.loads(buffer_tstparam.get('parametr')))

            for k in range(0, len(inparam_id)):
                testparams.append([{'FName': FName[k], 'InputParamsName': [], 'InputParamsValue': []}])
                for m in range(0, len(Inparam[k])):
                    testparams[k][0].get('InputParamsName').append(Inparam[k][m].get('Name'))
                    testparams[k][0].get('InputParamsValue').append(Inparam[k][m].get('Value'))

        # root.destroy()

    return testparams, status
