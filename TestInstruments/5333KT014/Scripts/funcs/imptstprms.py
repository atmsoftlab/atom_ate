def imptstprms(input_donnes):
    import json

    input_donn = input_donnes.read().replace("\\", "")
    input_map = json.loads(input_donn)

    testparams = []
    input_parameters = []

    for j in range(0, len(input_map)):
        testparams.append([{'FName': input_map[j].get('FunctionName'), 'InputParamsName': [], 'InputParamsValue': []}])
        for i in range(0, len(input_map[j].get('InputParameters'))):
            testparams[j][0].get('InputParamsName').append(input_map[j].get('InputParameters')[i].get('Name'))
            testparams[j][0].get('InputParamsValue').append(input_map[j].get('InputParameters')[i].get('Value'))
        input_parameters.append([input_map[j].get('InputParameters')])

    return testparams, input_parameters
