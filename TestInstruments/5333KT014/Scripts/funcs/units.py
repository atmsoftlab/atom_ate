def units(result, coef_scale):

    if coef_scale == 'А':
        res_preobraz = result

    elif coef_scale == 'мА':
        res_preobraz = result / pow(10, -3)

    elif coef_scale == 'мкА':
        res_preobraz = result / pow(10, -6)

    elif coef_scale == 'нА':
        res_preobraz = result / pow(10, -9)

    elif coef_scale == 'пкА':
        res_preobraz = result / pow(10, -12)

    elif coef_scale == 'В':
        res_preobraz = result

    elif coef_scale == 'мВ':
        res_preobraz = result / pow(10, -3)

    elif coef_scale == 'мкВ':
        res_preobraz = result / pow(10, -6)

    elif coef_scale == 'нВ':
        res_preobraz = result / pow(10, -9)

    elif coef_scale == 'пкВ':
        res_preobraz = result / pow(10, -12)

    else:
        res_preobraz = result

    return res_preobraz


# print(units(2.5E-6, 'мкА'))
