def connectdb(initdb, path2savtxt):  # host, port, user, password, db
    import pymysql.cursors
    import tkinter as tk
    from tkinter import messagebox as mb
    from funcs import status_izm

    connection = None
    connect = 0
    flag_status = 0
    status = 0
    port = initdb.get('Port')

    if type(port) != int:
        port = int(port)

    # while connect == 0:
    while flag_status != 1:
        try:
            connection = pymysql.connect(host=initdb.get('Server'),
                                         port=port,
                                         user=initdb.get('UserId'),
                                         password=initdb.get('Password'),
                                         db=initdb.get('Database'),
                                         charset='utf8mb4',
                                         cursorclass=pymysql.cursors.DictCursor)
            flag_status = 1
            status = 0
            # print("DB connection is good!")
        except Exception as e:
            print(e)
            # answer = mb.showerror(title="Ошибка подключения к БД", message="Ошибка подключения к базе данных.\n Устраните ошибку подключения и выполните повторный запуск.")
            # if answer is not True:
            connect += 1
            if connect > 2:
                flag_status = 1
                status = 1

    if status == 1:
        root = tk.Tk()
        root.withdraw()
        mb.showerror(title="Ошибка подключения к БД", message="Ошибка подключения к базе данных.\n Устраните ошибку подключения и выполните повторный запуск.")
        root.destroy()
        status_izm(path2savtxt, 'ОШИБКА')

    return status, connection
