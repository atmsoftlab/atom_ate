#include "extcode.h"
#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Test00
 */
void __cdecl Test00(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErr);
/*!
 * Test02
 */
void __cdecl Test02(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErr);
/*!
 * Test03
 */
void __cdecl Test03(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErrOut);
/*!
 * Test04
 */
void __cdecl Test04(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErrOut);
/*!
 * Test05
 */
void __cdecl Test05(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErrOut);
/*!
 * Test06
 */
void __cdecl Test06(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErrOut);
/*!
 * Test13
 */
void __cdecl Test13(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenStrErrOut);
/*!
 * Test07
 */
void __cdecl Test07(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test08
 */
void __cdecl Test08(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test09
 */
void __cdecl Test09(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test10
 */
void __cdecl Test10(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test11
 */
void __cdecl Test11(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test12
 */
void __cdecl Test12(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test14
 */
void __cdecl Test14(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test15
 */
void __cdecl Test15(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test16
 */
void __cdecl Test16(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test18
 */
void __cdecl Test18(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test17
 */
void __cdecl Test17(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test19
 */
void __cdecl Test19(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);
/*!
 * Test01
 */
void __cdecl Test01(double ArrayIn[], int32_t Regim, double ArrayOut[], 
	char StrErrOut[], int32_t lenIn, int32_t lenOut, int32_t lenErrStr);

MgErr __cdecl LVDLLStatus(char *errStr, int errStrLen, void *module);

void __cdecl SetExecuteVIsInPrivateExecutionSystem(Bool32 value);

#ifdef __cplusplus
} // extern "C"
#endif

