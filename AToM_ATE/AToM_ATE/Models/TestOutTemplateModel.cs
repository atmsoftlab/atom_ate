﻿using AToM_ATE.Classes;
using AToM_ATE.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class TestOutTemplateModel : ViewModelBase
    {
        public List<KeyValuePair<int, string>> Duts { get; set; }
        public KeyValuePair<int, string> SelectedDut
        {
            get => _SelectedDut;
            set
            {
                SetProperty(ref _SelectedDut, value);
                TestAtributsSource = new List<TestAttribute>() { new TestAttribute() { Id = -1 } };
                TestAtributsSource.AddRange(AppFunc.GetAtrNamesForDuts(SelectedDut.Key, (int)TypeParam.Out));
                LoadedTestAtributsSource = new List<TestAttribute>(TestAtributsSource);
                OnPropertyChanged("TestAtributsSource");
                TestsFromDb = AppFunc.GetTestParamsForDuts(SelectedDut.Key, TypeParam.Out);
                OnPropertyChanged("TestsFromDb");
                SelectedTestsFromDb = null;
            }
        }
        private KeyValuePair<int, string> _SelectedDut;

        public List<TestAttribute> TestAtributsSource { get; set; }
        public List<TestAttribute> LoadedTestAtributsSource { get; set; }

        public List<TestParam> SourceParam { get; set; }

        public string CommentForCurrentCollection
        {
            get => _CommentForCurrentCollection;
            set => SetProperty(ref _CommentForCurrentCollection, value);
        }
        private string _CommentForCurrentCollection;
        public List<string> NormaTableHeader { get; set; }
        public List<Tuple<int, string, List<TestParam>>> TestsFromDb { get; set; }
        public Tuple<int, string, List<TestParam>> SelectedTestsFromDb
        {
            get => _SelectedTestsFromDb;
            set
            {
                SetProperty(ref _SelectedTestsFromDb, value);
                if (SourceParam == null)
                    SourceParam = new List<TestParam>();
                if ((value?.Item3?.Count ?? 0) != 0)
                {
                    GenerateSourceParam(value.Item3.Count);
                    ClassificationGroupCount = value.Item3[0].NormaTable?.Count ?? 0;
                    value.Item3.ForEach(c =>
                    {
                        if (c.Ind < SourceParam.Count)
                            SourceParam[c.Ind] = c;
                    });
                }
                SourceParam = new List<TestParam>(SourceParam);
                OnPropertyChanged("SourceParam");
                SetNormaTableHeader();
            }
        }
        private Tuple<int, string, List<TestParam>> _SelectedTestsFromDb;

        List<string> WordsOfGroup = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "G", "K", "L", "M",
                                                         "N", "O","P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        public int ClassificationGroupCount
        {
            get => _ClassificationGroupCount;
            set
            {
                SetProperty(ref _ClassificationGroupCount, value);
                SourceParam.ForEach(c =>
                {
                    List<Norma> norm = new List<Norma>();

                    if (value > WordsOfGroup.Count - 1)
                        value = WordsOfGroup.Count - 1;
                    int startIndex = 0;
                    if ((c?.NormaTable?.Count ?? 0) > 0)
                    {
                        startIndex = c.NormaTable.Count;
                        norm = new List<Norma>(c.NormaTable);
                        if (norm.Count > value)
                            norm = norm.GetRange(0, value);
                    }
                    for (int i = startIndex; i < value; i++)
                        norm.Add(new Norma() { NameGroup = WordsOfGroup[i] });
                    c.NormaTable = new List<Norma>(norm);
                });
                SourceParam = new List<TestParam>(SourceParam);
                OnPropertyChanged("SourceParam");
                SetNormaTableHeader();
            }
        }
        private int _ClassificationGroupCount;

        public ICommand SaveParams { get; set; }
        public ICommand ShowFilter { get; set; }
        public ICommand SaveSelectedParams { get; set; }

        public TestOutTemplateModel()
        {
            try
            {
                UpdateDutsList();
                SaveParams = new RelayCommand(c => { SaveParam(0); });
                ShowFilter = new RelayCommand(c => { EvShowFilter(); });
                SaveSelectedParams = new RelayCommand(c => { SaveParam(SelectedTestsFromDb.Item1); });
            }
            catch { }
        }

        public void UpdateDutsList()
        {
            Duts = AppFunc.GetDuts();
            OnPropertyChanged("Duts");
        }

        private void GenerateSourceParam(int count)
        {
            SourceParam = new List<TestParam>();
            for (int i = 0; i < count; i++)
                SourceParam.Add(new TestParam() { Ind = i });
            OnPropertyChanged("SourceParam");
            SetNormaTableHeader();
        }

        private void SetNormaTableHeader()
        {
            try
            {
                NormaTableHeader = new List<string>();
                if ((SourceParam?.Count ?? 0) > 0 && (SourceParam.FirstOrDefault().NormaTable?.Count ?? 0) > 0)
                    foreach (Norma n in SourceParam.FirstOrDefault().NormaTable)
                        NormaTableHeader.Add("Гр.   Min            Max");
                OnPropertyChanged("NormaTableHeader");
            }
            catch
            { }
        }

        private void EvShowFilter()
        {
            try
            {

                Dictionary<int, int> HavenParams = new Dictionary<int, int>();
                if ((SourceParam?.Count ?? 0) > 0)
                    SourceParam.ForEach(c =>
                    {
                        if (!HavenParams.ContainsKey(c.AttrId))
                            HavenParams.Add(c.AttrId, c.Ind);
                    });

                AttributeFilterModel dtcnt = new AttributeFilterModel(LoadedTestAtributsSource.Where(a => a.Id != -1).ToList(), HavenParams);
                (new AttributeFilterView() { DataContext = dtcnt }).ShowDialog();

                List<AttrFilterStructure> chekedList = dtcnt.Source.Where(s => s.Check).OrderBy(s => s.Ind).ToList();
                TestAtributsSource = new List<TestAttribute>() { new TestAttribute() { Id = -1 } };
                chekedList.ForEach(cl => { TestAtributsSource.Add(new TestAttribute() { Id = cl.AttrId, Name = cl.Name, ShortName = cl.ShortName, Unit = cl.Unit }); });
                OnPropertyChanged("TestAtributsSource");
                List<TestParam> NewCollectionSP = new List<TestParam>();
                for (int i = 0; i < chekedList.Count(); i++)
                {
                    TestParam AddElement = new TestParam();
                    if (SourceParam.Any(c => c.AttrId == chekedList[i].AttrId))
                    {
                        AddElement = SourceParam.FirstOrDefault(c => c.AttrId == chekedList[i].AttrId);
                        AddElement.Ind = i;
                    }
                    else
                    {
                        AddElement = new TestParam()
                        {
                            Ind = i,
                            AttrId = chekedList[i].AttrId,
                            Name = chekedList[i].Name,
                            ShortName = chekedList[i].ShortName,
                            Unit = chekedList[i].Unit
                        };
                        List<Norma> norm = new List<Norma>();
                        for (int k = 0; k < ClassificationGroupCount; k++)
                            norm.Add(new Norma() { NameGroup = WordsOfGroup[k] });
                        AddElement.NormaTable = new List<Norma>(norm);
                    }
                    NewCollectionSP.Add(AddElement);
                }
                SourceParam = new List<TestParam>(NewCollectionSP);
                OnPropertyChanged("SourceParam");
                SetNormaTableHeader();
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage("Попытка сохранить некорректные данные!");
                AppFunc.ShowMessage(ee.Message); 
            }
        }

        private void SaveParam(int id)
        {
            if (string.IsNullOrWhiteSpace(CommentForCurrentCollection))
            {
                AppFunc.ShowMessage("Необходимо указать наименование набора");
                return;
            }
            try
            {
                List<TestParam> noEmptyParam = new List<TestParam>();
                int i = 0;
                SourceParam.OrderBy(c => c.Ind).ToList().ForEach(p =>
                {
                    if (p.AttrId != -1 && !string.IsNullOrEmpty(p.Name))
                    {
                        p.Ind = i;
                        p.Name = LoadedTestAtributsSource.FirstOrDefault(k => k.Id == p.AttrId).Name;
                        p.ShortName = LoadedTestAtributsSource.FirstOrDefault(k => k.Id == p.AttrId).ShortName;
                        p.Unit = LoadedTestAtributsSource.FirstOrDefault(k => k.Id == p.AttrId).Unit;
                        i++;
                        noEmptyParam.Add(p);
                    }
                });
                if (noEmptyParam.Count > 0)
                    AppFunc.SaveTestParameters(id, noEmptyParam.GetJSONString(), (int)TypeParam.Out, SelectedDut.Key, CommentForCurrentCollection);

                TestsFromDb = AppFunc.GetTestParamsForDuts(SelectedDut.Key, TypeParam.Out);
                OnPropertyChanged("TestsFromDb");
                //GenerateSourceParam(LoadedTestAtributsSource.Count);
                if (id != 0)
                    SelectedTestsFromDb = TestsFromDb.FirstOrDefault(c => c.Item1 == id);
                else
                    SelectedTestsFromDb = TestsFromDb.LastOrDefault();
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage("Попытка сохранить некорректные данные!");
                AppFunc.ShowMessage(ee.Message);
            }
        }
    }
}
