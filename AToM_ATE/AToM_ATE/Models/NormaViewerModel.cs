﻿using AToM_ATE.Classes;
using System.Collections.Generic;

namespace AToM_ATE.Models
{
    public class NormaViewerModel : ViewModelBase
    {
        public class NormaSouceStruct
        {
            public string Параметр { get; set; }
            public string Группа { get; set; }
            public double Min { get; set; }
            public double Max { get; set; }
        }

        public List<KeyValuePair<string, List<NormaSouceStruct>>> NormaByAllTestList { get; set; }

        public string Title
        {
            get => _Title;
            set => SetProperty(ref _Title, value);
        }
        private string _Title;


        public NormaViewerModel(string title, List<TestPlanElement> Sequence)
        {
            Title = title;
            NormaByAllTestList = new List<KeyValuePair<string, List<NormaSouceStruct>>>();
            Sequence.ForEach(s =>
            {
                List<NormaSouceStruct> norms = new List<NormaSouceStruct>();

                if ((s.TestResults?.Count ?? 0) > 0)
                {
                    s.TestResults.ForEach(c =>
                    {
                        if (c.NormaTable != null)
                        {
                            c.NormaTable.ForEach(n =>
                            {
                                if (n != null)
                                    norms.Add(new NormaSouceStruct() { Параметр = c.Name, Группа = n.NameGroup, Min = n.Min, Max = n.Max });
                            });
                        }

                    });
                }
                NormaByAllTestList.Add(new KeyValuePair<string, List<NormaSouceStruct>>(s.Name, norms));
            });


            OnPropertyChanged("NormaByAllTestList");
        }

    }
}
