﻿using AToM_ATE.Classes;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class ConstructorTestElementModel : ViewModelBase
    {
        private const string TEMPLATE_FIELD_STRING = "...";
        public List<KeyValuePair<int, string>> Duts { get; set; }
        public KeyValuePair<int, string> SelectedDut
        {
            get => _SelectedDut;
            set
            {
                SetProperty(ref _SelectedDut, value);
                InputParameters = AppFunc.GetTestParam(SelectedDut.Key, (int)TypeParam.In);
                OutputParameters = AppFunc.GetTestParam(SelectedDut.Key, (int)TypeParam.Out);
                OnPropertyChanged("InputParameters");
                OnPropertyChanged("OutputParameters");
                setDefaultPathesToForm(value);
            }
        }

        private void setDefaultPathesToForm(KeyValuePair<int, string> value)
        {
            PathToDll = value.Value + "\\DLL\\" + TEMPLATE_FIELD_STRING;
            Function = "Test" + TEMPLATE_FIELD_STRING;
        }

        private KeyValuePair<int, string> _SelectedDut;

        public string PathToDll
        {
            get => _PathToDll;
            set => SetProperty(ref _PathToDll, value);
        }
        private string _PathToDll;

        public string Function
        {
            get => _Function;
            set => SetProperty(ref _Function, value);
        }
        private string _Function;

        public string TestName
        {
            get => _TestName;
            set => SetProperty(ref _TestName, value);
        }
        private string _TestName;

        public List<KeyValuePair<int, string>> InputParameters { get; set; }
        public List<KeyValuePair<int, string>> OutputParameters { get; set; }

        public KeyValuePair<int, string> SelectedInParam
        {
            get => _SelectedInParam;
            set => SetProperty(ref _SelectedInParam, value);
        }
        private KeyValuePair<int, string> _SelectedInParam;

        public KeyValuePair<int, string> SelectedOutParam
        {
            get => _SelectedOutParam;
            set => SetProperty(ref _SelectedOutParam, value);
        }
        private KeyValuePair<int, string> _SelectedOutParam;

        public ICommand SaveTest { get; set; }

        public ConstructorTestElementModel()
        {
            try
            {
                UpdateDutsList();

                SaveTest = new RelayCommand(c =>
                {
                    if (CheckForEmptyParam())
                        AppFunc.SaveTestElement(TestName, SelectedDut.Key, Function, PathToDll, SelectedInParam.Key, SelectedOutParam.Key);
                });
            }
            catch
            {
            }
        }

        public void UpdateDutsList()
        {
            Duts = AppFunc.GetDuts();
            OnPropertyChanged("Duts");
        }

        private bool CheckForEmptyParam()
        {
            if (string.IsNullOrEmpty(Function))
            {
                AppFunc.ShowMessage("Не указана функция");
                return true;
                /* return false;  */
            }
            if (string.IsNullOrEmpty(TestName))
            {
                AppFunc.ShowMessage("Не указано название теста");
                return false;
            }
            if (SelectedInParam.Key == 0)
            {
                AppFunc.ShowMessage("Не выбраны входные параметры");
                return false;
            }
            if (SelectedOutParam.Key == 0)
            {
                AppFunc.ShowMessage("Не выбраны выходные параметры");
                return false;
            }
            if (Function.Contains(TEMPLATE_FIELD_STRING) || PathToDll.Contains(TEMPLATE_FIELD_STRING))
            {
                AppFunc.ShowMessage("Не изменены шаблоны пути к DLL и/или названия функции");
                return false;
            }

            return true;
        }
    }
}
