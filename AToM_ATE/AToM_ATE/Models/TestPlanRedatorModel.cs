﻿using AToM_ATE.Classes;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class TestPlanRedatorModel : ViewModelBase
    {
        public List<KeyValuePair<int, string>> Duts { get; set; }
        public KeyValuePair<int, string> SelectedDut
        {
            get => _SelectedDut;
            set
            {
                SetProperty(ref _SelectedDut, value);
                TestElements = new List<KeyValuePair<int, string>>();
                if (value.Key != 0)
                    TestElements = AppFunc.GetTestElementsSource(value.Key);
                OnPropertyChanged("TestElements");
                GenerateSourceParam();
                FillTestPlanList();
                setDefaults(value);
            }
        }

        private void setDefaults(KeyValuePair<int, string> value)
        {
            PathToRunPyScript = value.Value + "\\Scripts\\run.py";
        }

        private KeyValuePair<int, string> _SelectedDut;

        public List<KeyValuePair<int, string>> TestElements { get; set; }

        public List<TestPlanItem> TestPlanItems { get; set; }

        public string NameTextPlan
        {
            get => _NameTextPlan;
            set => SetProperty(ref _NameTextPlan, value);
        }
        private string _NameTextPlan;

        public string PathToRunPyScript
        {
            get => _PathToRunPyScript;
            set => SetProperty(ref _PathToRunPyScript, value);
        }
        private string _PathToRunPyScript;

        public List<TestPlanStruct> TestPlans { get; set; }
        public TestPlanStruct SelectedTestPlan
        {
            get => _SelectedTestPlan;
            set
            {
                SetProperty(ref _SelectedTestPlan, value);
                FillTestListAndOtherInfo();
            }
        }
        private TestPlanStruct _SelectedTestPlan;

        public ICommand ShowAttributRedactor { get; set; }
        public ICommand ShowTestParametersRedactor { get; set; }
        public ICommand ShowTestOutTemplate { get; set; }
        public ICommand ShowConstructorTestElement { get; set; }
        public ICommand SaveTestPlan { get; set; }
        public ICommand CreateNewTestPlan { get; set; }
        public ICommand AddNewDuts { get; set; }
        public ICommand DeleteTestPlan { get; set; }

        public TestPlanRedatorModel()
        {
            UpdateDutsList();

            SaveTestPlan = new RelayCommand(c => EvSaveTestPlan(false));
            CreateNewTestPlan = new RelayCommand(c => EvSaveTestPlan(true));

            DeleteTestPlan = new RelayCommand(c =>
            {
                if (MessageBox.Show("Удалить выбранный тест-план?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;

                if ((SelectedTestPlan?.Id ?? 0) != 0)
                {
                    AppFunc.HideTestPlan(SelectedTestPlan.Id, SelectedTestPlan.Comment);
                    FillTestPlanList();
                    FillTestListAndOtherInfo();
                    GenerateSourceParam();
                }
                else
                    AppFunc.ShowMessage("Выберите план из списка!");
            });
        }

        public void UpdateDutsList()
        {
            Duts = AppFunc.GetDuts();
            OnPropertyChanged("Duts");
        }

        private bool CheckSelectedDuts()
        {
            bool res = SelectedDut.Key != 0;
            if (!res)
                AppFunc.ShowMessage("Выберите изделие!");
            return res;
        }

        private void GenerateSourceParam()
        {
            TestPlanItems = new List<TestPlanItem>();
            for (int i = 0; i < 100; i++)
                TestPlanItems.Add(new TestPlanItem());
            OnPropertyChanged("TestPlanItems");
        }

        private void EvSaveTestPlan(bool createNew)
        {
            if (!CheckSelectedDuts())
                return;
            if (string.IsNullOrEmpty(NameTextPlan))
            {
                AppFunc.ShowMessage("Укажите название плана!");
                return;
            }
            if (string.IsNullOrEmpty(PathToRunPyScript))
            {
                AppFunc.ShowMessage("Укажите путь до скрипта!");
                return;
            }

            try
            {
                List<TestPlanItem> NoEmpty = new List<TestPlanItem>();
                TestPlanItems.ForEach(c =>
                {
                    if (c.TestId != 0)
                        NoEmpty.Add(c);
                });
                if (NoEmpty.Count != 0)
                {
                    int id = 0;
                    if (!createNew && SelectedTestPlan != null)
                        id = SelectedTestPlan.Id;
                    AppFunc.SaveTestPlan(id, NoEmpty.GetJSONString(), SelectedDut.Key, NameTextPlan, PathToRunPyScript);

                }
                else
                    AppFunc.ShowMessage("Нет выбранных элементов!");
            }
            catch
            { }
        }

        private void FillTestPlanList()
        {
            try
            {
                TestPlans = AppFunc.GetTestPlans(SelectedDut.Key);
                OnPropertyChanged("TestPlans");
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
        }

        private void FillTestListAndOtherInfo()
        {
            try
            {
                if (TestPlanItems.Count > 0 && SelectedTestPlan != null)
                {
                    TestPlanItems.ForEach(c => c.TestId = 0);
                    for (int i = 0; i < SelectedTestPlan.Items.Count; i++)
                        TestPlanItems[i].TestId = SelectedTestPlan.Items[i].TestId;
                }
                TestPlanItems = new List<TestPlanItem>(TestPlanItems);
                OnPropertyChanged("TestPlanItems");
                NameTextPlan = SelectedTestPlan?.Comment ?? "";
                PathToRunPyScript = SelectedTestPlan?.PathToScipt ?? "";
            }
            catch
            { }
        }
    }
}
