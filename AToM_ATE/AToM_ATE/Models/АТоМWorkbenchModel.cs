﻿using AToM_ATE.Classes;
using AToM_ATE.Views;
using AutoUpdaterDotNET;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace AToM_ATE.Models
{
    public class АТоМWorkbenchModel : ViewModelBase
    {
        public string SelectPythonPathBackground
        {
            get => _SelectPythonPathBackground;
            set => SetProperty(ref _SelectPythonPathBackground, value);
        }
        private string _SelectPythonPathBackground = "LightGray";

        public string PathToPython
        {
            get => _PathToPython;
            set => SetProperty(ref _PathToPython, value);
        }
        private string _PathToPython = "Не определён!";

        public string Title
        {
            get => _Title;
            set => SetProperty(ref _Title, value);
        }

        private string _Title = "АТоМ-АТЕ";

        public TestStarterControlModel TestStarterControlModelDtcnt { get; set; }
        public AddNewDutsModel AddNewDutsModelDtcnt { get; set; }
        public AttrTestParamRedactorModel AttrTestParamRedactorModelDtcnt { get; set; }
        public TestParametersRedactorModel TestParametersRedactorModelDtcnt { get; set; }
        public TestOutTemplateModel TestOutTemplateModelDtcnt { get; set; }
        public ConstructorTestElementModel ConstructorTestElementModelDtcnt { get; set; }
        public TestPlanRedatorModel TestPlanRedatorModelDtcnt { get; set; }
        private string SettingsFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\PythonPath.ate";

        public ICommand StartExternalRedactor { get; set; }
        public ICommand SelectPythonPath { get; set; }
        public ICommand RunReportModule { get; set; }
        public ICommand PrintAbout { get; set; }
        public ICommand CheckUpdate { get; set; }

        public AToMWorkbenchView View { get; set; }

        public string _StatusBarText1;
        public string StatusBarText1 { get => _StatusBarText1; set => SetProperty(ref _StatusBarText1, value); }

        private double progressBar1;
        public double ProgressBar1 { get => progressBar1; set => SetProperty(ref progressBar1, value); }

        private string statusBarText2;
        public string StatusBarText2 { get => statusBarText2; set => SetProperty(ref statusBarText2, value); }

        public АТоМWorkbenchModel()
        {
            try
            {
                AppFunc.ChekRootDllDirrectories();
                AppFunc.UpdateDutsList();
                TestStarterControlModelDtcnt = new TestStarterControlModel();
                OnPropertyChanged("TestStarterControlModelDtcnt");
                AddNewDutsModelDtcnt = new AddNewDutsModel() { MainDataContext = this };
                OnPropertyChanged("AddNewDutsModelDtcnt");
                AttrTestParamRedactorModelDtcnt = new AttrTestParamRedactorModel();
                OnPropertyChanged("AttrTestParamRedactorModelDtcnt");
                TestParametersRedactorModelDtcnt = new TestParametersRedactorModel();
                OnPropertyChanged("TestParametersRedactorModelDtcnt");
                TestOutTemplateModelDtcnt = new TestOutTemplateModel();
                OnPropertyChanged("TestOutTemplateModelDtcnt");
                ConstructorTestElementModelDtcnt = new ConstructorTestElementModel();
                OnPropertyChanged("ConstructorTestElementModelDtcnt");
                TestPlanRedatorModelDtcnt = new TestPlanRedatorModel();
                OnPropertyChanged("TestPlanRedatorModelDtcnt");

                // Thread SocketThread = new Thread(() => SocketServer.StartServer());
                // SocketThread.Start();

                Task.Factory.StartNew(() => SocketServer.StartServer());

                StartExternalRedactor = new RelayCommand(c =>
                {
                    try
                    {
                        string bat = $"{AppFunc.RoorDirForDll}\\editstart.cmd";
                        if (!File.Exists(bat))
                        {
                            AppFunc.ShowMessage($"Скрипт запуска внешнего редактора не найден: {bat}");
                            return;
                        }
                        Task.Factory.StartNew(() =>
                        {

                            Process proc = new Process()
                            {
                                StartInfo = new ProcessStartInfo()
                                {
                                    FileName = bat,
                                    UseShellExecute = false,
                                    RedirectStandardOutput = true,
                                    RedirectStandardError = true,
                                    CreateNoWindow = true,
                                    WindowStyle = ProcessWindowStyle.Hidden
                                }
                            };
                            proc.Start();
                        });
                    }
                    catch
                    { }
                });

                SelectPythonPath = new RelayCommand(o => EvSelectPythonPath());
                RunReportModule = new RelayCommand(o => EvRunReportModule());
                PrintAbout = new RelayCommand(o => EvPrintAbout());
                CheckUpdate = new RelayCommand(o => EvCheckUpdate());

                if (File.Exists(SettingsFile))
                {
                    StreamReader f = new StreamReader(SettingsFile);
                    PathToPython = f.ReadLine();
                    f.Close();
                }

                if (PathToPython == "Не определён!")
                    SelectPythonPathBackground = "Red";

                var version = Assembly.GetExecutingAssembly().GetName().Version;
                DateTime buildDate = new DateTime(2000, 1, 1).AddDays(version.Build).AddSeconds(version.Revision * 2);

                Title += $" сборка {buildDate.ToShortTimeString()} {buildDate.ToShortDateString()}";

                StatusBarText1 = "StatusBarText1";
                StatusBarText2 = "StatusBarText2";
                ProgressBar1 = 98;

            }
            catch
            { }
        }

        private void EvSelectPythonPath()
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog()
                {
                    CheckFileExists = false,
                    CheckPathExists = true,
                    Multiselect = false,
                    RestoreDirectory = false,
                    Title = "Выберите файл запуска программы",
                    Filter = "Прогамма (*.exe)|*.exe"

                };

                if (dialog.ShowDialog() == true)
                    PathToPython = dialog.FileName;

                if (TestStarterControlModelDtcnt != null)
                    TestStarterControlModelDtcnt.PathToPython = PathToPython;

                if (string.IsNullOrEmpty(PathToPython))
                    SelectPythonPathBackground = "Red";
                else
                    SelectPythonPathBackground = "LightGray";

                using (StreamWriter sw = new StreamWriter(SettingsFile, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(PathToPython);
                }
            }
            catch
            { }
        }

        private void EvRunReportModule()
        {
            try
            {
                string PathToReportModeule = $"{AppFunc.RoorDirForDll}\\Reports\\runreports.py";
                if (!File.Exists(PathToReportModeule))
                {
                    AppFunc.ShowMessage($"Скрипт запуска модуля отчётов не найден: {PathToReportModeule}");
                    return;
                }
                Task.Factory.StartNew(() =>
                {
                    //Гуи питона по человечески запускаться не хочет. А через батник нормально.
                    string Arguments = $"{PathToPython} {PathToReportModeule} \nexit";
                    string bat = $"{Environment.CurrentDirectory}\\run.bat";
                    File.WriteAllText(bat, Arguments);
                    Process proc = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = bat,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            CreateNoWindow = true,
                            WindowStyle = ProcessWindowStyle.Hidden
                        }
                    };
                    proc.Start();
                });
            }
            catch
            { }
        }

        private void EvPrintAbout()
        {
            try
            {
                AppFunc.ShowMessage("Функция пока не доступна!");
            }
            catch
            { }
        }

        private void EvCheckUpdate()
        {
            try
            {
                //Assembly myAssembly = Assembly.GetEntryAssembly();
                //AToMWorkbenchView AWB = new AToMWorkbenchView();
                //AWB.LabelVersion.Content = $"Current Version : {myassembly.GetName().Version}";

                //Thread.CurrentThread.CurrentCulture =
                //    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru");
                //AutoUpdater.LetUserSelectRemindLater = true;
                //AutoUpdater.RemindLaterTimeSpan = RemindLaterFormat.Minutes;
                //AutoUpdater.RemindLaterAt = 1;
                //AutoUpdater.ReportErrors = true;
                //DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMinutes(2) };
                //timer.Tick += delegate { AutoUpdater.Start("https://www.niiatm.ru/atom/distr/update.xml"); };
                //timer.Start();

                //Console.WriteLine("AutoUpdater, my aasembly>:" + myAssembly);
                AutoUpdater.Start("https://www.niiatm.ru/atom/distr/update.xml");

            }
            catch (Exception ex)
            {
                Console.WriteLine("AutoUpdater>:" + ex.Message);
            }
        }





    }
}

