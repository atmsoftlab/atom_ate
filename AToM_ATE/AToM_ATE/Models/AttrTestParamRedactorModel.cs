﻿using AToM_ATE.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class AttrTestParamRedactorModel : ViewModelBase
    {
        public List<KeyValuePair<int, string>> Duts { get; set; }
        public KeyValuePair<int, string> SelectedDut
        {
            get => _SelectedDut;
            set
            {
                SetProperty(ref _SelectedDut, value);
                DrawAttrLists();
            }
        }
        private KeyValuePair<int, string> _SelectedDut;

        public string TextForInputParam
        {
            get => _TextForInputParam;
            set => SetProperty(ref _TextForInputParam, value);
        }
        private string _TextForInputParam;

        public string TextForShortNameInputParam
        {
            get => _TextForShortNameInputParam;
            set => SetProperty(ref _TextForShortNameInputParam, value);
        }
        private string _TextForShortNameInputParam;

        public string TextForUnitInputParam
        {
            get => _TextForUnitInputParam;
            set => SetProperty(ref _TextForUnitInputParam, value);
        }
        private string _TextForUnitInputParam;

        public string TextForOutParam
        {
            get => _TextForOutParam;
            set => SetProperty(ref _TextForOutParam, value);
        }
        private string _TextForOutParam;

        public string TextForShortNameOutParam
        {
            get => _TextForShortNameOutParam;
            set => SetProperty(ref _TextForShortNameOutParam, value);
        }
        private string _TextForShortNameOutParam;

        public string TextForUnitOutParam
        {
            get => _TextForUnitOutParam;
            set => SetProperty(ref _TextForUnitOutParam, value);
        }
        private string _TextForUnitOutParam;

        public List<TestAttribute> InParam { get; set; }
        public TestAttribute SelectedInParam
        {
            get => _SelectedInParam;
            set
            {
                SetProperty(ref _SelectedInParam, value);
                TextForInputParam = value?.Name;
                TextForShortNameInputParam = value?.ShortName;
                TextForUnitInputParam = value?.Unit;
            }
        }
        private TestAttribute _SelectedInParam;

        public List<TestAttribute> OutParam { get; set; }
        public TestAttribute SelectedOutParam
        {
            get => _SelectedOutParam;
            set
            {
                SetProperty(ref _SelectedOutParam, value);
                TextForOutParam = value?.Name;
                TextForShortNameOutParam = value?.ShortName;
                TextForUnitOutParam = value?.Unit;
            }
        }
        private TestAttribute _SelectedOutParam;

        public ICommand AddInParam { get; set; }
        public ICommand ChangeInParam { get; set; }
        public ICommand AddOutParam { get; set; }
        public ICommand ChangeOutParam { get; set; }
        public ICommand RemoveInParam { get; set; }
        public ICommand RemoveOutParam { get; set; }

        public AttrTestParamRedactorModel()
        {
            try
            {
                UpdateDutsList();
                AddInParam = new RelayCommand(o =>
                {
                    if (CheckParam(TextForInputParam, TextForShortNameInputParam, InParam))
                        AppFunc.InsertNewAttrTestParam(0, TextForInputParam, TextForShortNameInputParam, TextForUnitInputParam, (int)TypeParam.In, SelectedDut.Key);
                    DrawAttrLists();
                });
                AddOutParam = new RelayCommand(o =>
                {
                    if (CheckParam(TextForOutParam, TextForShortNameOutParam, OutParam))
                        AppFunc.InsertNewAttrTestParam(0, TextForOutParam, TextForShortNameOutParam, TextForUnitOutParam, (int)TypeParam.Out, SelectedDut.Key);
                    DrawAttrLists();
                });
                ChangeInParam = new RelayCommand(o =>
                {
                    if (SelectedInParam != null)
                        AppFunc.InsertNewAttrTestParam(SelectedInParam.Id, TextForInputParam, TextForShortNameInputParam, TextForUnitInputParam, (int)TypeParam.In, SelectedDut.Key);
                    DrawAttrLists();
                });
                ChangeOutParam = new RelayCommand(o =>
                {
                    if (SelectedOutParam != null)
                        AppFunc.InsertNewAttrTestParam(SelectedOutParam.Id, TextForOutParam, TextForShortNameOutParam, TextForUnitOutParam, (int)TypeParam.Out, SelectedDut.Key);
                    DrawAttrLists();
                });

                RemoveInParam = new RelayCommand(o =>
                {
                    if (SelectedInParam != null)
                        AppFunc.RemoveAttrTestParam(SelectedInParam.Id);
                    DrawAttrLists();
                });

                RemoveOutParam = new RelayCommand(o =>
                {
                    if (SelectedOutParam != null)
                        AppFunc.RemoveAttrTestParam(SelectedOutParam.Id);
                    DrawAttrLists();
                });
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
        }

        public void UpdateDutsList()
        {
            Duts = AppFunc.GetDuts();
            OnPropertyChanged("Duts");
        }

        private bool CheckParam(string text, string shortname, List<TestAttribute> source)
        {
            if (string.IsNullOrEmpty(text))
            {
                AppFunc.ShowMessage("Не заполнено название атрибута!");
                return false;
            }
            if (SelectedDut.Key == 0)
            {
                AppFunc.ShowMessage("Не выбрано Изделие!");
                return false;
            }
            if (source.Any(c => c.Name == text && c.ShortName == shortname))
            {
                AppFunc.ShowMessage("Такой атрибут уже есть!");
                return false;
            }
            return true;
        }

        private void DrawAttrLists()
        {
            try
            {
                InParam = AppFunc.GetAtrNamesForDuts(SelectedDut.Key, (int)TypeParam.In);
                OnPropertyChanged("InParam");
                OutParam = AppFunc.GetAtrNamesForDuts(SelectedDut.Key, (int)TypeParam.Out);
                OnPropertyChanged("OutParam");
            }
            catch
            { }
        }


    }
}
