﻿using AToM_ATE.Classes;
using AToM_ATE.Views;
using Google.Protobuf.WellKnownTypes;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class TestStarterControlModel : ViewModelBase
    {
        private const string thisistrueresults = "thisistrueresults";
        private const string log_current_test = "log_current_test.txt";
        private const string log_full_statistic = "log_full_statistic.txt";
        private const string stopTestFile = "stoptest.stoptest";
        private const string log_current_state = "log_current_state.txt";
        private const string clearStatFile = "clearstat.clearstat";
        private const string initialStringOfFullStaticticFile = "0;0;0";

        private bool needAskPlateNumber = false;

        #region Свойства формы
        public string PathToPython
        {
            get => _PathToPython;
            set => SetProperty(ref _PathToPython, value);
        }
        private string _PathToPython;

        public string BatchNumber
        {
            get => _BatchNumber;
            set => SetProperty(ref _BatchNumber, value);
        }
        private string _BatchNumber = "0";


        public string ValidNumber
        {
            get => _ValidNumber;
            set => SetProperty(ref _ValidNumber, value);
        }
        private static string _ValidNumber = "0";

        public string BadNumber
        {
            get => _BadNumber;
            set => SetProperty(ref _BadNumber, value);
        }
        private string _BadNumber = "0";

        public string AllNumber
        {
            get => _AllNumber;
            set => SetProperty(ref _AllNumber, value);
        }
        private string _AllNumber = "0";

        public string CurrentState
        {
            get => _CurrentState;
            set => SetProperty(ref _CurrentState, value);
        }
        private string _CurrentState = "ГОТОВ";

        public string BakgrCurrentState
        {
            get => _BakgrCurrentState;
            set => SetProperty(ref _BakgrCurrentState, value);
        }
        private string _BakgrCurrentState = "CornflowerBlue";

        //*** private bool allowReadStaticticAndState = true;
        #endregion

        #region Списки и выбранный элемент
        public List<KeyValuePair<int, string>> Duts { get; set; }
        public KeyValuePair<int, string> SelectedDut
        {
            get => _SelectedDut;
            set
            {
                SetProperty(ref _SelectedDut, value);
                FillTestPlanList();
            }
        }
        private KeyValuePair<int, string> _SelectedDut;

        public List<TestPlanStruct> TestPlans { get; set; }
        public TestPlanStruct SelectedTestPlan
        {
            get => _SelectedTestPlan;
            set
            {
                SetProperty(ref _SelectedTestPlan, value);
                FillItemsTestPlan();

                byte[] TpBytes = Encoding.ASCII.GetBytes(value.GetJSONString());
                var crc32 = new Crc32();
                var hash = String.Empty;
                foreach (byte b in crc32.ComputeHash(TpBytes))
                    hash += b.ToString("x2").ToUpper();
                Src32ForTestPlan = hash;
                if (value != null)
                {
                    readCurrentStateAndStatistic();
                }
            }
        }
        private TestPlanStruct _SelectedTestPlan;

        public string Src32ForTestPlan
        {
            get => _Src32ForTestPlan;
            set => SetProperty(ref _Src32ForTestPlan, value);
        }
        string _Src32ForTestPlan = "";

        public List<TestPlanElement> Sequence { get; set; }

        public string CurrentSessionResults
        {
            get => _ScriptResults;
            set => SetProperty(ref _ScriptResults, value);
        }
        private string _ScriptResults;
        public string CurrentTestPlanResults
        {
            get => _TestPlanResults;
            set => SetProperty(ref _TestPlanResults, value);
        }
        private string _TestPlanResults;

        public string CountBadResults
        {
            get => _CountBadResults;
            set
            {
                if (string.IsNullOrEmpty(value))
                    SetProperty(ref _CountBadResults, value);
                else if (int.TryParse(value, out int i))
                    SetProperty(ref _CountBadResults, value);
                else
                {
                    value = _CountBadResults;
                    SetProperty(ref _CountBadResults, value);
                }
            }
        }

        private string _CountBadResults;

        public bool TestIsStarted
        {
            get => _TestIsStarted;
            set => SetProperty(ref _TestIsStarted, value);
        }
        private bool _TestIsStarted = false;

        public double OpacytyMainGrid
        {
            get => _OpacytyMainGrid;
            set => SetProperty(ref _OpacytyMainGrid, value);
        }
        private double _OpacytyMainGrid = 1;

        private int SecondsOfTimeExecute;

        public string TimeNow
        {
            get => _TimeNow;
            set => SetProperty(ref _TimeNow, value);
        }
        private string _TimeNow;

        //public bool LoopCurrentTestPlan
        //{
        //    get => _LoopCurrentTestPlan;
        //    set => SetProperty(ref _LoopCurrentTestPlan, value);
        //}
        //private bool _LoopCurrentTestPlan = false;

        public string LoopCurrentTestPlanCount
        {
            get => _LoopCurrentTestPlanCount;
            set => SetProperty(ref _LoopCurrentTestPlanCount, value);
        }
        private string _LoopCurrentTestPlanCount = "1";

        public bool EnableButtonPanel
        {
            get => _EnableButtonPanel;
            set => SetProperty(ref _EnableButtonPanel, value);
        }
        private bool _EnableButtonPanel = true;

        public bool StopButtonEnable
        {
            get => _StopButtonEnable;
            set => SetProperty(ref _StopButtonEnable, value);
        }
        private bool _StopButtonEnable;

        public bool PauseButtonEnable
        {
            get => _PauseButtonEnable;
            set => SetProperty(ref _PauseButtonEnable, value);
        }
        private bool _PauseButtonEnable;

        public bool StartButtonEnable
        {
            get => _StartButtonEnable;
            set => SetProperty(ref _StartButtonEnable, value);
        }
        private bool _StartButtonEnable = true;
        private bool TestWasPaused = false;

        #endregion

        private string SettingsFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\PythonPath.ate";

        public TestStarterControlView View { get; set; }

        #region Команды формы
        public ICommand TestPlanRedactor { get; set; }
        public ICommand PlayTestPlan { get; set; }
        public ICommand ScriptExecute { get; set; }
        public ICommand SaveTestResult { get; set; }
        public ICommand ShowNorms { get; set; }
        public ICommand CurrentTetPlanStopedByButton { get; set; }
        public ICommand CurrentTetPlanPauseButton { get; set; }
        public ICommand TryLoadFromDb { get; set; }
        public ICommand GetStatByGroups => _GetStatByGroups ?? (_GetStatByGroups = new RelayCommand(o => EvGetStatByGroups()));
        public ICommand ClearStat => _ClearStat ?? (_ClearStat = new RelayCommand(o => EvClearStat()));
        private ICommand _GetStatByGroups;
        private ICommand _ClearStat;
        
        #endregion

        public TestStarterControlModel()
        {
            InitComponents();
        }

        private void InitComponents()
        {
            try
            {
                TryLoadFromDb = new RelayCommand(o => LoadDuts());

                PlayTestPlan = new RelayCommand(o => EvPlayTestPlan());
                ScriptExecute = new RelayCommand(o => EvScriptExecute());
                ShowNorms = new RelayCommand(o => evShowNorms());
                
                CurrentTetPlanStopedByButton = new RelayCommand(o =>
                {
                    CreateFileFromCMD(stopTestFile);    //** только для отладки
                    SocketServer.TP_Stop();
                    PauseButtonEnable = false;
                    TestWasPaused = false;
                    StopButtonEnable = false;
                });

                CurrentTetPlanPauseButton = new RelayCommand(o =>
                {
                    CreateFileFromCMD("pause.pause");   //** только для отладки
                    SocketServer.TP_Pause();
                    TestWasPaused = true;
                    PauseButtonEnable = false;
                    StartButtonEnable = true;
                    StopButtonEnable = true;
                });

                if (File.Exists(SettingsFile))
                {
                    StreamReader f = new StreamReader(SettingsFile);
                    PathToPython = f.ReadLine();
                    f.Close();
                }
                else
                    AppFunc.ShowMessage("Для корректной работы приложения необходимо указать путь до исполняемого файла Python!\nЗапустить тест будет невозможно!");

                if (!string.IsNullOrEmpty(PathToPython) && PathToPython.Contains(" "))
                    AppFunc.ShowMessage("Путь до исполняемого файла Python содержит пробелы!\nЗапустить тест будет невозможно!");

                LoadDuts();

                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        SecondsOfTimeExecute++;
                        TimeNow = TimeSpan.FromSeconds(SecondsOfTimeExecute).ToString();
                        Thread.Sleep(1000);
                        readCurrentStateAndStatistic();
                    }
                });
            }
            catch
            { }
        }

        public void LoadDuts()
        {
            Duts = AppFunc.GetDuts();
            OnPropertyChanged("Duts");
        }

        private void FillTestPlanList()
        {
            try
            {
                TestPlans = AppFunc.GetTestPlans(SelectedDut.Key);
                OnPropertyChanged("TestPlans");
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
        }

        public void FillItemsTestPlan()
        {
            Sequence = new List<TestPlanElement>();
            try
            {
                if (SelectedTestPlan != null && SelectedTestPlan.Items.Count > 0)
                {
                    List<TestPlanItem> plan = SelectedTestPlan.Items;
                    if ((plan?.Count ?? 0) > 0)
                    {
                        List<TestPlanElement> itemsInfo = AppFunc.GetTestPlanElements(plan.Select(c => c.TestId).ToList());
                        plan.ForEach(c =>
                        {
                            TestPlanElement el = itemsInfo.FirstOrDefault(ie => ie.TestID == c.TestId);
                            if (el != null)
                                Sequence.Add(el);
                        });
                    }
                }
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
            OnPropertyChanged("Sequence");
        }

        #region События формы

        private void EvScriptExecute()
        {
            new MainWindowView() { DataContext = new MainWindowModel() }.Show();
        }

        private void RemovePauseFile()
        {
            bool res = false;
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    string StopFile = $"{Path.GetDirectoryName($"{AppFunc.RoorDirForDll}\\{SelectedTestPlan.PathToScipt}")}\\pause.pause";
                    File.Delete(StopFile);
                    res = true;//если выше не упало, то всё нормально                    
                }
                catch
                { }
                if (res)
                    break;
                Thread.Sleep(500);
            }
        }

        private void CreateFileFromCMD(string name)
        {
            try
            {
                string fullPath = $"{Path.GetDirectoryName($"{AppFunc.RoorDirForDll}\\{SelectedTestPlan.PathToScipt}")}\\{name}";
                if (!File.Exists(fullPath))
                {
                    File.Create(fullPath);
                }
            }
            catch
            {
                Thread.Sleep(100);
                CreateFileFromCMD(name);
            }
        }

        private void EvPlayTestPlan()
        {
            try
            {
                if (TestWasPaused)
                {
                    CreateFileFromCMD("stoppause.pause");
                    TestWasPaused = false;
                    PauseButtonEnable = true;
                    StopButtonEnable = true;
                    StartButtonEnable = false;
                    return;
                }
                SecondsOfTimeExecute = 0;
                if (!checkInputsInMainWindow(false))
                    return;

                SocketServer.TP_Start();
                try
                {
                    string fullPathRoLoge = $"{Path.GetDirectoryName($"{AppFunc.RoorDirForDll}\\{SelectedTestPlan.PathToScipt}")}\\{log_current_test}";
                    if (File.Exists(fullPathRoLoge))
                        File.Delete(fullPathRoLoge);

                    //string StopFile = $"{Path.GetDirectoryName($"{AppFunc.RoorDirForDll}\\{SelectedTestPlan.PathToScipt}")}\\{stopTestFile}";
                    //if (File.Exists(StopFile))
                    //    File.Delete(StopFile);
                    //RemovePauseFile();
                }
                catch
                { }
                //#endif
                Task.Factory.StartNew(() =>
                {
                    StopButtonEnable = true;
                    PauseButtonEnable = true;
                    DateTime StartTime = DateTime.Now;
                    EnableButtonPanel = false;
                    StartButtonEnable = false;
                    TimeNow = "";

                    TestIsStarted = true;
                    int countBadResults = 0;
                    if (!string.IsNullOrEmpty(CountBadResults))
                        int.TryParse(CountBadResults, out countBadResults);

                    StartPythonScriptWhithLogFile($"/c {PathToPython} " +
                                       $"{AppFunc.RoorDirForDll}\\{SelectedTestPlan.PathToScipt} " +
                                       $"{AppFunc.RoorDirForDll}{Sequence[0].PathDll} " +
                                       $"{countBadResults} {SelectedTestPlan.Id} " +
                                       //$"{SelectedDut.Key} {BatchNumber} {(LoopCurrentTestPlan ? 1 : 0)} " + 
                                       $"{SelectedDut.Key} {BatchNumber} {LoopCurrentTestPlanCount} " +
                                       $"{(needAskPlateNumber ? 1 : 0)}");
                });
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
                OpacytyMainGrid = 1;
                TestIsStarted = false;
                EnableButtonPanel = true;
                StartButtonEnable = true;
                StopButtonEnable = false;
                PauseButtonEnable = false;
            }
        }

        public void endTestForGui() {
            OpacytyMainGrid = 1;
            TestIsStarted = false;
            EnableButtonPanel = true;
            StartButtonEnable = true;
            StopButtonEnable = false;
            PauseButtonEnable = false;
        }
        private bool checkInputsInMainWindow(bool dutOnlyChecking)
        {
            if (string.IsNullOrWhiteSpace(BatchNumber))
            {
                MessageBox.Show("Необходимо задать номер партии!");
                return false;
            }
            if (SelectedDut.Value == null) {
                AppFunc.ShowMessage("Необходимо выбрать изделие!");
                return false;
            }
            if (dutOnlyChecking) {
                return true;
            }
            if ((Sequence?.Count ?? 0) == 0)
            {
                AppFunc.ShowMessage("Необходимо выбрать тест план!");
                return false;
            }
            //#if DEBUG == false
            if (string.IsNullOrEmpty(PathToPython))
            {
                AppFunc.ShowMessage("Необходимо выбрать путь до интерпретатора Python на вкладке сервис!");
                return false;
            }
            return true;
        }

        // private List<string> GettingLogs = new List<string>();

        private void StartPythonScriptWhithLogFile(string arg)
        {
            try
            {
                string rootDirectoryPath = $"{Path.GetDirectoryName($"{AppFunc.RoorDirForDll}\\{SelectedTestPlan.PathToScipt}")}\\";
                ResultFilesManager resultFilesManager = new ResultFilesManager(rootDirectoryPath, "Results");
                resultFilesManager.clearAll();
                TestDriver testDriver = new TestDriver( arg, rootDirectoryPath, View, this);
                Thread InstanceCaller = new Thread(
                       new ThreadStart(testDriver.Run));
                InstanceCaller.Start();
                
            }
            catch (Exception ee)
            {
                CurrentSessionResults += "\n" + ee.ToString();
            }
        }

        private void initCurrentSessionResultsBuffer(string tableHeader)
        {
            CurrentSessionResults = tableHeader;
            //View.LogAll(CurrentSessionResults);
        }

        private void initCurrentTestPlanResultsBuffer(string tableHeader) {
            CurrentTestPlanResults = tableHeader;
        }

        public void readCurrentStateAndStatistic()
        {
            try
            {
                    readStatistic();
                    readCurrentStatus();
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
        }

        private void readStatistic(){
			try
			{
                ValidNumber = SocketServer._ValidNumber;
                BadNumber   = SocketServer._BadNumber;
                AllNumber   = SocketServer._AllNumber;
            }
			catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
		}

        public void readCurrentStatus()
        {
            try
            {
                    string result = SocketServer._TestStatus;
                    if (!string.IsNullOrWhiteSpace(result))
                    {
                    result = result.Replace("\r", "").Replace("\n", "");
                    CurrentState = result.ToUpper();
                    if (CurrentState.StartsWith("READY"))
                        BakgrCurrentState = "CornflowerBlue";
                    else if (CurrentState.StartsWith("MEASURE"))
                        BakgrCurrentState = "Yellow";
                    else if (CurrentState.StartsWith("VALID"))
                        BakgrCurrentState = "Green";
                    else if (CurrentState.StartsWith("BAD"))
                        BakgrCurrentState = "Crimson";
                    else
                        BakgrCurrentState = "Transparent";
                }
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage(ee.ToString());
            }
        }

        public void evShowNorms()
        {
            try
            {
                if ((Sequence?.Count ?? 0) > 0)
                    (new NormaViewerView() { DataContext = new NormaViewerModel(SelectedDut.Value, Sequence) }).Show();
                else
                    AppFunc.ShowMessage("Для просмотра норм нужно выбрать тестплан");
            }
            catch
            {
            }
        }

        private void EvGetStatByGroups()
        {
            try
            {
                try
                {
                    if (!checkInputsInMainWindow(false)) { return; }
                    //if (ValidNumber.Equals("0")) return;
                    string PathToReportModeule = $"{AppFunc.RoorDirForDll}\\Reports\\group_stat.py";
                    if (!File.Exists(PathToReportModeule))
                    {
                        AppFunc.ShowMessage($"Скрипт запуска модуля отчётов не найден: {PathToReportModeule}");
                        return;
                    }
                    Task.Factory.StartNew(() =>
                    {
                        //Гуи питона по человечески запускаться не хочет. А через батник нормально.
                        string Arguments = $"{PathToPython} {PathToReportModeule} {SelectedDut.Key} {SelectedTestPlan.Id} {AllNumber} \nexit";
                        string bat = $"{Environment.CurrentDirectory}\\runGroupStat.bat";
                        File.WriteAllText(bat, Arguments);
                        Process proc = new Process()
                        {
                            StartInfo = new ProcessStartInfo()
                            {
                                FileName = bat,
                                UseShellExecute = false,
                                RedirectStandardOutput = true,
                                RedirectStandardError = true,
                                CreateNoWindow = true,
                                WindowStyle = ProcessWindowStyle.Hidden
                            }
                        };
                        proc.Start();

                        while (true)
                        {
                            try
                            {
                                if (proc.HasExited)
                                {
                                    string err = proc.StandardError.ReadToEnd();
                                    if (!string.IsNullOrEmpty(err))
                                    {
                                        CurrentSessionResults = err + CurrentSessionResults;
                                        //View.LogAll(CurrentSessionResults);
                                        break;
                                    }
                                    break;
                                }
                            }
                            catch
                            { }
                            Thread.Sleep(300);
                        }
                    });
                }
                catch
                { }
            }
            catch
            { }
        }

        private void EvClearStat()
        {
            if (!checkInputsInMainWindow(false))  return;
            if (TestIsStarted) return;

            //CreateFileFromCMD(clearStatFile);

            SocketServer.ClearStat();
            Console.WriteLine("clear statistic");
        }

        ~TestStarterControlModel()
        {
            //if (TestIsStarted && LoopCurrentTestPlan)
            if (TestIsStarted)
            {
                try
                {
                    CreateFileFromCMD(stopTestFile);
                    Thread.Sleep(100);
                }
                catch (Exception ee)
                {
                    AppFunc.ShowMessage(ee.Message);
                }

                
            }
        }
        #endregion
    }
}

