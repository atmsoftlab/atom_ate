﻿using AToM_ATE.Classes;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class AddNewDutsModel : ViewModelBase
    {
        public string NameDuts
        {
            get => _NameDuts;
            set => SetProperty(ref _NameDuts, value);
        }
        private string _NameDuts;

        public string FullNameDuts
        {
            get => _FullNameDuts;
            set => SetProperty(ref _FullNameDuts, value);
        }
        private string _FullNameDuts;

        public ICommand AddNewDuts { get; set; }

        public АТоМWorkbenchModel MainDataContext { get; set; }

        public AddNewDutsModel()
        {
            AddNewDuts = new RelayCommand(c =>
            {
                try
                {
                    if (string.IsNullOrEmpty(NameDuts))
                    {
                        AppFunc.ShowMessage("Укажите название изделия");
                        return;
                    }
                    if (string.IsNullOrEmpty(FullNameDuts))
                    {
                        AppFunc.ShowMessage("Укажите описание изделия");
                        return;
                    }
                    AppFunc.InsertNewDuts(NameDuts, FullNameDuts);
                    AppFunc.UpdateDutsList();
                    MainDataContext.AttrTestParamRedactorModelDtcnt.UpdateDutsList();
                    MainDataContext.TestParametersRedactorModelDtcnt.UpdateDutsList();
                    MainDataContext.TestOutTemplateModelDtcnt.UpdateDutsList();
                    MainDataContext.TestPlanRedatorModelDtcnt.UpdateDutsList();
                    MainDataContext.ConstructorTestElementModelDtcnt.UpdateDutsList();
                    MainDataContext.TestStarterControlModelDtcnt.LoadDuts();
                }
                catch
                { }
            });
        }


    }
}
