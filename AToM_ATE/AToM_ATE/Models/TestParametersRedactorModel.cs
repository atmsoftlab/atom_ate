﻿using AToM_ATE.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class TestParametersRedactorModel : ViewModelBase
    {
        public List<KeyValuePair<int, string>> Duts { get; set; }
        public KeyValuePair<int, string> SelectedDut
        {
            get => _SelectedDut;
            set
            {
                SetProperty(ref _SelectedDut, value);
                GenerateSourceParam();
                TestAtributsSource = AppFunc.GetAtrNamesForDuts(SelectedDut.Key, (int)TypeParam.In);
                OnPropertyChanged("TestAtributsSource");
                TestsFromDb = AppFunc.GetTestParamsForDuts(SelectedDut.Key, TypeParam.In);
                OnPropertyChanged("TestsFromDb");
            }
        }
        private KeyValuePair<int, string> _SelectedDut;

        public List<TestAttribute> TestAtributsSource { get; set; }

        public List<TestParam> SourceParam { get; set; }

        public string CommentForCurrentCollection
        {
            get => _CommentForCurrentCollection;
            set => SetProperty(ref _CommentForCurrentCollection, value);
        }
        private string _CommentForCurrentCollection;

        public List<Tuple<int, string, List<TestParam>>> TestsFromDb { get; set; }
        public Tuple<int, string, List<TestParam>> SelectedTestsFromDb
        {
            get => _SelectedTestsFromDb;
            set
            {
                SetProperty(ref _SelectedTestsFromDb, value);
                GenerateSourceParam();
                if ((value?.Item3?.Count ?? 0) != 0)
                    value.Item3.ForEach(c =>
                    {
                        if (c.Ind < SourceParam.Count)
                            SourceParam[c.Ind] = c;
                    });
                SourceParam = new List<TestParam>(SourceParam);
                OnPropertyChanged("SourceParam");
            }
        }
        private Tuple<int, string, List<TestParam>> _SelectedTestsFromDb;


        public ICommand SaveParams { get; set; }
        public ICommand ClearAll { get; set; }
        public ICommand SaveSelectedParams { get; set; }

        public TestParametersRedactorModel()
        {
            try
            {
                UpdateDutsList();

                SaveParams = new RelayCommand(c => { SaveParam(); });
                ClearAll = new RelayCommand(c =>
                {
                    GenerateSourceParam();
                    SelectedTestsFromDb = null;
                });
                SaveSelectedParams = new RelayCommand(c => { SaveParam(SelectedTestsFromDb.Item1); });
            }
            catch
            { }
        }

        public void UpdateDutsList()
        {
            Duts = AppFunc.GetDuts();
            OnPropertyChanged("Duts");
        }

        private void GenerateSourceParam()
        {
            SourceParam = new List<TestParam>();
            for (int i = 0; i < 50; i++)
                SourceParam.Add(new TestParam() { Ind = i });
            OnPropertyChanged("SourceParam");
        }

        private void SaveParam(int id = 0)
        {
            if (string.IsNullOrWhiteSpace(CommentForCurrentCollection))
            {
                AppFunc.ShowMessage("Необходимо указать наименование набора");
                return;
            }
            try
            {
                List<TestParam> noEmptyParam = new List<TestParam>();
                int i = 0;
                SourceParam.OrderBy(c => c.Ind).ToList().ForEach(p =>
                {
                    if (!string.IsNullOrEmpty(p.Value))
                    {
                        p.Ind = i;
                        p.Name = TestAtributsSource.FirstOrDefault(k => k.Id == p.AttrId).Name;
                        p.ShortName = TestAtributsSource.FirstOrDefault(k => k.Id == p.AttrId).ShortName;
                        p.Unit = TestAtributsSource.FirstOrDefault(k => k.Id == p.AttrId).Unit;
                        p.Value = p.Value.Replace(",", ".");
                        noEmptyParam.Add(p);
                        i++;
                    }
                });
                if (noEmptyParam.Count > 0)
                    AppFunc.SaveTestParameters(id, noEmptyParam.GetJSONString(), (int)TypeParam.In, SelectedDut.Key, CommentForCurrentCollection);

                TestsFromDb = AppFunc.GetTestParamsForDuts(SelectedDut.Key, TypeParam.In);
                OnPropertyChanged("TestsFromDb");
                if (id != 0)
                    SelectedTestsFromDb = TestsFromDb.FirstOrDefault(c => c.Item1 == id);
                else
                    SelectedTestsFromDb = TestsFromDb.LastOrDefault();
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage("Попытка сохранить некорректные данные!");
                AppFunc.ShowMessage(ee.Message);
            }
        }
    }
}
