﻿using AToM_ATE.Classes;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace AToM_ATE.Models
{
    public class MainWindowModel : ViewModelBase
    {
        #region Свойства формы
        public string SelectedPath
        {
            get => _SelectedPath;
            set => SetProperty(ref _SelectedPath, value);
        }
        private string _SelectedPath = "Выберите скрипт для запуска...";

        public string ScriptResults
        {
            get => _ScriptResults;
            set => SetProperty(ref _ScriptResults, value);
        }
        private string _ScriptResults;

        public string ResultsFromDB
        {
            get => _ResultsFromDB;
            set => SetProperty(ref _ResultsFromDB, value);
        }
        private string _ResultsFromDB;

        public string PathToPython
        {
            get => _PathToPython;
            set => SetProperty(ref _PathToPython, value);
        }
        private string _PathToPython;

        public string SelectPythonPathBackground
        {
            get => _SelectPythonPathBackground;
            set => SetProperty(ref _SelectPythonPathBackground, value);
        }
        private string _SelectPythonPathBackground;


        private List<string> DbResults = new List<string>();

        #endregion

        private string SettingsFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "PythonPath.ate";


        #region Команды формы

        public ICommand SelectPythonPath { get; set; }
        public ICommand PlayScript { get; set; }
        public ICommand OpenScript { get; set; }
        #endregion

        public MainWindowModel()
        {
            InitComponents();
        }

        private void InitComponents()
        {
            try
            {
                SelectPythonPath = new RelayCommand(o => EvSelectPythonPath());
                PlayScript = new RelayCommand(o => EvPlayScript());
                OpenScript = new RelayCommand(o => EvOpenScript());

                if (File.Exists(SettingsFile))
                {
                    StreamReader f = new StreamReader(SettingsFile);
                    PathToPython = f.ReadLine();
                    f.Close();
                }

                if (string.IsNullOrEmpty(PathToPython))
                    SelectPythonPathBackground = "Red";
            }
            catch
            { }
        }


        #region События формы

        private void EvSelectPythonPath()
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog()
                {
                    CheckFileExists = false,
                    CheckPathExists = true,
                    Multiselect = false,
                    RestoreDirectory = false,
                    Title = "Выберите файл запуска программы",
                    Filter = "Прогамма (*.exe)|*.exe"

                };
                if (dialog.ShowDialog() == true)
                    PathToPython = dialog.FileName;

                if (string.IsNullOrEmpty(PathToPython))
                    SelectPythonPathBackground = "Red";
                else
                    SelectPythonPathBackground = "Transparent";

                using (StreamWriter sw = new StreamWriter(SettingsFile, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(PathToPython);
                }

            }
            catch
            { }
        }

        private void EvPlayScript()
        {
            try
            {
                if (string.IsNullOrEmpty(PathToPython))
                {
                    AppFunc.ShowMessage("Необходимо выбрать путь до интерпретатора Python!");
                    return;
                }
                if (SelectedPath == "Выберите скрипт для запуска...")
                {
                    AppFunc.ShowMessage("Скрипт не выбран!");
                    return;
                }
                if (!SelectedPath.EndsWith(".py") && MessageBox.Show("У выбраного файла расширение не соответствует скрипту Python!\n" +
                                                                     "Все равно продолжить?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;

                ScriptResults = StartPythonScript($"/c {PathToPython} {SelectedPath}");
            }
            catch (Exception ee)
            {
                ScriptResults += $"ERROR: {ee.Message}\n";
            }

        }

        private string StartPythonScript(string arg)
        {
            Process p = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "cmd.exe",
                    Arguments = arg,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden
                }
            };
            p.Start();
            p.WaitForExit();

            string result = "";
            if (p.StandardOutput != null)
                result += p.StandardOutput.ReadToEnd();
            if (result != "")
                result = result.TrimEnd('\n');
            string err = p.StandardError.ReadToEnd();
            if (!string.IsNullOrEmpty(err))
                result += $"ОШИБКА\n--------------------------------\n{err}--------------------------------\n";
            return result;
        }

        private void EvOpenScript()
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog()
                {
                    CheckFileExists = false,
                    CheckPathExists = true,
                    Multiselect = false,
                    RestoreDirectory = true,
                    Title = "Выберите скрипт",
                    Filter = "Python (*.py)|*.py"

                };
                if (dialog.ShowDialog() == true)
                    SelectedPath = dialog.FileName;
            }
            catch
            { }
        }

        #endregion
    }
}
