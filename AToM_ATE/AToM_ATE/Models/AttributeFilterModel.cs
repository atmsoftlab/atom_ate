﻿using AToM_ATE.Classes;
using System.Collections.Generic;
using System.Linq;

namespace AToM_ATE.Models
{
    public class AttrFilterStructure : ViewModelBase
    {
        public bool Check
        {
            get => _Check;
            set
            {
                SetProperty(ref _Check, value);
                if (value && ParentSourceContext != null)
                {
                    if (ParentSourceContext.Source.Count(c => c.Check) == 1)
                        Ind = 0;
                    else
                        Ind = ParentSourceContext.Source.Where(c => c.Check).Max(c => c.Ind) + 1;
                    ParentSourceContext.Filter();
                }
            }
        }
        private bool _Check;
        public int AttrId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Unit { get; set; }
        public int Ind { get; set; }

        public AttributeFilterModel ParentSourceContext { get; set; }
    }

    public class AttributeFilterModel : ViewModelBase
    {
        public List<AttrFilterStructure> Source = new List<AttrFilterStructure>();
        public List<AttrFilterStructure> FilteredSource { get; set; }

        public string NameFilter
        {
            get => _NameFilter;
            set
            {
                SetProperty(ref _NameFilter, value);
                Filter();
            }
        }

        private string _NameFilter;

        public void Filter()
        {
            Source = new List<AttrFilterStructure>(Source);
            if (Source.Count > 0 && !string.IsNullOrEmpty(NameFilter))
                FilteredSource = Source.Where(c => !string.IsNullOrEmpty(c.Name) && c.Name.ToLower().Contains(NameFilter.ToLower())).ToList();
            else
                FilteredSource = Source;
            OnPropertyChanged("FilteredSource");
        }

        public AttributeFilterModel(List<TestAttribute> TestAtributsSource, Dictionary<int, int> checkedAttribute)
        {
            try
            {
                int i = 0;
                if ((TestAtributsSource?.Count ?? 0) > 0)
                    TestAtributsSource.ForEach(s =>
                    {
                        Source.Add(new AttrFilterStructure()
                        {
                            Check = checkedAttribute.ContainsKey(s.Id),
                            AttrId = s.Id,
                            Ind = checkedAttribute.ContainsKey(s.Id) ? checkedAttribute[s.Id] : 0,
                            Name = s.Name,
                            ShortName = s.ShortName,
                            Unit = s.Unit,
                            ParentSourceContext = this
                        });
                        i++;
                    });
                FilteredSource = Source;
                OnPropertyChanged("FilteredSource");
            }
            catch
            { }
        }
    }
}
