﻿using System.Windows;
using System.Windows.Controls;

namespace AToM_ATE.Views
{
    /// <summary>
    /// Логика взаимодействия для TestOutTemplateView.xaml
    /// </summary>
    public partial class TestOutTemplateView : UserControl
    {
        public TestOutTemplateView()
        {
            InitializeComponent();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if ((sender as TextBox).Text == "0")
                (sender as TextBox).Text = "";
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((sender as TextBox).Text == "")
                (sender as TextBox).Text = "0";
        }
    }
}
