﻿using AToM_ATE.Classes;
using System.Reflection;
using System.Threading;
using System.Windows;
using AutoUpdaterDotNET;

namespace AToM_ATE.Views
{
    /// <summary>
    /// Логика взаимодействия для АТоМWorkbenchView.xaml
    /// </summary>

    public partial class AToMWorkbenchView : Window
    {


        public AToMWorkbenchView()
        {
            InitializeComponent();
            Assembly _assembly = Assembly.GetEntryAssembly();
            LabelVersion.Content = $"Текущая версия : {_assembly.GetName().Version}";

        }

        private void win_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxButton buttons = MessageBoxButton.YesNo;
            MessageBoxResult result;
            result = MessageBox.Show("Вы уверены, что хотите выйти?", "Предупреждение:", buttons);
            
            if (result == MessageBoxResult.Yes) SocketServer.StopServer();
            e.Cancel = !(result == MessageBoxResult.Yes);
        }

        private void TestStarterControlView_Loaded(object sender, RoutedEventArgs e)
        {

        }


    }
}
