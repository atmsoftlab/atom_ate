﻿using System.Windows.Controls;

namespace AToM_ATE.Views
{
    /// <summary>
    /// Логика взаимодействия для AttrTestParamRedactorView.xaml
    /// </summary>
    public partial class AttrTestParamRedactorView : UserControl
    {
        public AttrTestParamRedactorView()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                (sender as TextBox).ScrollToEnd();
            }
            catch { }
        }
    }
}
