﻿using System.Windows;
using System.Windows.Controls;

namespace AToM_ATE
{
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                (sender as TextBox).ScrollToEnd();
            }
            catch { }
        }
    }
}
