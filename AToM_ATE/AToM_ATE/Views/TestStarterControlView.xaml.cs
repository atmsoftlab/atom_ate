﻿using AToM_ATE.Classes;
using AToM_ATE.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;



namespace AToM_ATE.Views
{
    public partial class TestStarterControlView : System.Windows.Controls.UserControl
    {
        private static string TextShow = "";

        public TestStarterControlView()
        {
            InitializeComponent();
            FillResultsFromDB(JSONView.Records.Text);
        }

        private void win_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DataContext is TestStarterControlModel vm)
                {
                    vm.View = this;
                    vm.readCurrentStateAndStatistic();
                }
            }
            catch
            { }
        }

        public void LogAll(String msg)
        {
            TextShow = msg + TextShow;
            AllWebBrws.Dispatcher.InvokeAsync(new Action(() =>
            {
                if (!string.IsNullOrEmpty(msg))
                {
                    AllWebBrws.NavigateToString(TextShow);
                }
            }));
        }

        public void LogCurr(String msg)                 // память утекает здесь))
        {
            CurWebBrws.Dispatcher.InvokeAsync(new Action(() =>
            {
                if (!string.IsNullOrEmpty(msg))
                {
                    CurWebBrws.NavigateToString(msg);
                }
            }));
        }

        public void FillResultsFromDB(String Records)
        {
            String sql = "SELECT value FROM measurements ORDER BY id DESC LIMIT " + Records;
            MySQLWraper Wraper = new MySQLWraper();
            DataTable dt = Wraper.Select(sql);

            foreach (DataRow row in dt.Rows)
            {
                string str3 = row["value"].ToString();  
                try { JSONView.Load(str3); }                            // доделать - отображается только последняя (?) запись
                catch (Exception ex) { MessageBox.Show(ex.Message); }       
            }
        }
    }
}
