﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace AToM_ATE.Classes
{
    public static class SocketServer
    {
        public static String _AllNumber     = "00";
        public static String _ValidNumber   = "00";
        public static String _BadNumber     = "00";
        public static String _TestStatus    = "ГОТОВ";

        private const String StartTP = "!StartTestPlan";
        private const String PauseTP = "!PauseTestPlan";
        private const String StopTP  = "!StopTestPlan";
        private const String InitTP  = "!Init";

        private static String Command_to_send = InitTP;
        private static Boolean ServStarted = true;

        public static void ClearStat()
        {
            _AllNumber = "0";
            _ValidNumber = "0";
            _BadNumber = "0";
        }

        public static void TP_Start() { Command_to_send = StartTP; Console.WriteLine("...StartTestPlan");}
        public static void TP_Pause() { Command_to_send = PauseTP; Console.WriteLine("...PauseTestPlan");}
        public static void TP_Stop()  { Command_to_send = StopTP;  Console.WriteLine("...StopTestPlan"); }
        public static void TP_Init()  { Command_to_send = InitTP;  Console.WriteLine("...InitTestPlan"); }

        public static void StartServer()
        {
            String message = "";
            String[] messages;
            Char[] delimiterChars = { ' ', ',', '.', ';', '=' };

            byte[] bytes = new byte[256];

            try
            {
                TcpListener serverSocket = new TcpListener(IPAddress.Any, 7000);
                while (ServStarted)
                {
                    try
                    {
                        serverSocket.Start();
                        Console.WriteLine("Socket server started");
                        TcpClient clientSocket = serverSocket.AcceptTcpClient();
                        NetworkStream stream = clientSocket.GetStream();
                        SendPrompt(stream);

                        while (ServStarted)
                        {
                            int length = stream.Read(bytes, 0, bytes.Length);
                            String Request = Encoding.ASCII.GetString(bytes, 0, length);
                            Console.WriteLine("Recieved request: " + Request + ", length= " + length);
 
                            if (length == 0) break;             // клиент отвалился, не закрыв соединения

                            message += Request;
                            if (Request.IndexOf('?') > -1)      // команда от клиента '?' - запрос команды для скрипта
                            {
                                bytes = Encoding.ASCII.GetBytes(Command_to_send+"\n\r");   // шлем клиенту команду для скрипта
                                stream.Write(bytes, 0, bytes.Length);
                                stream.Flush();
                                SendPrompt(stream);
                                Console.WriteLine("Sent command: " + Command_to_send);
                            }
                            else if (Request.IndexOf(':') > -1)     // команда от клиента ':' - конец текущего сообщения от клиента
                            {
                                // Console.WriteLine("Sent message: " + message);

                                messages = message.Split(delimiterChars);
                                message = "";

                                int index = Array.IndexOf(messages, "ValidNumber"); if (index > -1) { _ValidNumber = messages[index + 1]; }
                                index = Array.IndexOf(messages, "BadNumber"); if (index > -1) { _BadNumber = messages[index + 1]; }
                                index = Array.IndexOf(messages, "AllNumber"); if (index > -1) { _AllNumber = messages[index + 1]; }
                                index = Array.IndexOf(messages, "TestStatus"); if (index > -1) { _TestStatus = messages[index + 1]; }
                                SendPrompt(stream);
                            }
                            else if (Request.IndexOf('\u001a') > -1)     // команда от клиента - закрыть сессию 
                            {
                                break;
                            }
                            else        // получено завершенное сообщение от клиента
                            {
                                Console.WriteLine("Get message: " + message);
                            }
                        }
                        clientSocket.Close();
                        serverSocket.Stop();
                        Console.WriteLine("Socket server stopped");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                serverSocket.Stop();
                Console.WriteLine("Socket server shutdown");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void SendPrompt(NetworkStream stream)
        {
            stream.Write(Encoding.ASCII.GetBytes("\n\r>"), 0, Encoding.ASCII.GetBytes("\n\r>").Length);
            stream.Flush();
            Console.WriteLine(">");
        }

        public static void StopServer()
        {
            ServStarted = false;
            StartServer();
        }
    }
}
