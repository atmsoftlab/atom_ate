﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows;

namespace AToM_ATE.Classes
{
    public enum TypeParam
    {
        In = 1,
        Out = 2
    }

    public static class AppFunc
    {
        public static string RoorDirForDll = "";

        //Ищем наш рут по дискам
        public static bool ChekRootDllDirrectories()
        {
            bool res = false;
            try
            {
                List<string> WordsDisk = new List<string>()
                { "A", "B", "C", "D", "E", "F", "G", "H", "I", "G", "K", "L", "M",
                  "N", "O","P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
                foreach (string disk in WordsDisk)
                    if (Directory.Exists($"{disk}:\\TestInstruments"))
                    {
                        res = true;
                        AppFunc.RoorDirForDll = $"{disk}:\\\\TestInstruments";
                        break;
                    }
            }
            catch
            {
            }
            if (!res)
                AppFunc.ShowMessage("Прикладные модули располагаются в корне любого диска в папке \"TestInstruments\"\n" +
                                    " Каталог \"TestInstruments\" не обнаружен! Запуск прикладных модулей невозможен!");
            return res;
        }

        public static string GetJSONString(this object o)
        {
            return JsonConvert.SerializeObject(o);
        }

        public static void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        public static List<KeyValuePair<int, string>> DutsCache = new List<KeyValuePair<int, string>>();

        public static void UpdateDutsList()
        {
            DutsCache = GetKvpIdAndName($"SELECT * FROM duts");
        }

        public static List<KeyValuePair<int, string>> GetDuts()
        {
            return DutsCache;
        }

        public static bool InsertNewDuts(string name, string fullname)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = Wraper.Select($"insert into duts (name, fullname) values ('{name}', '{fullname}')");
                res = true;
                string mes = res ? "Добавлено" : "Не добавлено!";
                ShowMessage(mes);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            return res;
        }

        public static List<TestPlanStruct> GetTestPlans(int DutsId)
        {
            List<TestPlanStruct> res = new List<TestPlanStruct>();
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                string sql = $"select * from test_plans tp where tp.duts_id = {DutsId} && tp.ishide = 0";
                DataTable dt = Wraper.Select(sql);
                if ((dt?.Rows?.Count ?? 0) > 0)
                    foreach (DataRow r in dt.Rows)
                        res.Add(new TestPlanStruct(r));
            }
            catch (Exception ee)
            {
                ShowMessage(ee.ToString());
            }
            return res;
        }

        public static List<TestAttribute> GetAtrNamesForDuts(int DutsId, int type)
        {
            List<TestAttribute> res = new List<TestAttribute>();
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                string sql = $"SELECT * FROM attr_test_param atp where atp.duts_id = {DutsId} AND atp.type = {type}";
                DataTable dt = Wraper.Select(sql);
                if ((dt?.Rows?.Count ?? 0) > 0)
                    foreach (DataRow r in dt.Rows)
                        res.Add(new TestAttribute(r));
            }
            catch (Exception ee)
            {
                ShowMessage(ee.ToString());
            }
            return res;

        }

        public static List<KeyValuePair<int, string>> GetTestParam(int DutsId, int type)
        {
            return GetKvpIdAndName($"SELECT * FROM test_parameters tp where tp.duts_id = {DutsId} AND tp.type = {type}", nameColumn: "comment");
        }

        public static bool SaveTestElement(string name, int duts_id, string test_function, string path_to_dll, int inparam, int outparam)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = Wraper.Select($"insert into test_elements (name, duts_id, test_function, path_to_dll, inparam, outparam) " +
                                                            $"values ('{name}', {duts_id}, '{test_function}', '{path_to_dll}', {inparam}, {outparam})");
                res = true;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            string mes = res ? "Добавлено" : "Не добавлено!";
            ShowMessage(mes);
            return res;
        }

        public static bool SaveTestParameters(int id, string value, int type, int duts, string comment)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = null;
                if (id == 0)
                    dt = Wraper.Select($"insert into test_parameters (parametr, type, duts_id, comment) values ('{value}', {type}, {duts}, '{comment}')");
                else
                    dt = Wraper.Select($"update test_parameters set parametr = '{value}' where id = {id}");
                res = true;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            string mes = res ? "добавлено" : "НЕ добавлено!";
            if (id != 0)
                mes = mes.Replace("добавлено", "сохранено");
            ShowMessage(mes);
            return res;
        }

        public static bool InsertNewAttrTestParam(int id, string name, string short_name, string unit, int type, int duts)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                string sql = $"insert into attr_test_param (name, type, duts_id, short_name, unit) values ('{name}', {type}, {duts}, '{short_name}', '{unit}')";
                if (id != 0)
                    sql = $"update attr_test_param set name = '{name}', short_name = '{short_name}', unit = '{unit}' where id = {id}";
                DataTable dt = Wraper.Select(sql);
                res = true;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            string mes = res ? "Добавлено" : "Не добавлено!";
            if (!res)
                ShowMessage(mes);
            return res;
        }

        public static bool RemoveAttrTestParam(int id)
        {
            bool res = false;
            try
            {
                string sql = $"delete from attr_test_param where id = {id}";
                DataTable dt = new MySQLWraper().Select(sql);
                res = true;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            string mes = res ? "Добавлено" : "Не добавлено!";
            if (!res)
                ShowMessage(mes);
            return res;
        }

        public static List<Tuple<int, string, List<TestParam>>> GetTestParamsForDuts(int duts, TypeParam type)
        {
            List<Tuple<int, string, List<TestParam>>> res = new List<Tuple<int, string, List<TestParam>>>();
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                string sql = $"select * from test_parameters tp where tp.duts_id = {duts} AND tp.type = {(int)type}";

                DataTable dt = Wraper.Select(sql);
                if ((dt?.Rows?.Count ?? 0) > 0)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        try
                        {
                            string param = GetDataByColumn("parametr", r);
                            if (!string.IsNullOrEmpty(param))
                                res.Add(new Tuple<int, string, List<TestParam>>(Convert.ToInt32(GetDataByColumn("id", r)), GetDataByColumn("comment", r), JsonConvert.DeserializeObject<List<TestParam>>(param)));
                        }
                        catch
                        {
                            //нечего в базу руками лазить!!!
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ShowMessage(ee.ToString());
            }
            return res;
        }

        public static long SaveTestPlanResult(string plate, string batch, int result, string resGroup, DateTime start, DateTime finish)
        {
            long res = 0;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                string sql = $"INSERT INTO testplan_results(plate_number, batch_number, result, result_group, start_date, finish_date) " +
                             $"values ('{plate}', '{batch}', {result}, '{resGroup}', '{MySQLWraper.ToMySqlDt(start)}', '{MySQLWraper.ToMySqlDt(finish)}'); " +
                             $"SELECT @@identity AS 'Res'";
                DataTable dt = Wraper.Select(sql);

                if ((dt?.Rows?.Count ?? 0) > 0)
                {
                    string val = GetDataByColumn("Res", dt.Rows[0]);
                    if (!string.IsNullOrEmpty(val))
                        res = Convert.ToInt64(val);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            return res;
        }

        public static bool SaveMeasurements(string value, int test, int testPlan, int duts, string inputparam, long TestplanResultId)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = Wraper.Select($"insert into measurements (value, test_id, test_plan_id, duts_id, input_parameters, testplan_results) " +
                                             $"values ('{value}', {test}, {testPlan}, {duts}, '{inputparam}', {TestplanResultId})");
                res = true;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            return res;
        }

        public static List<TestPlanElement> GetTestPlanElements(List<int> ids)
        {
            List<TestPlanElement> res = new List<TestPlanElement>();
            if ((ids?.Count ?? 0) == 0)
                return res;
            DataTable dt = null;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                dt = Wraper.Select($"SELECT te.id, te.name, te.test_function, te.path_to_dll, tpin.parametr AS 'input', tpout.parametr AS 'output' " +
                                   $"FROM test_elements te " +
                                   $"JOIN test_parameters tpin ON te.inparam = tpin.id " +
                                   $"JOIN test_parameters tpout ON te.outparam = tpout.id " +
                                   $"WHERE te.id IN({string.Join(",", ids)})");
                if ((dt?.Rows?.Count ?? 0) > 0)
                    foreach (DataRow r in dt.Rows)
                    {
                        TestPlanElement te = new TestPlanElement(Convert.ToInt32(GetDataByColumn("id", r)))
                        {
                            Name = GetDataByColumn("name", r),
                            FunctionName = GetDataByColumn("test_function", r),
                            PathDll = GetDataByColumn("path_to_dll", r),
                            InputParameters = JsonConvert.DeserializeObject<List<TestParam>>(GetDataByColumn("input", r)),
                            TestResults = JsonConvert.DeserializeObject<List<TestParam>>(GetDataByColumn("output", r)),
                        };
                        if (!te.PathDll.StartsWith("\\"))
                            te.PathDll = "\\" + te.PathDll;
                        res.Add(te);
                    }
            }
            catch (Exception ee)
            {
                ShowMessage(ee.ToString());
            }

            return res;
        }

        public static List<KeyValuePair<int, string>> GetTestElementsSource(int duts)
        {
            List<KeyValuePair<int, string>> res = new List<KeyValuePair<int, string>>() { new KeyValuePair<int, string>(0, "") };
            string sql = $"select * from test_elements te where te.duts_id = {duts}";
            var fromDb = GetKvpIdAndName(sql);
            if (fromDb.Count > 0)
                res.AddRange(fromDb);
            return res;
        }

        public static bool SaveTestPlan(int id, string structure, int duts_id, string comment, string path_to_script)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = null;
                string sql = $"insert into test_plans (structure, duts_id, comment) " +
                                                 $"values ('{structure}', {duts_id}, '{comment}')";
                if (id != 0)
                    sql = $"update test_plans " +
                          $"set structure = '{structure}', duts_id = {duts_id}, comment = '{comment}',  " +
                          $"path_to_script = '{path_to_script.Replace("\\", "\\\\")}' " +
                          $"where id = {id}";



                dt = Wraper.Select(sql);

                res = true;
            }
            catch (Exception ee)
            {
                string err = ee.ToString();
                if (err.ToLower().Contains("Duplicate entry"))
                    err = "Тест план с таким названием уже существует";
                ShowMessage(err);
                return false;
            }
            string mes = res ? "Добавлено" : "Не добавлено!";
            ShowMessage(mes);
            return res;
        }

        public static bool HideTestPlan(int id, string comment)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = null;
                if (id != 0)
                {
                    string sql = $"update test_plans " +
                          $"set ishide = 1, comment = 'Remove_{DateTime.Now}_{comment}' " +
                          $"where id = {id}";



                    dt = Wraper.Select(sql);
                }
                res = true;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            string mes = res ? "Удалён" : "Не удалён!";
            ShowMessage(mes);
            return res;
        }

        public static List<KeyValuePair<int, string>> GetKvpIdAndName(string sql, string nameIdColumn = "id", string nameColumn = "name")
        {
            List<KeyValuePair<int, string>> res = new List<KeyValuePair<int, string>>();
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = Wraper.Select(sql);
                if ((dt?.Rows?.Count ?? 0) > 0)
                {
                    foreach (DataRow r in dt.Rows)
                        res.Add(new KeyValuePair<int, string>(Convert.ToInt32(GetDataByColumn(nameIdColumn, r)), GetDataByColumn(nameColumn, r)));
                }
            }
            catch (Exception ee)
            {
                if (ee.ToString().Contains("Unable to connect to any of the specified MySQL hosts"))
                    ShowMessage("Ошибка подключения к базе данных.\nПроверьте параметры подключения в файле arm_id.ini\nи наличие подключения к СУБД.");
                else
                    ShowMessage(ee.Message);
            }

            return res;
        }

        public static string GetDataByColumn(string ColumnName, DataRow dr)
        {
            string res = "";
            if (dr.Table.Columns.Contains(ColumnName))
                res = dr[ColumnName].ToString();
            return res;
        }

    }
}
