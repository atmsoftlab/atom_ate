﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AToM_ATE.Classes
{
    class ResultFilesManager
    {
        
        private const bool ALLOW_END_CHECK = true;
        private const string DEFAULT_FILES_PLACE_ON_ROOT = "/Results/";
        private const string END_STRING = "thisistrueresults DONE!";
        private HashSet<string> buffer;
        private string directory;
        FileCleaner FCleaner;
        public ResultFilesManager(string rootDirectory, string template) {
            this.directory = rootDirectory + (template != null ? template : DEFAULT_FILES_PLACE_ON_ROOT);
            checkDirectoryOrCreate(directory);
            buffer = new HashSet<string>();
        }

        private void checkDirectoryOrCreate(string directory)
        {
            if (!Directory.Exists(directory)) {
                Directory.CreateDirectory(directory);
            }
        }

        public string getNewFilePathIfExist()
        {
            DirectoryInfo info = new DirectoryInfo(directory);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
            int filesCount = files.Length;
            if (buffer.Count == filesCount) return "";
            else {
                foreach (FileInfo file in files)
                {
                    string filePath = file.FullName;
                    if (!buffer.Contains(filePath))
                    {
                        string fileText = "";
                        try
                        {
                            fileText = File.ReadAllText(filePath);
                            if (!fileText.Contains(END_STRING) && ALLOW_END_CHECK) {
                                return ""; 
                            }
                        }
                        catch {
                            return ""; 
                        }
                        buffer.Add(filePath);
                        return fileText;
                    }
                }
                return "";
            }
        }
        public bool isCompliteReading() {
            DirectoryInfo info = new DirectoryInfo(directory);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
            
            foreach (FileInfo file in files) {
                if (!buffer.Contains(file.FullName)) {
                    return false;
                }            
            }
                return true;
        }

        public void startCleaner()
        {
            FCleaner = new FileCleaner(buffer);
            Thread InstanceCaller = new Thread(
                       new ThreadStart(FCleaner.Run));
            InstanceCaller.Start();
        }

        public void endCleaner() {
            FCleaner.Stop();
        } 
        public void clearAll()
        {
            foreach (string filePath in Directory.GetFiles(directory)) {
                while (true)
                {
                    try
                    {
                        File.Delete(filePath);
                        break;
                    }
                    catch
                    {
                        Thread.Sleep(100);
                    }
                }
            }
        }
    }
}
