﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AToM_ATE.Classes
{
    class FileCleaner
    {
        private volatile bool isStart = false;
        HashSet<string> files;
        public FileCleaner(HashSet<string> listFiles)
        {
            files = listFiles;
        }

        public void Run()
        {
            isStart = true;
            while (isStart)
            {
                Thread.Sleep(300);
                if (files.Count > 0)
                {
                    deleteOldFile();
                }
            }
        }

        public void Stop()
        {
            isStart = false;
        }

        private void deleteOldFile()
        {
            List<String> filesForDel = files.ToList();
            foreach (string filePath in filesForDel)
            {
                try
                {
                    File.Delete(filePath);
                    files.Remove(filePath);
                }
                catch
                {
                    
                }
            }
        }
    }
}
