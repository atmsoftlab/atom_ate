﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace AToM_ATE.Classes
{
    public class TestParam
    {
        public int Ind { get; set; }
        public int AttrId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Unit { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Norma> NormaTable { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ClassAfterTestExecute { get; set; }
    }

    public class TestPlanStruct
    {
        public int Id { get; set; }
        public List<TestPlanItem> Items { get; set; }
        public string PathToScipt { get; set; }
        public string Comment { get; set; }

        public TestPlanStruct() { }

        public TestPlanStruct(DataRow r)
        {
            Id = Convert.ToInt32(AppFunc.GetDataByColumn("id", r));
            Comment = AppFunc.GetDataByColumn("comment", r);
            PathToScipt = AppFunc.GetDataByColumn("path_to_script", r);
            string structure = AppFunc.GetDataByColumn("structure", r);
            if (!string.IsNullOrEmpty(structure))
                Items = JsonConvert.DeserializeObject<List<TestPlanItem>>(structure);
        }
    }

    public class TestPlanItem
    {
        public int TestId { get; set; }
    }

    public class TestPlanElement
    {
        public int TestID { get; set; }
        public string Name { get; set; }
        public bool TestFinished { get; set; }
        public string PathDll { get; set; }
        public string FunctionName { get; set; }
        public List<TestParam> InputParameters { get; set; }
        public List<TestParam> TestResults { get; set; }

        public TestPlanElement(int _TestID)
        {
            TestID = _TestID;
        }
    }

    public class Norma
    {
        public string NameGroup { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
    }

    public class TestAttribute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Unit { get; set; }

        public TestAttribute(DataRow r)
        {
            Id = Convert.ToInt32(AppFunc.GetDataByColumn("id", r));
            Name = AppFunc.GetDataByColumn("name", r);
            ShortName = AppFunc.GetDataByColumn("short_name", r);
            Unit = AppFunc.GetDataByColumn("unit", r);
        }
        public TestAttribute()
        { }
    }

}