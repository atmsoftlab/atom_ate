﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AToM_ATE.Classes
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityCollapsedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool bValue = (bool)value;
            return ((bValue) ? (Visibility.Visible) : (Visibility.Collapsed));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibilityValue = (Visibility)value;
            return (visibilityValue == Visibility.Visible);
        }
    }
}
