﻿using IniParser;
using IniParser.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace AToM_ATE.Classes
{
    public class MySQLWraper
    {
        private class BuildQueryParametersResult
        {
            public string Query { get; set; }
            public Dictionary<string, object> Parameters { get; set; }
        }

        public MySqlConnection ConnectionString = null;

        public void CreateConnectionString()
        {
            try
            {
                string ConString = "";
                string armIni = AppFunc.RoorDirForDll + "\\arm_id.ini";
                if (File.Exists(armIni))
                {
                    FileIniDataParser ini = new FileIniDataParser();
                    IniData rf = ini.ReadFile(armIni);
                    SectionData dbconnection = rf.Sections.GetSectionData("dbconnection");
                    string NewConString = "";
                    foreach (KeyData sec in dbconnection.Keys)
                    {
                        NewConString += $"{sec.KeyName}={sec.Value};";
                    }
                    ConString = NewConString.TrimEnd(';');
                }
                else
                    AppFunc.ShowMessage("Файл arm_id.ini в папке TestInstruments не найден!\nНастройки к базе данных будут дистрибутивные!");

                ConnectionString = new MySqlConnection(ConString);
            }
            catch (Exception ee)
            {
                AppFunc.ShowMessage($"Ошибка в параметрах файла arm_id.ini. Работа с базой производиться не будет!\n{ee.Message}");
            }
        }

        public DataTable Procedure(string name, params object[] prms)
        {
            if (ConnectionString == null)
                CreateConnectionString();
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("CALL {0}(", name);
            BuildQueryParametersResult buildSqlRes = BuildQueryPrms(prms);
            sb.Append(buildSqlRes.Query);
            string sql = sb.ToString() + ")";
            DataTable res = GetDataTable(sql, name, buildSqlRes.Parameters);
            return res;
        }

        //Прямые запросы плохо! Пока есть вопросы по структуре базы. Потом уберу.
        public DataTable Select(string sql)
        {
            if (ConnectionString == null)
                CreateConnectionString();
            using (MySqlDataAdapter da = new MySqlDataAdapter(sql, ConnectionString))
            {
                DataTable dt = new DataTable("Data");
                da.Fill(dt);
                return dt;
            }
        }

        private DataTable GetDataTable(string sql, string name, Dictionary<string, object> parameters = null)
        {
            using (MySqlDataAdapter da = new MySqlDataAdapter(sql, ConnectionString))
            {
                if (parameters != null)
                {
                    foreach (KeyValuePair<string, object> kpv in parameters)
                    {
                        da.SelectCommand.Parameters.AddWithValue(kpv.Key, kpv.Value);
                    }
                }
                DataTable dt = new DataTable(name);
                da.Fill(dt);
                return dt;
            }

        }

        private BuildQueryParametersResult BuildQueryPrms(params object[] prms)
        {
            Dictionary<string, object> dso = null;
            StringBuilder sb = new StringBuilder();
            int numberParameter = 0;
            foreach (object o in prms)
            {
                if (o == null)
                {
                    sb.Append("NULL");
                }
                else if (o.GetType().IsEnum)
                {
                    sb.Append(Convert.ToInt32(o));
                }
                else if (o is string)
                {
                    sb.Append("\"" + NoInjection(o.ToString()) + "\"");
                }
                else if (o is DateTime)
                {
                    DateTime dt = (DateTime)o;
                    sb.Append("\'" + ToMySqlDt(dt) + "\'");
                }
                else if (o is float || o is decimal || o is double)
                {
                    sb.Append(o.ToString().Replace(',', '.'));
                }
                else if (o is byte[])
                {
                    numberParameter++;
                    string key = "@cusdata" + numberParameter;
                    sb.Append(key);
                    if (dso == null)
                        dso = new Dictionary<string, object>();
                    dso.Add(key, o);
                }
                else
                {
                    sb.Append(o);
                }
                sb.Append(",");
            }
            BuildQueryParametersResult res = new BuildQueryParametersResult()
            {
                Query = sb.ToString().TrimEnd(','),
                Parameters = dso
            };
            return res;
        }

        private string NoInjection(string s)
        {
            return s.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("\'", "\\\'");
        }

        public static string ToMySqlDt(DateTime dt)
        {
            return dt.ToString("yyyy.MM.dd HH:mm:ss");
        }
    }
}
