﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AToM_ATE.Views;
using System.Threading;
using AToM_ATE.Models;
using System.IO;

namespace AToM_ATE.Classes
{
    class TestDriver
    {
        /*
         * Here need make:
         * 1. Python start
         * 2. Results reading
         * 3. Results printing
         * 
         * to do it we need to have refernces for:
         * two browsers**/
       
        private string rootFolderPath;
        private string pythonArgs;
        private TestStarterControlView View;
        private TestStarterControlModel testStarterControl;
        private string CurrentSessionResults = "";
        private string CurrentTestPlanResults = "";
        private List<string> GettingLogs = new List<string>();

        public TestDriver(string pythonArgs, string rootFolderPath, TestStarterControlView View, TestStarterControlModel testStarterControl) {
            this.pythonArgs = pythonArgs;
            this.View = View;
            this.testStarterControl = testStarterControl;
            this.rootFolderPath = rootFolderPath;
        }

        public void Run() {
            System.Console.WriteLine("Python arguments: " + pythonArgs);
            StartPythonScriptWhithLogFile(pythonArgs);
        }
        
        //private Stream GenerateStreamFromString(string s)
        //{
        //    var stream = new MemoryStream();
        //    var writer = new StreamWriter(stream);
        //    writer.Write(s);
        //    writer.Flush();
        //    stream.Position = 0;
        //    return stream;
        //}

        private void StartPythonScriptWhithLogFile(string arg)
        {
            bool init = true;
            string tableHeader = ResultStringParser.GetInitialResultTableString();
            initCurrentSessionResultsBuffer(tableHeader);
            initCurrentTestPlanResultsBuffer(tableHeader);

            try
            {
                Process p = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "cmd.exe",
                        Arguments = arg,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden
                    }
                };

                p.Start();
               
                ResultFilesManager resultFilesManager = new ResultFilesManager(rootFolderPath, "Results");
                resultFilesManager.clearAll();
                resultFilesManager.startCleaner();
                while (true)
                {
                    Thread.Sleep(200);
                    try
                    {
                        string result = resultFilesManager.getNewFilePathIfExist(); 
                        if (!result.Equals(""))
                        {
                            Console.WriteLine("result: "+result);
                            result = result.Trim('\r').Trim('\n');      
                            testStarterControl.readCurrentStatus();

                            string[] lines = result.Split('\n');
                            foreach (string line in lines)
                            {
                                string[] spl = line.Split(';');
                                string curText = "";
                                GettingLogs.Add(result);
                                foreach (string s in spl)
                                {
                                    try
                                    {
                                        string newTableRow = ResultStringParser.getAddedString(s);
                                        if (!newTableRow.Equals(""))
                                        {
                                            if (ResultStringParser.isNewTestPlanResult())
                                            {
                                                initCurrentTestPlanResultsBuffer(tableHeader);
                                            }
                                            curText += newTableRow;
                                            CurrentTestPlanResults += newTableRow;
                                        }
                                    }
                                    catch (Exception ee)
                                    { AppFunc.ShowMessage("1_>>" + ee.Message); }
                                }
                                if (!init)
                                {
                                    CurrentSessionResults = CurrentSessionResults.Insert(tableHeader.Length, curText);
                                }
                                else
                                {
                                    init = false;
                                    CurrentSessionResults += curText;
                                }
                            }
                            
                            View.LogCurr(CurrentTestPlanResults);                            

 //                           CurrentTestPlanResults += "</html>";
 //                           WriteToFile(rootFolderPath + "123.htm", CurrentTestPlanResults);
                            
                            testStarterControl.readCurrentStateAndStatistic();
                        }

                        if (p.HasExited)
                        {
                            if (p.StandardOutput != null)
                            {
                                CurrentSessionResults = p.StandardOutput.ReadToEnd() + CurrentSessionResults;
                            }
                            string err = p.StandardError.ReadToEnd();
                            if (!string.IsNullOrEmpty(err))
                            {
                                CurrentSessionResults = err + CurrentSessionResults;
                            }

                            if (resultFilesManager.isCompliteReading())
                            {
                                resultFilesManager.endCleaner();
                                testStarterControl.readCurrentStatus();
                                break;
                            }
                        }
                    }
                    catch (Exception ee) { AppFunc.ShowMessage(ee.Message); }
                }
                testStarterControl.endTestForGui();
                View.LogAll(CurrentSessionResults);
            }
            catch (Exception ee)
            {
                Console.WriteLine("from Python: "+ee.Message);
                CurrentSessionResults += "\n" + ee.ToString();
            }
        }

        private void WriteToFile(string fileName,string msg)
        {
        try
            {
            StreamWriter sw = new StreamWriter(fileName);
            sw.WriteLine(msg);
            sw.Close();
            }
        catch(Exception e)
        {
            Console.WriteLine("Exception: " + e.Message);
        }
        finally
        {
            Console.WriteLine("Executing finally block.");
        }
    }

private void initCurrentSessionResultsBuffer(string tableHeader)
        {
            CurrentSessionResults = tableHeader;
        }

        private void initCurrentTestPlanResultsBuffer(string tableHeader)
        {
            CurrentTestPlanResults = tableHeader;
        }
    }
}
