﻿using System;
using System.Collections.Generic;

namespace AToM_ATE.Classes
{

    enum ResultStringType
    {
        PlateNumber,
        MeasNumber,
        StartDate,
        EndDate,
        TestHeaderStatus,
        OutResults,
        ShortName,
        FullName,
        Value,
        Unit,
        Status,
        MinNorm,
        MaxNorm,
        Unknown,
        Dut,
        Part,
        TestName,
        TPStatus,
        TPGroup
    }
    class ResultStringParser
    {
        private const char KEY_VALUE_SEPARATOR = ':';
        private const string GOOD_STATUS_STRING = "ГОДЕН";
        private const string BAD_STATUS_STRING = "БРАК";
        private const ResultStringType END_TYPE = ResultStringType.MaxNorm;
        private const ResultStringType SUB_TITLE_END = ResultStringType.TPGroup;

        private static bool isSubTitleReturned = false;
        private static bool isNewTestPlanResults = false;
        // private static int plateNumber;

       

        private static Dictionary<string, ResultStringType>  TYPE_TEGS = new Dictionary<string, ResultStringType>() {
            {"Dut", ResultStringType.Dut},
            {"Part", ResultStringType.Part},
            {"Plate_number", ResultStringType.PlateNumber},
            {"Num", ResultStringType.MeasNumber},
            {"TName", ResultStringType.TestName},
            {"ShortName", ResultStringType.ShortName},
            {"FullName", ResultStringType.FullName},
            {"Value", ResultStringType.Value},
            {"Unit", ResultStringType.Unit},
            {"Status", ResultStringType.Status},
            {"Min", ResultStringType.MinNorm},
            {"Статус ТП", ResultStringType.TPStatus},
            {"Группа ТП", ResultStringType.TPGroup},
            {"Max", ResultStringType.MaxNorm}
        };

        private static Dictionary<string, string> currentData = new Dictionary<string, string>();
        
        private static string[] keysForTableRowCreate = new string[] {
            ResultStringType.MeasNumber.ToString(),
            ResultStringType.TestName.ToString(),
            ResultStringType.ShortName.ToString(),
            ResultStringType.FullName.ToString(),
            ResultStringType.Value.ToString(),
            ResultStringType.Unit.ToString(),
            ResultStringType.Status.ToString(),
            ResultStringType.MinNorm.ToString(),
            ResultStringType.MaxNorm.ToString()
        };

        private static ResultStringType GetStringType(string resultString) 
        {
            string key;
            try
            {
                key = resultString.Split(KEY_VALUE_SEPARATOR)[0].Trim();
                return TYPE_TEGS[key];
             }
            catch (Exception ee)
            {
                key = resultString.Split(KEY_VALUE_SEPARATOR)[0].Trim();
                Console.WriteLine("2>:"+ key + "-"+ ee.Message);
                return ResultStringType.Unknown;
            }           
        }

        public static string getAddedString(string inputStr)
        {
            inputStr = inputStr.Trim();
            ResultStringType type = GetStringType(inputStr);
            ParseInputString(inputStr, type);
            isNewTestPlanResults = false;
            if (type == END_TYPE) {
                if (isSubTitleReturned) {
                    isNewTestPlanResults = true;
                    isSubTitleReturned = false;
                }
                return getNewTableRow();
            }
            if (type == SUB_TITLE_END) {
                return getSubTitleString();
            }
            return "";
        }

        private static string getNewTableRow()
        {
            string color = "White";
            if (currentData[ResultStringType.Status.ToString()].Equals(BAD_STATUS_STRING)) {
                color = "#FF4C3D";
                //currentData[ResultStringType.Status.ToString()] = "Б";
            }
            
            string style = $"style = 'border : 1px solid black'  bgcolor={color}";
            string resultString = "";
            foreach (string teg in keysForTableRowCreate)
            {
                resultString += $"<td {style}> {currentData[teg]} </td>";
            }
            resultString += "</tr>\n";

            return resultString;
        }

        private static void ParseInputString(string inputStr, ResultStringType type)
        {
            switch (type){
                case ResultStringType.ShortName:
                case ResultStringType.FullName:
                case ResultStringType.Value:
                case ResultStringType.Unit:
                case ResultStringType.Status:
                case ResultStringType.MinNorm:
                case ResultStringType.MaxNorm:
                case ResultStringType.Dut:
                case ResultStringType.PlateNumber:
                case ResultStringType.MeasNumber:
                case ResultStringType.Part:
                case ResultStringType.TestName:
                case ResultStringType.TPGroup:
                case ResultStringType.TPStatus:
                    currentData[type.ToString()] = inputStr.Split(KEY_VALUE_SEPARATOR)[1].Trim();
                    break;
            }
        }

        private static string getSubTitleString() {
            string dutName = currentData[ResultStringType.Dut.ToString()];
            string part = currentData[ResultStringType.Part.ToString()];
            string plateNumber = currentData[ResultStringType.PlateNumber.ToString()];
            string TPGroup = currentData[ResultStringType.TPGroup.ToString()];
            string TPStatus = currentData[ResultStringType.TPStatus.ToString()];
            string style = $"style = 'border : 1px solid black' colspan=\"{keysForTableRowCreate.Length}\"";
            isSubTitleReturned = true;
            return $"<tr><th {style}>Изделие : {dutName}, Партия : {part}, Номер изделия: {plateNumber}<br>" +
                $"Статус ТП - {TPStatus}; Группа ТП - {TPGroup}" +
                $"</th></tr>";
        }

        public static string GetInitialResultTableString() {
            return @"<html>
                        <meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
                        <body>
                        <table cellspacing='0' border='1' cellpadding='5'>
                          <tr>
                            <th style='border : 1px solid black; width : 5%'><bold>№</bold></th>
                            <th style='border : 1px solid black; width : 10%'><bold>Тест</bold></th>
                            <th style='border : 1px solid black; width : 10%'><bold>Параметр</bold></th>
                            <th style='border : 1px solid black; width : 25%'><bold>Название</bold></th>
                            <th style='border : 1px solid black; width : 15%'><bold>Значение</bold></th>
                            <th style='border : 1px solid black; width : 10%'><bold>Ед. изм.</bold></th>
                            <th style='border : 1px solid black; width : 5%'><bold>Группа/Брак</bold></th>
                            <th style='border : 1px solid black; width : 10%'><bold>Н. норма</bold></th>
                            <th style='border : 1px solid black; width : 10%'><bold>В. норма</bold></th>
                          </tr>";
        }

        public static string getEndTableString()
        {
            return "</table>";
        }

        public static bool isNewTestPlanResult() {
            return isNewTestPlanResults;
        }
    }
}
